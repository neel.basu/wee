CREATE TABLE public.comments
(
    id bigserial NOT NULL,
    rand bigint NOT NULL DEFAULT ((random() * ((9999999 - 1000))::double precision) + (1000)::double precision),
    "when" timestamp without time zone NOT NULL DEFAULT now(),
    name character varying COLLATE pg_catalog."default" NOT NULL,
    email character varying COLLATE pg_catalog."default",
    phone character varying COLLATE pg_catalog."default",
    body character varying COLLATE pg_catalog."default",
    CONSTRAINT comments_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.comments
    OWNER to postgres;
