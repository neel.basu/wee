/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef WEE_PERSON_H
#define WEE_PERSON_H

#include <cstddef>
#include <string>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/signals2.hpp>
#include <udho/watcher.h>

namespace wee{

namespace session{
    struct user{
        std::size_t _id;
        std::string _first;
        std::string _last;
        
        inline std::size_t id() const{ return _id; }
        inline std::string first() const{ return _first; }
        inline std::string last() const{ return _last; }
        inline std::string name() const{ return _first + " " + _last; }
    };
}
    
struct gathering;
    
struct person{
    friend struct gathering;
    
    enum class status{
        unknown,
        online,
        away,
        busy,
        offline
    };
        
    explicit person(): _id(0), _status(status::unknown), _previous(status::unknown), _seen(boost::posix_time::second_clock::local_time()){}
    explicit person(std::size_t id): _id(id), _status(status::unknown), _previous(status::unknown), _seen(boost::posix_time::second_clock::local_time()){}
    explicit person(std::size_t id, std::string first, std::string last): _id(id), _first(first), _last(last), _status(status::unknown), _seen(boost::posix_time::second_clock::local_time()){}
    inline bool operator<(const person& other) const{ return id() < other.id(); }
    inline std::size_t id() const{ return _id; }
    inline std::string name() const{ return _first + " " + _last; }
    static std::string status_str(status s);
    inline status state() const{ return _status; }
    inline std::string state_str() const{ return status_str(state()); }
    inline boost::posix_time::ptime seen() const{ return _seen; }
    inline boost::posix_time::time_duration age() const{ return boost::posix_time::second_clock::local_time() - _seen; }
    inline boost::posix_time::ptime active() const{ return _active; }
    inline boost::posix_time::time_duration idle() const{ return boost::posix_time::second_clock::local_time() - _active; }
    inline void seen(boost::posix_time::ptime time){ _seen = time; }
    inline void active(boost::posix_time::ptime time){ _active = time; }
    
  private:
    std::size_t _id;
    std::string _first;
    std::string _last;
    status      _status;
    status      _previous;
    boost::posix_time::ptime _seen;
    boost::posix_time::ptime _active;
    
    bool state(status s);
};

struct offline_watch: udho::watch<offline_watch, std::size_t>{
    typedef udho::watch<offline_watch, std::size_t> base;
    
    wee::gathering& _universe;
    std::size_t _user;
    
    offline_watch(std::size_t user, wee::gathering& universe);
    void operator()(const boost::system::error_code& e);
};

struct gathering{
    typedef udho::watcher<offline_watch> offline_watcher_type;
    typedef std::map<std::size_t, person> people_map_type;
    
    boost::asio::io_service& _io;
    offline_watcher_type     _watcher;
    people_map_type          _people;
    
    boost::signals2::signal<void (const wee::person&)> updated;
    
    gathering(boost::asio::io_service& io);
    void enter(const person& p);
    void leave(std::size_t id);
    bool exists(std::size_t id) const;
    person& operator[](std::size_t id);
    const person& operator[](std::size_t id) const;
    bool update(std::size_t id, wee::person::status status);
    inline auto begin() const { return _people.begin(); }
    inline auto end() const { return _people.end(); }
};

}

#endif // WEE_PERSON_H
