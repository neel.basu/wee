/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef WEE_PROJECT_H
#define WEE_PROJECT_H

#include <nlohmann/json.hpp>
#include <udho/context.h>
#include <udho/application.h>
#include <sqlpp11/postgresql/connection.h>
#include <sqlpp11/postgresql/exception.h>
#include <sqlpp11/sqlpp11.h>
#include "person.h"
#include "members.h"

namespace wee{
namespace apps{

/**
 * @todo write docs
 */
struct project: public udho::application<project>{
    typedef udho::application<project> base;
    
    sqlpp::postgresql::connection& _connection;
    wee::apps::members _members;
    
    project(sqlpp::postgresql::connection& conn);
    
    nlohmann::json fetch(udho::contexts::stateful<wee::session::user> ctx);
    nlohmann::json retrieve(udho::contexts::stateful<wee::session::user> ctx, std::size_t id);
    nlohmann::json create(udho::contexts::stateful<wee::session::user> ctx);
    nlohmann::json update(udho::contexts::stateful<wee::session::user> ctx, std::size_t id);
    nlohmann::json remove(udho::contexts::stateful<wee::session::user> ctx, std::size_t id);    
    std::string map(udho::contexts::stateful<wee::session::user> ctx, std::size_t project);
    void home(udho::contexts::stateful<wee::session::user> ctx, std::size_t project);
    void home_named(udho::contexts::stateful<wee::session::user> ctx, std::string project);
    void home_named_args(udho::contexts::stateful<wee::session::user> ctx, std::string project, std::string args);
//     std::string home_tabular(udho::contexts::stateful<wee::session::user> ctx, std::size_t project);
    std::string list(udho::contexts::stateful<wee::session::user> ctx);
    
    template <typename RouterT>
    auto route(RouterT& router){
        return router | (get (&project::retrieve).json()            = "^/(\\d+)/synopsis/?$")
                      | (post(&project::create).json()              = "^/create/?$")
                      | (post(&project::update).json()              = "^/(\\d+)/?$")
                      | (post(&project::remove).json()              = "^/(\\d+)/remove/?$")
                      | (get (&project::map).html()                 = "^/(\\d+)/map/?$")
                      | (get (&project::home).deferred()            = "^/(\\d+)/?$")
                      | (get (&project::home_named).deferred()      = "^/([a-zA-Z]\\w+)/?$")
                      | (get (&project::home_named_args).deferred() = "^/([a-zA-Z]\\w+)/([\\w\\/]+)/?$")
                      | (get (&project::fetch).json()               = "^/fetch/?$")
                      | (udho::app<wee::apps::members>(_members)    = "^/members")
                      | (get (&project::list).html()                = "^/?$");
    }
    
    private:
        bool allowed_to(const std::string& resource, std::size_t project, std::size_t uid, std::string& reason);
};

}
}

#endif // WEE_PROJECT_H
