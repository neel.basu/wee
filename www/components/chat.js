import Vue from 'vue'
import WeeNav from './WeeNav'
import WeeNavItem from './WeeNavItem'
import WeeChatComponent from './WeeChatComponent.vue'
import './style.css'
// import '@fortawesome/fontawesome-free/js/all'

Vue.config.devtools = true

new Vue({
    el: '#main',
    components: {
        WeeNav,
        WeeNavItem,
        'WeeChat': WeeChatComponent
    },
    mounted: function(){},
    methods: {}
});
