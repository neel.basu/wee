import Vue from 'vue'
import WeeNav from './WeeNav'
import WeeNavItem from './WeeNavItem'
import './style.css'

Vue.config.devtools = true

new Vue({
    el: '#main',
    components: {
        WeeNav,
        WeeNavItem
    },
    mounted: function(){},
    methods: {}
});
