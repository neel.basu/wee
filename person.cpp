/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "person.h"

std::string wee::person::status_str(wee::person::status s){
    switch(s){
        case status::unknown:
            return "unknown";
            break;
        case status::online:
            return "online";
            break;
        case status::away:
            return "away";
            break;
        case status::busy:
            return "busy";
            break;
        case status::offline:
            return "offline";
            break;
    }
    return "unknown";
}

bool wee::person::state(status s){
    _previous = _status;
    _status = s;
    return (_previous != _status);
}

wee::offline_watch::offline_watch(std::size_t user, wee::gathering& universe): base(user), _universe(universe), _user(user){

}

void wee::offline_watch::operator()(const boost::system::error_code& e){
    if(e != boost::asio::error::operation_aborted){ // timeout occured, user didn't perform another ping to stop the timeout
        _universe.update(_user, wee::person::status::offline);
    }
}

wee::gathering::gathering(boost::asio::io_service& io): _io(io), _watcher(io, boost::posix_time::seconds(17)){
    
}

void wee::gathering::enter(const wee::person& p){
    _people.insert(std::make_pair(p.id(), p));
    _people.find(p.id())->second.active(boost::posix_time::second_clock::local_time());
}

void wee::gathering::leave(std::size_t id){
    _people.erase(id);
}

bool wee::gathering::exists(std::size_t id) const{
    return _people.find(id) != _people.end();
}

wee::person& wee::gathering::operator[](std::size_t id){
    return _people.find(id)->second;
}

const wee::person& wee::gathering::operator[](std::size_t id) const{
    return _people.find(id)->second;
}

bool wee::gathering::update(std::size_t id, wee::person::status status){
    wee::person& p = _people.find(id)->second;
    _watcher.notify(id);
    offline_watch watch(id, *this);
    _watcher.insert(watch);
    if(p.state(status)){
        updated(p);
        return true;
    }
    if(status == wee::person::status::offline){
        leave(id);
    }
    return false;
}

