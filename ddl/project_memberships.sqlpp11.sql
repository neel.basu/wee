CREATE TABLE project_memberships
(
    user bigint NOT NULL,
    project bigint NOT NULL,
    role bigint NOT NULL,
    order bigint,
    name text,
    membership_id bigint,
    since timestamp without time zone
);
