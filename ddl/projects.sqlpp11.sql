CREATE TABLE projects
(
    id bigserial NOT NULL,
    name text NOT NULL,
    label text NOT NULL,
    description text,
    properties text,
    created timestamp without time zone NOT NULL DEFAULT NOW(),
    owner bigint NOT NULL,
    listing boolean NOT NULL DEFAULT true
);
