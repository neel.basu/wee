/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "type.h"
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/split.hpp>
#include <sqlpp11/value_or_null.h>
#include <sqlpp11/remove.h>
#include <sqlpp11/postgresql/insert.h>
#include <sqlpp11/postgresql/update.h>
#include <sqlpp11/postgresql/returning.h>
#include <sqlpp11/group_by.h>
#include <udho/util.h>
#include <boost/regex.hpp>
#include <xlnt/xlnt.hpp>
#include "ddl/reports.h"
#include "ddl/users.h"
#include "ddl/places.h"
#include "ddl/projects.h"
#include "ddl/memberships.h"
#include "ddl/types.h"
#include "data.h"

wee::apps::type::type(sqlpp::postgresql::connection& conn): base("type"), _connection(conn){}

nlohmann::json wee::apps::type::create(udho::contexts::stateful<wee::session::user> ctx, std::size_t project){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        std::string body = ctx.request().body();
        nlohmann::json json = nlohmann::json::parse(body);
        nlohmann::json properties = json["properties"];
        udho::form::required<std::string> name("name", "Name of the type must not be empty");
        udho::form::required<std::string> label("label", "Label must not be empty");
        
        auto validator = udho::form::validate();
        validator << (json.contains("name")  && json["name"] .is_string() ? name(json["name"]  .get<std::string>()) : name());
        validator << (json.contains("label") && json["label"].is_string() ? label(json["label"].get<std::string>()) : label());
        
        if(!validator.valid()){
            return {
                {"error", true},
                {"success", false},
                {"message", validator.errors().size() ? validator.errors().front() : std::string("Invalid input")},
                {"details", {
                    {"errors", validator.errors()},
                    {"fields", {
                        {"name", {
                            {"valid", validator["name"].valid()},
                            {"error", validator["name"].error()}
                        }},
                        {"label", {
                            {"valid", validator["label"].valid()},
                            {"error", validator["label"].error()}
                        }}
                    }}
                }}
            };
        }else{
            wee::Projects projects;
            auto project_records = _connection(select(projects.id, projects.owner).from(projects).where(projects.id == project));
            if(!project_records.size()){
                throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
            }
            const auto& project_record = project_records.front();
            std::size_t owner = project_record.owner;
            
            std::string reason;
            if(loggedin_user.id() != owner && !allowed_to("project:mutate", project, loggedin_user.id(), reason)){
                return {
                    {"error", true},
                    {"message", reason}
                };
            }
            
            wee::Types types;
            auto statement = sqlpp::postgresql::insert_into(types).set(
                types.name       = name.value(),
                types.label      = label.value(),
                types.properties = properties.dump(),
                types.project    = project
            ).returning(types.id);

            const auto results = _connection(statement);
            return {
                {"id", results.front().id.value()},
                {"success", true}
            };
        }
    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}

nlohmann::json wee::apps::type::update(udho::contexts::stateful<wee::session::user> ctx, std::size_t id){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        std::string body = ctx.request().body();
        nlohmann::json json = nlohmann::json::parse(body);
        nlohmann::json properties = json["properties"];
        udho::form::required<std::string> name("name", "Name of the type must not be empty");
        udho::form::required<std::string> label("label", "Label must not be empty");
        
        auto validator = udho::form::validate();
        validator << (json.contains("name")  && json["name"] .is_string() ? name(json["name"]  .get<std::string>()) : name());
        validator << (json.contains("label") && json["label"].is_string() ? label(json["label"].get<std::string>()) : label());
        
        if(!validator.valid()){
            return {
                {"error", true},
                {"success", false},
                {"message", validator.errors().size() ? validator.errors().front() : std::string("Invalid input")},
                {"details", {
                    {"errors", validator.errors()},
                    {"fields", {
                        {"name", {
                            {"valid", validator["name"].valid()},
                            {"error", validator["name"].error()}
                        }},
                        {"label", {
                            {"valid", validator["label"].valid()},
                            {"error", validator["label"].error()}
                        }}
                    }}
                }}
            };
        }else{
            wee::Types types;
            auto type_records = _connection(select(types.project).from(types).where(types.id == id));
            if(!type_records.size()){
                throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Type");
            }
            std::size_t project = type_records.front().project;
            
            wee::Projects projects;
            auto project_records = _connection(select(projects.id, projects.owner).from(projects).where(projects.id == project));
            if(!project_records.size()){
                throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
            }
            const auto& project_record = project_records.front();
            std::size_t owner = project_record.owner;
            std::string reason;
            if(loggedin_user.id() != owner && !allowed_to("project:mutate", project, loggedin_user.id(), reason)){
                return {
                    {"error", true},
                    {"message", reason}
                };
            }
            
            auto statement =  sqlpp::update(types).set(
                types.name       = name.value(),
                types.label      = label.value(),
                types.properties = properties.dump()
            ).where(types.id == id);

            _connection(statement);
            return {
                {"id", id},
                {"success", true}
            };
        }
    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}

nlohmann::json wee::apps::type::remove(udho::contexts::stateful<wee::session::user> ctx, std::size_t id){}

bool wee::apps::type::allowed_to(const std::string& resource, std::size_t project, std::size_t uid, std::string& reason){
    std::string sql = (boost::format("select * from allowed_to('%1%', %2%, %3%)") % resource % project % uid).str();
    auto statement = custom_query(sqlpp::verbatim(sql))
        .with_result_type_of(select(
            sqlpp::value(false).as(sqlpp::alias::a),
            sqlpp::value("").as(sqlpp::alias::b)
        ));
    auto records = _connection(statement);
    const auto& permission = records.front();
    reason = permission.b.value();
    return permission.a.value();
}
