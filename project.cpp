/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "project.h"
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/split.hpp>
#include <sqlpp11/value_or_null.h>
#include <sqlpp11/remove.h>
#include <sqlpp11/postgresql/insert.h>
#include <sqlpp11/postgresql/update.h>
#include <sqlpp11/postgresql/returning.h>
#include <sqlpp11/group_by.h>
#include <udho/util.h>
#include <boost/regex.hpp>
#include <xlnt/xlnt.hpp>
#include "ddl/reports.h"
#include "ddl/users.h"
#include "ddl/places.h"
#include "ddl/projects.h"
#include "ddl/types.h"
#include "ddl/memberships.h"
#include "data.h"

SQLPP_ALIAS_PROVIDER(nreports);
SQLPP_ALIAS_PROVIDER(nplaces);
SQLPP_ALIAS_PROVIDER(ntypes);
SQLPP_ALIAS_PROVIDER(type_name);
SQLPP_ALIAS_PROVIDER(type_label);

wee::apps::project::project(sqlpp::postgresql::connection& conn): base("project"), _connection(conn), _members(conn){}

std::string wee::apps::project::map(udho::contexts::stateful<wee::session::user> ctx, std::size_t p){
    wee::Projects projects;
    wee::Reports  reports;
    wee::Users    users;
    wee::Types    types;
    auto reportcount = select(reports.project, count(reports.id)).from(reports).unconditionally().group_by(reports.project).as(sqlpp::alias::a);
    auto typescount  = select(types.project, count(types.id)).from(types).unconditionally().group_by(types.project).as(sqlpp::alias::b);
    auto records = _connection(
        select(
            projects.id, 
            projects.name, 
            projects.label, 
            projects.description, 
            projects.created, 
            projects.properties,
            projects.owner,
            users.firstName, 
            users.lastName,
            reportcount.count.as(nreports),
            typescount.count.as(ntypes)
        ).from(
            projects.join(users).on(projects.owner == users.id)
                   .left_outer_join(reportcount).on(projects.id == reportcount.project)
                   .left_outer_join(typescount).on(projects.id == typescount.project)
        ).order_by(projects.id.asc())
        .where(projects.id == p)
    );
    
    if(!records.size()){
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
    }
    
    std::size_t owner = records.front().owner;
    
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        std::string reason;
        if(loggedin_user.id() != owner && !allowed_to("place:fetch", p, loggedin_user.id(), reason)){
            throw udho::exceptions::http_error(boost::beast::http::status::forbidden, reason);
        }
    }else{
        std::string reason;
        if(!allowed_to("place:fetch", p, 0, reason)){
            throw udho::exceptions::http_error(boost::beast::http::status::forbidden, reason);
        }
    }
    
    auto types_records = _connection(
        select(
            types.id,
            types.name,
            types.label,
            types.properties
        ).from(types).order_by(types.id.asc())
        .where(types.project == p)
    );
    
    auto& record = records.front();
    
    std::time_t created_time = std::chrono::system_clock::to_time_t(record.created.value());
    std::string created_str(std::ctime(&created_time));
        
    std::string properties = record.properties.value();
    
    nlohmann::json project_types = nlohmann::json::array();
    for(const auto& type_record: types_records){
        std::string type_properties = type_record.properties.value();
        
        nlohmann::json project_type = {
            {"id",          type_record.id.value()},
            {"name",        type_record.name},
            {"label",       type_record.label},
            {"properties",  type_properties.empty() ? nlohmann::json::object() : nlohmann::json::parse(type_properties)}
        };
        project_types.push_back(project_type);
    }
    
    nlohmann::json project_json = {
        {"id",           record.id.value()},
        {"name",         record.name},
        {"label",        record.label},
        {"description",  record.description},
        {"reports",      record.nreports.value()},
        {"created",      created_str},
        {"types",        project_types},
        {"properties",   properties.empty() ? nlohmann::json::object() : nlohmann::json::parse(properties)},
        {"owner", {
            {"id",    ctx.session().exists<wee::session::user>() ? record.owner.value() : 0},
            {"first", ctx.session().exists<wee::session::user>() ? record.firstName : std::string("VoxPeople")},
            {"last",  ctx.session().exists<wee::session::user>() ? record.lastName : std::string()}
        }},
    };
    
    wee::Places places;
    auto contributions = select(places.author, count(places.id)).from(places).unconditionally().group_by(places.author).as(sqlpp::alias::a);
    auto contribution_records = _connection(
        select(
            users.id, 
            users.firstName, 
            users.lastName, 
            users.photo,
            users.designation, 
            users.currentDesignation, 
            contributions.count
        )
        .from(users.join(contributions).on(contributions.author == users.id))
        .where(contributions.count > 1)
        .order_by(contributions.count.desc())
    );
    
    data::project project(p, ctx.session().exists<wee::session::user>());
    project.name  = record.name;
    project.label = record.label;
    project.description = record.description;
    project.synopsis = project_json;
    
    for(const auto& record: contribution_records){
        std::string name = record.firstName.value() + " " + record.lastName.value();
        data::volunteer volunteer(name);
        volunteer.id           = record.id;
        volunteer.affiliation  = record.designation;
        volunteer.affiliation2 = record.currentDesignation;
        volunteer.photo        = record.photo;
        volunteer.count = record.count.value();
        project.volunteers.push_back(volunteer);
    }
    
    data::user user(ctx.session().exists<wee::session::user>());
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        user.id   = loggedin_user.id();
        user.name = loggedin_user.name();
    }
    return ctx.render("spatial.html", project, user);
}

void wee::apps::project::home(udho::contexts::stateful<wee::session::user> ctx, std::size_t project){
    ctx.reroute((boost::format("/projects/%1%/map") % project).str());
    return;
}

void wee::apps::project::home_named(udho::contexts::stateful<wee::session::user> ctx, std::string project){
    wee::Projects projects;
    auto records = _connection(
        select(
            projects.id, 
            projects.name
        ).from(projects)
        .where(projects.name == project)
    );
    if(!records.size()){
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
    }
    auto& record = records.front();
    std::cout << "rerouted to: " << (boost::format("/projects/%1%") % record.id.value()).str() << std::endl;
    ctx.reroute((boost::format("/projects/%1%") % record.id.value()).str());
    return;
}

void wee::apps::project::home_named_args(udho::contexts::stateful<wee::session::user> ctx, std::string project, std::string args){
    wee::Projects projects;
    auto records = _connection(
        select(
            projects.id, 
            projects.name
        ).from(projects)
        .where(projects.name == project)
    );
    if(!records.size()){
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
    }
    auto& record = records.front();
    std::cout << "rerouted to: " << (boost::format("/projects/%1%/%2%") % record.id.value() % args).str() << std::endl;
    ctx.reroute((boost::format("/projects/%1%/%2%") % record.id.value() % args).str());
    return;
}

nlohmann::json wee::apps::project::fetch(udho::contexts::stateful<wee::session::user> ctx){
    wee::Projects projects;
    wee::Reports  reports;
    wee::Users    users;
    wee::Types    types;
    auto reportcount = select(reports.project, count(reports.id)).from(reports).unconditionally().group_by(reports.project).as(sqlpp::alias::a);
    auto typecount = select(types.project, count(types.id)).from(types).unconditionally().group_by(types.project).as(sqlpp::alias::b);
    auto records = _connection(
        select(
            projects.id, 
            projects.name, 
            projects.label, 
            projects.description, 
            projects.created, 
            projects.properties,
            projects.owner,
            projects.listing,
            users.firstName, 
            users.lastName,
            reportcount.count.as(nreports),
            typecount.count.as(ntypes)
        ).from(
            projects.join(users).on(projects.owner == users.id)
                   .left_outer_join(reportcount).on(projects.id == reportcount.project)
                   .left_outer_join(typecount).on(projects.id == typecount.project)
        ).order_by(projects.id.asc())
        .unconditionally()
    );
    nlohmann::json spatial_projects = nlohmann::json::array();
    for(const auto& record: records){
        const auto& project_record = record;
        std::size_t p     = project_record.id;
        std::size_t owner = project_record.owner;

        if(ctx.session().exists<wee::session::user>()){
            wee::session::user loggedin_user;
            ctx.session() >> loggedin_user;
            std::string reason;
            if(loggedin_user.id() != owner && !allowed_to("project:view", p, loggedin_user.id(), reason)){
                continue;
            }
        }else{
            std::string reason;
            if(!allowed_to("project:view", p, 0, reason)){
                continue;
            }
        }
        
        std::time_t created_time = std::chrono::system_clock::to_time_t(record.created.value());
        std::string created_str(std::ctime(&created_time));

        nlohmann::json project = {
            {"id",           record.id.value()},
            {"name",         record.name},
            {"label",        record.label},
            {"description",  record.description},
            {"reports",      record.nreports.value()},
            {"types",        record.ntypes.value()},
            {"created",      created_str},
            {"listing",      record.listing.value()},
            {"properties",   nlohmann::json::parse(record.properties.value())},
            {"owner", {
                {"id",    ctx.session().exists<wee::session::user>() ? record.owner.value() : 0},
                {"first", ctx.session().exists<wee::session::user>() ? record.firstName : std::string("VoxPeople")},
                {"last",  ctx.session().exists<wee::session::user>() ? record.lastName : std::string()}
            }}
        };
        spatial_projects.push_back(project);
    }
    
    return {
        {"count",    spatial_projects.size()},
        {"projects", spatial_projects}
    };
}

nlohmann::json wee::apps::project::retrieve(udho::contexts::stateful<wee::session::user> ctx, std::size_t id){
    wee::Projects projects;
    wee::Reports  reports;
    wee::Users    users;
    wee::Types    types;
    auto reportcount = select(reports.project, count(reports.id)).from(reports).unconditionally().group_by(reports.project).as(sqlpp::alias::a);
    auto records = _connection(
        select(
            projects.id, 
            projects.name, 
            projects.label, 
            projects.description, 
            projects.created, 
            projects.properties,
            projects.owner,
            projects.listing,
            users.firstName, 
            users.lastName,
            reportcount.count
        ).from(
            projects.join(users).on(projects.owner == users.id)
                   .left_outer_join(reportcount).on(projects.id == reportcount.project)
        ).order_by(projects.id.asc())
        .where(projects.id == id)
    );
    
    if(!records.size()){
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
    }
    
    auto& record = records.front();
    std::size_t owner = record.owner;
    
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        std::string reason;
        if(loggedin_user.id() != owner && !allowed_to("project:view", id, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }else{
        std::string reason;
        if(!allowed_to("project:view", id, 0, reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }
    
    std::time_t created_time = std::chrono::system_clock::to_time_t(record.created.value());
    std::string created_str(std::ctime(&created_time));
    
    auto types_records = _connection(
        select(
            types.id,
            types.name,
            types.label,
            types.properties
        ).from(types).order_by(types.id.asc())
        .where(types.project == id)
    );
    
    nlohmann::json project_types = nlohmann::json::array();
    for(const auto& type_record: types_records){
        std::string type_properties = type_record.properties.value();
        
        nlohmann::json project_type = {
            {"id",          type_record.id.value()},
            {"name",        type_record.name},
            {"label",       type_record.label},
            {"properties",  type_properties.empty() ? nlohmann::json::object() : nlohmann::json::parse(type_properties)}
        };
        project_types.push_back(project_type);
    }
        
    nlohmann::json project = {
        {"id",           record.id.value()},
        {"name",         record.name},
        {"label",        record.label},
        {"description",  record.description},
        {"reports",      record.count.value()},
        {"types",        project_types},
        {"created",      created_str},
        {"listing",      record.listing.value()},
        {"properties",   nlohmann::json::parse(record.properties.value())},
        {"owner", {
            {"id",    ctx.session().exists<wee::session::user>() ? record.owner.value() : 0},
            {"first", ctx.session().exists<wee::session::user>() ? record.firstName : std::string("VoxPeople")},
            {"last",  ctx.session().exists<wee::session::user>() ? record.lastName : std::string()}
        }}
    };
    
    return {
        {"project", project}
    };
}

nlohmann::json wee::apps::project::create(udho::contexts::stateful<wee::session::user> ctx){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        std::string reason;
        if(!allowed_to("project:create", 0, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
        
        std::string body = ctx.request().body();
        nlohmann::json json = nlohmann::json::parse(body);
        nlohmann::json properties  = json["properties"];
        nlohmann::json permissions = json["permissions"];
        udho::form::required<std::string> name("name", "Name of the type must not be empty");
        udho::form::required<std::string> label("label", "Label must not be empty");
        udho::form::required<std::string> description("description", "Description must not be empty");
        udho::form::required<bool>        listing("listing", "listing must not be empty");
        udho::form::required<std::string> mutate("mutate", "mutate must not be empty");
        udho::form::required<std::string> write("write", "write must not be empty");
        udho::form::required<std::string> read("read", "read must not be empty");
        
        auto validator = udho::form::validate();
        validator << (json.contains("name")  && json["name"] .is_string() ? name(json["name"]  .get<std::string>()) : name());
        validator << (json.contains("label") && json["label"].is_string() ? label(json["label"].get<std::string>()) : label());
        validator << (json.contains("description") && json["description"].is_string() ? description(json["description"].get<std::string>()) : description());
        validator << (permissions.contains("listing") && permissions["listing"].is_boolean() ? listing.unchecked(permissions["listing"].get<bool>()) : listing());
        validator << (permissions.contains("mutate") && permissions["mutate"].is_string() ? mutate(permissions["mutate"].get<std::string>()) : mutate());
        validator << (permissions.contains("write") && permissions["write"].is_string() ? write(permissions["write"].get<std::string>()) : write());
        validator << (permissions.contains("read") && permissions["read"].is_string() ? read(permissions["read"].get<std::string>()) : read());
        
        if(!validator.valid()){
            return {
                {"error", true},
                {"success", false},
                {"message", validator.errors().size() ? validator.errors().front() : std::string("Invalid input")},
                {"details", {
                    {"errors", validator.errors()},
                    {"fields", {
                        {"name", {
                            {"valid", validator["name"].valid()},
                            {"error", validator["name"].error()}
                        }},
                        {"label", {
                            {"valid", validator["label"].valid()},
                            {"error", validator["label"].error()}
                        }},
                        {"description", {
                            {"valid", validator["description"].valid()},
                            {"error", validator["description"].error()}
                        }},
                        {"listing", {
                            {"valid", validator["listing"].valid()},
                            {"error", validator["listing"].error()}
                        }},
                        {"mutate", {
                            {"valid", validator["mutate"].valid()},
                            {"error", validator["mutate"].error()}
                        }},
                        {"write", {
                            {"valid", validator["write"].valid()},
                            {"error", validator["write"].error()}
                        }},
                        {"read", {
                            {"valid", validator["read"].valid()},
                            {"error", validator["read"].error()}
                        }},
                    }}
                }}
            };
        }else{
            wee::Projects projects;
            auto statement = sqlpp::postgresql::insert_into(projects).set(
                projects.name        = name.value(),
                projects.label       = label.value(),
                projects.description = description.value(),
                projects.properties  = properties.dump(),
                projects.owner       = loggedin_user.id(),
                projects.listing     = listing.value()
            ).returning(projects.id);

            const auto results = _connection(statement);
            return {
                {"id", results.front().id.value()},
                {"success", true}
            };
        }
    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}

nlohmann::json wee::apps::project::update(udho::contexts::stateful<wee::session::user> ctx, std::size_t id){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
                
        std::string body = ctx.request().body();
        nlohmann::json json = nlohmann::json::parse(body);
        nlohmann::json properties  = json["properties"];
        nlohmann::json permissions = json["permissions"];
        udho::form::required<std::string> name("name", "Name of the type must not be empty");
        udho::form::required<std::string> label("label", "Label must not be empty");
        udho::form::required<std::string> description("description", "Description must not be empty");
        udho::form::required<bool>        listing("listing", "listing must not be empty");
        
        auto validator = udho::form::validate();
        validator << (json.contains("name")  && json["name"] .is_string() ? name(json["name"]  .get<std::string>()) : name());
        validator << (json.contains("label") && json["label"].is_string() ? label(json["label"].get<std::string>()) : label());
        validator << (json.contains("description") && json["description"].is_string() ? description(json["description"].get<std::string>()) : description());
        validator << (json.contains("listing") && json["listing"].is_boolean() ? listing.unchecked(json["listing"].get<bool>()) : listing());
        
        if(!validator.valid()){
            return {
                {"error", true},
                {"success", false},
                {"message", validator.errors().size() ? validator.errors().front() : std::string("Invalid input")},
                {"details", {
                    {"errors", validator.errors()},
                    {"fields", {
                        {"name", {
                            {"valid", validator["name"].valid()},
                            {"error", validator["name"].error()}
                        }},
                        {"label", {
                            {"valid", validator["label"].valid()},
                            {"error", validator["label"].error()}
                        }},
                        {"description", {
                            {"valid", validator["description"].valid()},
                            {"error", validator["description"].error()}
                        }},
                        {"listing", {
                            {"valid", validator["listing"].valid()},
                            {"error", validator["listing"].error()}
                        }}
                    }}
                }}
            };
        }else{
            wee::Projects projects;
            auto project_records = _connection(select(projects.id, projects.owner).from(projects).where(projects.id == id));
            if(!project_records.size()){
                throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
            }
            const auto& project_record = project_records.front();
            std::size_t owner = project_record.owner;
            std::string reason;
            if(loggedin_user.id() != owner && !allowed_to("project:update", id, loggedin_user.id(), reason)){
                return {
                    {"error", true},
                    {"message", reason}
                };
            }
            
            auto statement =  sqlpp::update(projects).set(
                projects.name        = name.value(),
                projects.label       = label.value(),
                projects.description = description.value(),
                projects.properties  = properties.dump(),
                projects.owner       = loggedin_user.id(),
                projects.listing     = listing.value()
            ).where(projects.id == id);

            _connection(statement);
            
            return {
                {"id", id},
                {"success", true}
            };
        }
    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}

nlohmann::json wee::apps::project::remove(udho::contexts::stateful<wee::session::user> ctx, std::size_t id){}

std::string wee::apps::project::list(udho::contexts::stateful<wee::session::user> ctx){
    wee::Projects projects;
    wee::Reports  reports;
    wee::Users    users;
    wee::Places   places;
    auto reportcount = select(reports.project, count(reports.id)).from(reports).unconditionally().group_by(reports.project).as(sqlpp::alias::a);
    auto placecount = select(places.project, count(places.id)).from(places).unconditionally().group_by(places.project).as(sqlpp::alias::b);
    auto records = _connection(
        select(
            projects.id, 
            projects.name, 
            projects.label, 
            projects.description, 
            projects.created, 
            projects.properties,
            projects.owner,
            projects.listing,
            users.firstName, 
            users.lastName,
            reportcount.count.as(nreports),
            placecount.count.as(nplaces)
        ).from(
            projects.join(users).on(projects.owner == users.id)
                   .left_outer_join(reportcount).on(projects.id == reportcount.project)
                   .left_outer_join(placecount).on(projects.id == placecount.project)
        ).order_by(projects.id.asc())
        .where(projects.listing == true)
    );
    data::projects_list plist;
    for(const auto& record: records){
        const auto& project_record = record;
        
        std::size_t p     = project_record.id;
        std::size_t owner = project_record.owner;

        if(ctx.session().exists<wee::session::user>()){
            wee::session::user loggedin_user;
            ctx.session() >> loggedin_user;
            std::string reason;
            if(loggedin_user.id() != owner && !allowed_to("project:view", p, loggedin_user.id(), reason)){
                continue;
            }
        }else{
            std::string reason;
            if(!allowed_to("project:view", p, 0, reason)){
                continue;
            }
        }
        
        std::time_t created_time = std::chrono::system_clock::to_time_t(record.created.value());
        std::string created_str(std::ctime(&created_time));
        
        nlohmann::json properties = nlohmann::json::parse(record.properties.value());
        
        data::project project(record.id.value(), ctx.session().exists<wee::session::user>());
        project.name             = record.name;
        project.label            = record.label;
        project.nreports         = record.nreports.value();
        project.nplaces          = record.nplaces.value();
        project.description      = record.description;
        project.properties.lat   = properties["center"]["lat"].get<double>();
        project.properties.lng   = properties["center"]["lng"].get<double>();
        project.properties.color = properties["color"].get<std::string>();
        plist.projects.push_back(project);
    }
    
    data::user user(ctx.session().exists<wee::session::user>());
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        user.id   = loggedin_user.id();
        user.name = loggedin_user.name();
    }
    return ctx.render("projects-list.html", plist, user);
}

bool wee::apps::project::allowed_to(const std::string& resource, std::size_t project, std::size_t uid, std::string& reason){
    std::string sql = (boost::format("select * from allowed_to('%1%', %2%, %3%)") % resource % project % uid).str();
    auto statement = custom_query(sqlpp::verbatim(sql))
        .with_result_type_of(select(
            sqlpp::value(false).as(sqlpp::alias::a),
            sqlpp::value("").as(sqlpp::alias::b)
        ));
    auto records = _connection(statement);
    const auto& permission = records.front();
    reason = permission.b.value();
    return permission.a.value();
}

// std::string wee::apps::project::home_tabular(udho::contexts::stateful<wee::session::user> ctx, std::size_t p){
//     wee::Projects projects;
//     wee::Reports  reports;
//     wee::Users    users;
//     wee::Types    types;
//     auto reportcount = select(reports.project, count(reports.id)).from(reports).unconditionally().group_by(reports.project).as(sqlpp::alias::a);
//     auto typescount  = select(types.project, count(types.id)).from(types).unconditionally().group_by(types.project).as(sqlpp::alias::b);
//     auto records = _connection(
//         select(
//             projects.id, 
//             projects.name, 
//             projects.label, 
//             projects.description, 
//             projects.created, 
//             projects.properties,
//             projects.owner,
//             users.firstName, 
//             users.lastName,
//             reportcount.count.as(nreports),
//             typescount.count.as(ntypes)
//         ).from(
//             projects.join(users).on(projects.owner == users.id)
//                    .left_outer_join(reportcount).on(projects.id == reportcount.project)
//                    .left_outer_join(typescount).on(projects.id == typescount.project)
//         ).order_by(projects.id.asc())
//         .where(projects.id == p)
//     );
//     
//     if(!records.size()){
//         throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
//     }
// 
//     auto& record = records.front();
//     
//     std::size_t owner = record.owner;
//     
//     if(ctx.session().exists<wee::session::user>()){
//         wee::session::user loggedin_user;
//         ctx.session() >> loggedin_user;
//         std::string reason;
//         if(loggedin_user.id() != owner && !allowed_to("report:fetch", p, loggedin_user.id(), reason)){
//             throw udho::exceptions::http_error(boost::beast::http::status::forbidden, reason);
//         }
//     }else{
//         std::string reason;
//         if(!allowed_to("report:fetch", p, 0, reason)){
//             throw udho::exceptions::http_error(boost::beast::http::status::forbidden, reason);
//         }
//     }
//     
//     auto types_records = _connection(
//         select(
//             types.id,
//             types.name,
//             types.label,
//             types.properties
//         ).from(types).order_by(types.id.asc())
//         .where(types.project == p)
//     );
//     
//     std::time_t created_time = std::chrono::system_clock::to_time_t(record.created.value());
//     std::string created_str(std::ctime(&created_time));
//         
//     std::string properties = record.properties.value();
//     
//     nlohmann::json project_types = nlohmann::json::array();
//     for(const auto& type_record: types_records){
//         std::string type_properties = type_record.properties.value();
//         
//         nlohmann::json project_type = {
//             {"id",          type_record.id.value()},
//             {"name",        type_record.name},
//             {"label",       type_record.label},
//             {"properties",  type_properties.empty() ? nlohmann::json::object() : nlohmann::json::parse(type_properties)}
//         };
//         project_types.push_back(project_type);
//     }
//     
//     nlohmann::json project_json = {
//         {"id",           record.id.value()},
//         {"name",         record.name},
//         {"label",        record.label},
//         {"description",  record.description},
//         {"reports",      record.nreports.value()},
//         {"created",      created_str},
//         {"types",        project_types},
//         {"properties",   properties.empty() ? nlohmann::json::object() : nlohmann::json::parse(properties)},
//         {"owner", {
//             {"id",    ctx.session().exists<wee::session::user>() ? record.owner.value() : 0},
//             {"first", ctx.session().exists<wee::session::user>() ? record.firstName : std::string("VoxPeople")},
//             {"last",  ctx.session().exists<wee::session::user>() ? record.lastName : std::string()}
//         }},
//     };
//     
//     wee::Places places;
// 
//     
//     data::project project(p, ctx.session().exists<wee::session::user>());
//     project.name  = record.name;
//     project.label = record.label;
//     project.description = record.description;
//     project.synopsis = project_json;
//     
//     auto contributions = select(places.author, count(places.id)).from(places).unconditionally().group_by(places.author).as(sqlpp::alias::a);
//     auto contribution_records = _connection(
//         select(
//             users.id, 
//             users.firstName, 
//             users.lastName, 
//             users.designation, 
//             users.currentDesignation, 
//             contributions.count
//         )
//         .from(users.join(contributions).on(contributions.author == users.id))
//         .where(contributions.count > 1)
//         .order_by(contributions.count.desc())
//     );
//     
//     for(const auto& record: contribution_records){
//         std::string name = record.firstName.value() + " " + record.lastName.value();
//         data::volunteer volunteer(name);
//         volunteer.affiliation = record.designation;
//         volunteer.affiliation2 = record.currentDesignation;
//         volunteer.count = record.count.value();
//         project.volunteers.push_back(volunteer);
//     }
//     
//     auto placecount = select(places.report, count(places.id)).from(places).unconditionally().group_by(places.report).as(sqlpp::alias::a);
//     auto report_records = _connection(
//         select(
//             reports.id, 
//             reports.aoi, 
//             reports.toi, 
//             reports.type, 
//             reports.story, 
//             reports.created, 
//             reports.properties,
//             reports.author,
//             reports.project,
//             users.firstName, 
//             users.lastName,
//             placecount.count,
//             types.name.as(type_name),
//             types.label.as(type_label)
//         ).from(
//             reports.join(users).on(reports.author == users.id)
//                    .left_outer_join(placecount).on(reports.id == placecount.report)
//                    .left_outer_join(types).on(reports.type == types.id)
//         ).order_by(reports.id.asc())
//         .where(reports.project == p && placecount.count.is_null())
//     );
//     
//     data::user user(ctx.session().exists<wee::session::user>());
//     if(ctx.session().exists<wee::session::user>()){
//         wee::session::user loggedin_user;
//         ctx.session() >> loggedin_user;
//         
//         user.id   = loggedin_user.id();
//         user.name = loggedin_user.name();
//     }
//     data::book book;
//     for(const auto& record: report_records){
//         data::report report;
//         report.id = record.id.value();
//         report.aoi = record.aoi.value();
//         report.nplaces = record.count.value();
//         book.reports.push_back(report);
//     }
//     return ctx.render("spatial-reports.html", project, user, book);
// }
