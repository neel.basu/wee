/*
 * Copyright 2020 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef WEE_USER_H
#define WEE_USER_H

#include <nlohmann/json.hpp>
#include <udho/context.h>
#include <udho/watcher.h>
#include <udho/application.h>
#include <sqlpp11/postgresql/connection.h>
#include <sqlpp11/postgresql/exception.h>
#include <sqlpp11/sqlpp11.h>
#include <set>
#include "person.h"

namespace wee{
namespace apps{

struct presence_watch: udho::watch<presence_watch, std::size_t>{
    typedef udho::watch<presence_watch, std::size_t> base;
    
    wee::gathering& _universe;
    sqlpp::postgresql::connection& _connection;
    udho::contexts::stateful<wee::session::user> _context;
    std::size_t _user;
    
    presence_watch(udho::contexts::stateful<wee::session::user> ctx, std::size_t user, wee::gathering& universe, sqlpp::postgresql::connection& connection);
    void operator()(const boost::system::error_code& e);
};
    
/**
 * @todo write docs
 */    
struct user: public udho::application<user>{
    typedef udho::application<user> base;
    typedef udho::watcher<presence_watch> presence_watcher_type;
    
    sqlpp::postgresql::connection& _connection;
    wee::gathering&                _universe;
    presence_watcher_type          _watcher;
        
    user(sqlpp::postgresql::connection& conn, boost::asio::io_service& io, wee::gathering& universe);
    std::string login_view(udho::contexts::stateful<wee::session::user> ctx);
    std::string login_submit_view(udho::contexts::stateful<wee::session::user> ctx);
    std::string register_view(udho::contexts::stateful<wee::session::user> ctx);
    std::string register_submit_view(udho::contexts::stateful<wee::session::user> ctx);
    std::string logout(udho::contexts::stateful<wee::session::user> ctx);
    std::string self(udho::contexts::stateful<wee::session::user> ctx);
    std::string profile(udho::contexts::stateful<wee::session::user> ctx, std::size_t uid);
    void ping(udho::contexts::stateful<wee::session::user> ctx, std::size_t n);
    void updated(const wee::person& person);
    nlohmann::json like(udho::contexts::stateful<wee::session::user> ctx, std::string q);
    nlohmann::json upload(udho::contexts::stateful<wee::session::user> ctx, std::size_t uid);
    
    template <typename RouterT>
    auto route(RouterT& router){
        return router | (get (&user::login_view).html()            = "^/login/?$")
                      | (post(&user::login_submit_view).html()     = "^/login/?$")
                      | (get (&user::register_view).html()         = "^/register/?$")
                      | (post(&user::register_submit_view).html()  = "^/register/?$")
                      | (get (&user::self).html()                  = "^/profile/?$")
                      | (get (&user::profile).html()               = "^/profile/(\\d+)/?$")
                      | (get (&user::logout).html()                = "^/logout/?$")
                      | (get (&user::like).json()                  = "^/like/(.+)?$")
                      | (get (&user::ping).deferred()              = "^/ping/(\\d)/?$")
                      | (post(&user::ping).deferred()              = "^/ping/(\\d)/?$")
                      | (post(&user::upload).json()                = "^/profile/(\\d+)/photo/?$");
    }
    
    
    private:
        bool allowed_to(const std::string& resource, std::size_t project, std::size_t uid, std::string& reason);
};

}
}

#endif // WEE_USER_H
