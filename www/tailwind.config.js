module.exports = {
    purge: {
        enabled: true,
        content: [
            './components/**/*.vue',
            './components/**/*.js',
            './*.html',
            '../tmpl/*.html'
        ],
        whitelist: ['border-pink-500'],
        whitelistPatterns: [/^border\-\w+\-500$/]
    },
    theme: {
        theme: {
            opacity: {
                '0' : '0',
                '10': '.1',
                '20': '.2',
                '25': '.25',
                '30': '.3',
                '40': '.4',
                '50': '.5',
                '60': '.6',
                '70': '.7',
                '75': '.75',
                '80': '.8',
                '90': '.9',
                '100': '1'
            },
            maxWidth: {
                '3xs': '5rem',
                '2xs': '10rem'
            }
        },
        extend: {
            spacing: {
                '72': '18rem',
                '84': '21rem',
                '96': '24rem',
                '108': '27rem',
                '120': '30rem',
                '132': '33rem',
                '144': '36rem'
            }
        },
    },
    variants: {},
    plugins: [
        require('tailwind-percentage-heights-plugin')(),
    ],
}
