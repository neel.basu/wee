/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef WEE_CHAT_H
#define WEE_CHAT_H

#include <nlohmann/json.hpp>
#include <udho/context.h>
#include <udho/watcher.h>
#include <udho/application.h>
#include <sqlpp11/postgresql/connection.h>
#include <sqlpp11/postgresql/exception.h>
#include <sqlpp11/sqlpp11.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>
#include "user.h"
#include "person.h"
#include <set>
#include <queue>

namespace wee{
namespace apps{

struct message{
    std::size_t id;
    std::size_t from;
    std::size_t to;
    std::string content;
    std::string type;
    std::string category;
    boost::posix_time::ptime time;
    
    bool operator<(const message& other)const{return id < other.id;}
};

void to_json(nlohmann::json& j, const message& msg);
void from_json(const nlohmann::json& j, message& p);

typedef boost::multi_index_container<
    message,
    boost::multi_index::indexed_by<
        boost::multi_index::ordered_unique<boost::multi_index::identity  <message>>,
        boost::multi_index::ordered_non_unique<boost::multi_index::member<message, std::size_t, &message::from>>,
        boost::multi_index::ordered_non_unique<boost::multi_index::member<message, std::size_t, &message::to>>,
        boost::multi_index::ordered_non_unique<boost::multi_index::member<message, boost::posix_time::ptime, &message::time>>
    > 
> message_storage_type;
typedef typename message_storage_type::nth_index<1>::type messages_from_storage_type;
typedef typename message_storage_type::nth_index<2>::type messages_to_storage_type;
typedef typename message_storage_type::nth_index<3>::type messages_time_storage_type;

struct message_watch: udho::watch<message_watch, std::size_t>{
    typedef udho::watch<message_watch, std::size_t> base;
    
    std::size_t _last;
    std::size_t _user;
    udho::contexts::stateful<wee::session::user> _context;
    messages_from_storage_type& _messages_from;
    messages_to_storage_type&   _messages_to;
    
    message_watch(udho::contexts::stateful<wee::session::user> ctx, std::size_t last, std::size_t user, messages_from_storage_type& messages_from, messages_to_storage_type& messages_to);
    bool pending() const;
    void operator()(const boost::system::error_code& e);
};
    
/**
 * @todo write docs
 */
struct chat: public udho::application<chat>{
    typedef udho::application<chat> base;
    typedef udho::watcher<message_watch> watcher_type;
    
    sqlpp::postgresql::connection& _connection;
    boost::asio::io_service&       _io;
    wee::gathering&                _universe;
    watcher_type                   _watcher;
    //{ storage
    message_storage_type        _messages;
    messages_to_storage_type&   _messages_by_to;
    messages_from_storage_type& _messages_by_from;
    messages_time_storage_type& _messages_by_time;
    //}
        
    chat(sqlpp::postgresql::connection& conn, boost::asio::io_service& io, wee::gathering& universe);

    std::string home(udho::contexts::stateful<wee::session::user> ctx);
    void ping(udho::contexts::stateful<wee::session::user> ctx, std::size_t last);
    nlohmann::json send(udho::contexts::stateful<wee::session::user> ctx);
    
    template <typename RouterT>
    auto route(RouterT& router){
        return router | (get (&chat::home).html()      = "^$")
                      | (get (&chat::ping).deferred()  = "^/ping/(\\d+)$")
                      | (post(&chat::send).json()      = "^/send$");
    }
};

}
}

#endif // WEE_CHAT_H
