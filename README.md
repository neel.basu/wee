## Build

### arch linux

```
sudo pacman -Syu
sudo pacman -S base-devel git cmake boost icu pugixml postgresql libpqxx npm
```

### Ubuntu 19.10

```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install git cmake build-essential libboost-all-dev libicu-dev libpugixml-dev
```

### Compiling

```
git clone https://gitlab.com/neel.basu/wee.git
cd wee
git checkout develop
git submodule update --init --recursive
mkdir build
cd build
cmake ..
make -j2
cd ../www
npm install
npx webpack
cd ../build
./wee --db-base DATABASE_NAME --db-user DATABASE_USER --db-pass DATABASE_PASSWORD
```

## Setup PostgreSQL Database

#### create locale utf8

Check whether locale `en_US.utf8` is included in the output of 
```
locale -a
```
If utf8 locale is not generate then `sudo vim /etc/locale.gen` and add this line 
```
en_US.UTF-8 UTF-8
```
Save the file and run `sudo locale-gen`
issue `locale -a` again to verify `utf8` locale

#### Install postgreSQL database server

```
sudo -iu postgres
initdb --locale=en_US.UTF-8 -E UTF8 -D /var/lib/postgres/data
exit
sudo systemctl enable postgresql
sudo systemctl start postgresql
sudo systemctl status postgresql
```

#### Database remote connectivity

Open file `/var/lib/postgres/data/postgresql.conf` and change the `listen_address` to `*`. Then restart the postgreSQL server.
Open file `/var/lib/postgres/data/pg_hba.conf` and add the following line
```
# host    all             all             CURRENT_IP_ADDRESS/32        md5
```
Replace `CURRENT_IP_ADDRESS` with teh IP address of the development machine. 
```
sudo systemctl restart postgresql
```

#### Create Database User

First create a `role`

```
sudo -iu postgres
createuser --interactive
```
Now alter the role to add password

```
psql
ALTER USER role_name WITH PASSWORD 'some_password';
exit
```


## DNS server

```
sudo pacman -S named
```
Change options in `/etc/named.conf` if required

```
options {
    directory "/var/named";
    pid-file "/run/named/named.pid";

    // Uncomment these to enable IPv6 connections support
    // IPv4 will still work:
    //  listen-on-v6 { any; };
    // Add this for no IPv4:
    //  listen-on { none; };
    listen-on port 53 { 127.0.0.1; any; };
    allow-query { localhost; any; };
    // allow-recursion { 127.0.0.1; };
    // allow-transfer { none; };
    // allow-update { none; };

    //version none;
    //hostname none;
    //server-id none;
};
```

To turn on logging uncommon the following

```
logging {
    channel xfer-log {
        file "/var/log/named.log";
            print-category yes;
            print-severity yes;
            severity info;
        };
        category xfer-in { xfer-log; };
        category xfer-out { xfer-log; };
        category notify { xfer-log; };
};
```

After the DNS server is installed add the forward zone for `DOMAIN.com` in `/etc/named.conf`
```
zone "DOMAIN.com" IN {
        type master;
        file "DOMAIN.com.zone";
        allow-query { any; };
        allow-update { none; };
        notify no;
};
```
restart the nameserver and check status

```
sudo systemctl restart named
sudo systemctl status named
```

If the logfile is not present then the error will be reported in `status` output. Touch the logfile and chown to `named:root`. 
Then add the forward zone file `/var/named/DOMAIN.com.zone`. Replace `DOMAIN.com` with the domain name and replace `VM_PUBLIC_IP` with the public IP address of the VM.

```
$TTL 7200
; DOMAIN.com
@       IN      SOA     ns1.DOMAIN.com. postmaster.DOMAIN.com. (
                                        2018111111 ; Serial
                                        28800      ; Refresh
                                        1800       ; Retry
                                        604800     ; Expire - 1 week
                                        86400 )    ; Negative Cache TTL
@               IN      NS      ns1.DOMAIN.com.
@               IN      NS      ns2.DOMAIN.com.
DOMAIN.com.                 IN      A       VM_PUBLIC_IP
ns1.DOMAIN.com.             IN      A       VM_PUBLIC_IP
ns2.DOMAIN.com.             IN      A       VM_PUBLIC_IP
ns1     IN      A       VM_PUBLIC_IP
ns2     IN      A       VM_PUBLIC_IP
www     IN      A       VM_PUBLIC_IP
```

in `/etc/resolv.conf` add

```
search DOMAIN.com
```

Check configurations 

```
named-checkconf /etc/named.conf
named-checkzone DOMAIN.com /var/named/DOMAIN.com.zone
```

Restart `named` and try to `ping DOMAIN.com`

## Setup nginx reverse proxy server

Install nginx HTTP server and install `certbot` for generating SSL certificates

```
sudo pacman -S nginx certbot certbot-nginx
sudo systemctl enable nginx
sudo systemctl start nginx
```

Add the server name in `/etc/nginx/nginx.conf`

```
# ...
server {
        server_name  DOMAIN.com localhost ;
        # ...
    }
# ...
```

Generate the SSL certificate and add to the nginx configuration automatically by using `certbot` interactive command

```
certbot --nginx
```

Restart nginx server and check whether the default installation works.
Edit `/etc/nginx/nginx.conf` and add redirection to `wee` by commenting out the `root` and `index` and adding `proxy_pass`

```
location / {
    # root   /usr/share/nginx/html;
    # index  index.html index.htm;
    proxy_pass  http://127.0.0.1:9198/;
}

```
Ensure that the `wee` server must be running and listening on port `9198` by running `./wee` in the build directory created previously.
Restart nginx and test.


Things should be working fine.