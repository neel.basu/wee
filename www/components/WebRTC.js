import axios from 'axios'

/**
 * class MyWebRTC extends WebRTC{
 *     _dial(sdp){
 *         // deliver sdp to peer
 *     }
 * }
 * 
 * let rtc = new MyWebRTC($("#screen"))
 * rtc.dial() 
 * // will call _dial with sdp which is supposed to be delivered to another peer through pegion
 * // The other peer on recieving that sdp 
 */
class WebRTC{
    _conn;
    _local;
    _remote;
    _active;
    _onconnected;
    _ondisconnected;
    _answered;
    _ice_collected;
    _local_stream;
    constructor(local, remote){
        this._local  = local;
        this._remote = remote;
        this._active = false;
        this._onconnected = function(){}
        this._ondisconnected = function(){}
        this._answered = false;
        this._ice_collected = [];
        this._local_stream = null;
    }
    _start(is_caller, callback){
        let self = this;
        navigator.getMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
        navigator.getMedia({video: true, audio: true}, function(stream){
            let config = {'iceServers': [
                {'url': 'stun:stun.services.mozilla.com'}, 
                {'url': 'stun:stun.l.google.com:19302'}
            ]};
            window.RTCPeerConnection = window.RTCPeerConnection || window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
            let connection = new RTCPeerConnection(config);
            connection.onicecandidate = function(event){
                if(event.candidate != null){
                    self.ice(event);
                }
            }
            connection.onaddstream = function(event){
                self.peer(event);
            }
            connection.oniceconnectionstatechange = function() {
                if(connection.iceConnectionState == 'disconnected') {
                    self.reset();
                }
            }
            
            self._local.srcObject = stream;
            if(stream){
                connection.addStream(stream);
                self._local_stream = stream;
            }
            self._conn = connection;
            if(callback){
                callback(connection);
            }
            self._active = true;
        }, function(error){
            console.log(error);
        });
    }
    _dial(sdp){
        // TODO derived class must implement
        //      _dial sends the sdp to the other peer
    }
    stop(){
        this._local_stream.getTracks().forEach(function(track) {
            track.stop();
        });
    }
    get active(){
        return this._active;
    }
    get onconnected(){
        return this._onconnected;
    }
    set onconnected(callback){
        this._onconnected = callback;
    }
    get ondisconnected(){
        return this._ondisconnected;
    }
    set ondisconnected(callback){
        this._ondisconnected = callback;
    }
    dial(){
        let self = this;
        this._start(true, function(connection){
            connection.createOffer().then(function(offer){
                console.log("dial: Setting local descriptor");
                return connection.setLocalDescription(offer);
            }).then(function(){
                console.log("dial: Sending SDP ", JSON.stringify(connection.localDescription));
                self._dial(connection.localDescription);
            }).catch(function(reason){
                console.log(reason);
            });
        });
    }
    _answer(accepted, sdp){
        // TODO derived class must implement
        //      _answer trsnamit the sdp to other peer if accepted
    }
    answer(accepted, sdp){
        if(accepted){
            this._answered = true;
            let self = this;
            this._start(false, function(connection){
                console.log('answer: setting remote descriptor, ', sdp);
                connection.setRemoteDescription(new RTCSessionDescription(sdp)).then(function(){
                    connection.createAnswer().then(function(answer){
                        console.log("answer: Setting local descriptor");
                        return connection.setLocalDescription(answer);
                    }).then(function(){
                        console.log("answer: Sending SDP ", JSON.stringify(connection.localDescription));
                        self._answer(true, connection.localDescription);
                        for(candidate of self._ice_collected){
                            self._ice(candidate);
                        }
                    }).catch(function(reason){
                        console.log(reason);
                    });
                });
            });
        }else{
            this._answer(false);
        }
    }
    accept(sdp){
        console.log("Call Accepted");
        this.answer(true, sdp);
    }
    reject(){
        console.log("Call Rejected");
        this.answer(false);
        this.reset();
    }
    answered(sdp){
        this._conn.setRemoteDescription(new RTCSessionDescription(sdp));
    }
    ice(event){
        if(!this._answered){
            this._ice_collected.push(event.candidate);
        }else{
            this._ice(event.candidate);
        }
    }
    _ice(candidate){
        // TODO derived class must implement
        //      ice transmit the event.candidate to the other peer
    }
    addIce(candidate){
        console.log("adding ICE candidate");
        this._conn.addIceCandidate(candidate);
    }
    peer(event){
        console.log("peer", event);
        this._remote.srcObject = event.stream;
        console.log("calling onconnected", this._onconnected);
        this._onconnected();
    }
    reset(){
        console.log("reset");
        this.stop();
        this._active = false;
        this._answered = false;
        this._ice_collected = [];
        this._remote.srcObject = null;
        this._local.srcObject = null;
        if(this._conn){
            this._conn.close();
            this._conn.onicecandidate = null;
            this._conn.onaddstream = null;
            this._conn.oniceconnectionstatechange = null;
        }
    }
    disconnected(){
        this.reset();
        this._disconnected();
    }
    disconnect(){
        this.disconnected();
    }
    _disconnected(){
        // TODO derived class must implement
        //      transmit a hangup packet to other peer.
    }
}

/**
 * let rtc = new WebRTCConnection($("#screen"))
 * signal.onrcvd = function(msg){
 *     rtc.rcvd(msg);
 * }
 * rtc.dial()
 */
class WebRTCConnection extends WebRTC{
    constructor(local, remote){
        super(local, remote);
    }
    _dial(sdp){
        this._transmit({
            topic: "webrtc", 
            command: "dial", 
            sdp: sdp
        });
    }
    rcvd(message){
        const msg = this._extract(message);
        if(!msg) return;
        if(msg.topic != "webrtc") return;
        console.log("rcvd command ", msg.command);
        if(msg.command == "dial"){
            console.log("dial rcvd -> calling ring")
            this.ring(message);
        }else if(msg.command == "answer"){
            const accepted = this._extract(message).accepted;
            const sdp = this._extract(message).sdp;
            if(accepted){
                this.answered(sdp);
            }else{
                this._ondisconnected();
                this.reset();
            }
        }else if(msg.command == "ice"){
            this.addIce(msg.ice);
        }else if(msg.command == "hangup"){
            this._ondisconnected();
            this.reset();
        }
    }
    ring(message){
        const sdp = this._extract(message).sdp;
        let self = this;
        let cb_accept = function(){
            console.log("call accepted");
            self.accept(sdp);
        }
        let cb_reject = function(){
            self.reject();
        }
        this._ring(message, cb_accept, cb_reject);
    }
    _ring(message, on_accept, on_reject){
        // TODO Derived must implement
        //      This function shows the UI for ringing
        //      On accept or reject in UI it calls the corresponding on_accept and on_reject callbacks
    }
    _answer(accepted, sdp){
        this._transmit({
            topic: "webrtc",
            command: "answer",
            accepted: accepted,
            sdp: accepted ? sdp : false
        });
    }
    _ice(candidate){
        this._transmit({
            topic: "webrtc",
            command: "ice",
            ice: candidate
        });
    }
    _disconnected(){
        this._transmit({
            topic: "webrtc",
            command: "hangup"
        });
    } 
}

/**
 * Instantiate the Communicator, forward all recieved messages to 
 * the communicator if that msg is related to WebRTC
 * \code
 * let communicator = new WebRTCCommunicator({
 *      screen:      $('#dom_element')[0],
 *      extractor:   extractor_callback,
 *      transmitter: tx_callback,
 *      ringer:      ringer_callback
 * });
 * signal.onrcv = function(msg){
 *      if(msg.webrtc_related()){
 *          communicator.rcvd(msg);
 *      }
 * }
 * \endcode
 * To initiate a call
 * \code
 * communicator.dial()
 * \endcode
 * The communicator will call the tx_callback with the dial packet
 * the signaling system (text chat) needs to deliver that packet to
 * the appropriate recipient.
 * 
 * After the recipient recieves that dial packet it calls the ringer_callback
 * which is supposed to show a ringer UI with accept and reject buttons
 * Once the accept button is pressed the on_accept callback passed to the 
 * ringer_callback has to be called.
 */
class WebRTCCommunicator extends WebRTCConnection{
    _extractor;
    _transmitter;
    _ringer;
    constructor(config){
        super(config.local, config.remote);
        this._extractor   = config.extractor;
        this._transmitter = config.transmitter;
        this._ringer      = config.ringer;
    }
    _extract(message){
        return this._extractor(message);
    }
    _transmit(packet){
        this._transmitter(packet);
    }
    _ring(message, on_accept, on_reject){
        this._ringer(message, on_accept, on_reject);
    }
}

export {WebRTC, WebRTCConnection, WebRTCCommunicator}
