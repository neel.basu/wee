CREATE TABLE types
(
    id bigserial NOT NULL,
    name text NOT NULL,
    label text NOT NULL,
    project bigint NOT NULL,
    properties text
);
