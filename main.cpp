#include <iostream>
#include <boost/asio.hpp>
#include <udho/router.h>
#include <udho/logging.h>
#include <udho/server.h>
#include <udho/context.h>
#include <udho/application.h>
#include <udho/visitor.h>
#include <udho/logging.h>
#include <nlohmann/json.hpp>
#include <boost/program_options.hpp>
#include "user.h"
#include "chat.h"
#include "spatial.h"
#include "person.h"
#include "report.h"
#include "place.h"
#include "type.h"
#include "project.h"
#include "privileges.h"
#include "admin.h"
#include <sqlpp11/postgresql/connection.h>
#include <sqlpp11/postgresql/exception.h>
#include <sqlpp11/sqlpp11.h>
#include <boost/thread.hpp>
#include <udho/configuration.h>
#include <Magick++.h> 

int main(int argc, char** argv){
    std::size_t max_threads = boost::thread::hardware_concurrency();
    
    boost::program_options::options_description desc;
    desc.add_options()
        ("help,h",    "produce help")
        ("threads,t", boost::program_options::value<unsigned>()->default_value(max_threads),    "number of threads")
        ("port,p",    boost::program_options::value<unsigned>()->default_value(9198),           "http server port")
        ("db-base",   boost::program_options::value<std::string>()->default_value("wee"),       "database name")
        ("db-host",   boost::program_options::value<std::string>()->default_value("localhost"), "database server hostname")
        ("db-user",   boost::program_options::value<std::string>()->default_value("postgres"),  "database username")
        ("db-pass",   boost::program_options::value<std::string>(),                             "database user password");
        
    boost::program_options::variables_map arguments;
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), arguments);
    boost::program_options::notify(arguments);
    
    if (arguments.count("help")){
        std::cout << desc << std::endl;
        return 0;
    }else if(!arguments.count("db-pass")){
        std::cout << "Please provide password with --db-pass" << std::endl;
        return -1;
    }
    
    Magick::InitializeMagick(*argv);
    
    auto config = std::make_shared<sqlpp::postgresql::connection_config>();
    config->host     = arguments["db-host"].as<std::string>();
    config->user     = arguments["db-user"].as<std::string>();
    config->password = arguments["db-pass"].as<std::string>();
    config->dbname   = arguments["db-base"].as<std::string>();
    sqlpp::postgresql::connection db(config);
    
    unsigned port    = arguments["port"].as<unsigned>();
    unsigned threads = arguments["threads"].as<unsigned>();
    if(threads < 1){
        threads = 1;
    }
    
    boost::asio::io_service io;
    udho::servers::ostreamed::stateful<wee::session::user> server(io, std::cout);
    server[udho::configs::server::template_root] = TMPL_PATH;
    server[udho::configs::server::document_root] = WWW_PATH;
    
    server += udho::logging::features::colored(true);

    wee::gathering        universe(io);
    wee::apps::user       user(db, io, universe);
    wee::apps::chat       chat(db, io, universe);
    wee::apps::spatial    spatial(db);
    wee::apps::report     report(db);
    wee::apps::place      place(db);
    wee::apps::type       type(db);
    wee::apps::project    project(db);
    wee::apps::privileges privileges(db);
    wee::apps::admin      admin(db);
    
    auto router = udho::router()
        | (udho::app<wee::apps::user>(user)             = "^/user")
        | (udho::app<wee::apps::chat>(chat)             = "^/chat")
        | (udho::app<wee::apps::spatial>(spatial)       = "^/spatial")
        | (udho::app<wee::apps::report>(report)         = "^/reports")
        | (udho::app<wee::apps::place>(place)           = "^/places")
        | (udho::app<wee::apps::type>(type)             = "^/types")
        | (udho::app<wee::apps::project>(project)       = "^/projects")
        | (udho::app<wee::apps::privileges>(privileges) = "^/privileges")
        | (udho::app<wee::apps::admin>(admin)           = "^/admin")
        | (udho::get(udho::reroute("/projects/covid")).raw() = "^/$");

    server.serve(router, port);
    
    std::cout << boost::format("udho server running with %1% threads on port %2%") % threads % port << std::endl;
    router /= udho::visitors::print<udho::visitors::visitable::both, std::ostream>(std::cout);

    if(threads > 1){
        boost::thread_group pool;
        for (unsigned i = 0; i < threads; ++i){
            pool.create_thread(boost::bind(&boost::asio::io_service::run, &io));
        }
        pool.join_all();
    }else{
        io.run();
    }
    
    return 0;
}


