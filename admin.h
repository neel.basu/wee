/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef WEE_APPS_ADMIN_H
#define WEE_APPS_ADMIN_H

#include <nlohmann/json.hpp>
#include <udho/context.h>
#include <udho/application.h>
#include <sqlpp11/postgresql/connection.h>
#include <sqlpp11/postgresql/exception.h>
#include <sqlpp11/sqlpp11.h>
#include "person.h"
#include "members.h"

namespace wee{
namespace apps{

/**
 * @todo write docs
 */
struct admin: public udho::application<admin>{
    typedef udho::application<admin> base;
    
    sqlpp::postgresql::connection& _connection;
    
    admin(sqlpp::postgresql::connection& conn);
    
    nlohmann::json users(udho::contexts::stateful<wee::session::user> ctx);
    nlohmann::json users_save(udho::contexts::stateful<wee::session::user> ctx, std::size_t id, std::size_t rank);
    std::string users_view(udho::contexts::stateful<wee::session::user> ctx);
    nlohmann::json mutables(udho::contexts::stateful<wee::session::user> ctx);
    std::string projects(udho::contexts::stateful<wee::session::user> ctx);
    
    template <typename RouterT>
    auto route(RouterT& router){
        return router | (get (&admin::users).json()       = "^/users/fetch/?$")
                      | (post(&admin::users_save).json()  = "^/users/save/(\\d+)/(\\d+)/?$")
                      | (get (&admin::users_view).html()  = "^/users/?$")
                      | (get (&admin::mutables).json()    = "^/mutables/?$") 
                      | (get (&admin::projects).html()    = "^/projects/?$");
    }
    
    private:
        bool allowed_to(const std::string& resource, std::size_t project, std::size_t uid, std::string& reason);
};

}
}

#endif // WEE_APPS_ADMIN_H
