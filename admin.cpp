/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "admin.h"
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/split.hpp>
#include <sqlpp11/value_or_null.h>
#include <sqlpp11/remove.h>
#include <sqlpp11/postgresql/insert.h>
#include <sqlpp11/postgresql/update.h>
#include <sqlpp11/postgresql/returning.h>
#include <sqlpp11/group_by.h>
#include <udho/util.h>
#include <boost/regex.hpp>
#include <xlnt/xlnt.hpp>
#include "ddl/reports.h"
#include "ddl/users.h"
#include "ddl/ranks.h"
#include "ddl/places.h"
#include "ddl/projects.h"
#include "ddl/types.h"
#include "ddl/memberships.h"
#include "data.h"

SQLPP_ALIAS_PROVIDER(nreports);
SQLPP_ALIAS_PROVIDER(nplaces);
SQLPP_ALIAS_PROVIDER(ntypes);

wee::apps::admin::admin(sqlpp::postgresql::connection& conn): _connection(conn), base("admin"){}

nlohmann::json wee::apps::admin::users(udho::contexts::stateful<wee::session::user> ctx){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
    
        std::string reason;
        if(!allowed_to("user:fetch", 0, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
        
        nlohmann::json users_json = nlohmann::json::array();
        wee::Users users;
        auto records = _connection(select(users.id, users.firstName, users.lastName, users.photo, users.rank).from(users).unconditionally());
        for(const auto& record: records){
            users_json.push_back({
                {"id",    record.id.value()},
                {"first", record.firstName},
                {"last",  record.lastName},
                {"rank",  record.rank.value()},
                {"photo", record.photo}
            });
        }
        wee::Ranks ranks;
        auto rank_records = _connection(select(ranks.id, ranks.name, ranks.order, ranks.permissive).from(ranks).order_by(ranks.order.asc()).unconditionally());
        nlohmann::json ranks_json = nlohmann::json::array();
        for(const auto& record: rank_records){
            ranks_json.push_back({
                {"id",         record.id.value()},
                {"name",       record.name.value()},
                {"order",      record.order.value()},
                {"permissive", record.permissive.value()}
            });
        }
        
        return {
            {"users", users_json},
            {"ranks", ranks_json}
        };
        
    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}

nlohmann::json wee::apps::admin::users_save(udho::contexts::stateful<wee::session::user> ctx, std::size_t id, std::size_t rank){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
    
        std::string reason;
        if(!allowed_to("user:update", 0, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
        
        wee::Users users;
        
        _connection(update(users).set(
            users.rank = rank
        ).where(users.id == id));
        
        return {
            {"success", true},
            {"rank",    rank}
        };
        
    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}

std::string wee::apps::admin::users_view(udho::contexts::stateful<wee::session::user> ctx){
    data::user user(ctx.session().exists<wee::session::user>());
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        user.id   = loggedin_user.id();
        user.name = loggedin_user.name();
        
        return ctx.render("users.html", user);
    }else{
        throw udho::exceptions::http_redirection("/user/login");
    }
}

nlohmann::json wee::apps::admin::mutables(udho::contexts::stateful<wee::session::user> ctx){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        wee::Projects    projects;
        wee::Reports     reports;
        wee::Users       users;
        wee::Types       types;
        wee::Memberships memberships;
        auto reportcount = select(reports.project, count(reports.id)).from(reports).unconditionally().group_by(reports.project).as(sqlpp::alias::a);
        auto typecount = select(types.project, count(types.id)).from(types).unconditionally().group_by(types.project).as(sqlpp::alias::b);
        auto records = _connection(
            select(
                projects.id, 
                projects.name, 
                projects.label, 
                projects.description, 
                projects.created, 
                projects.properties,
                projects.owner,
                projects.listing,
                users.firstName, 
                users.lastName,
                reportcount.count.as(nreports),
                typecount.count.as(ntypes)
            ).from(
                projects.join(users).on(projects.owner == users.id)
                    .left_outer_join(reportcount).on(projects.id == reportcount.project)
                    .left_outer_join(typecount).on(projects.id == typecount.project)
                    .left_outer_join(memberships).on(projects.id == memberships.project)
            ).order_by(projects.id.asc())
            .where((memberships.user == loggedin_user.id() or memberships.user.is_null()))
        );
        nlohmann::json spatial_projects = nlohmann::json::array();
        for(const auto& record: records){
            std::size_t p     = record.id;
            std::size_t owner = record.owner;

            if(ctx.session().exists<wee::session::user>()){
                wee::session::user loggedin_user;
                ctx.session() >> loggedin_user;
                std::string reason;
                if(loggedin_user.id() != owner && !allowed_to("project:mutate", p, loggedin_user.id(), reason)){
                    continue;
                }
            }else{
                std::string reason;
                if(!allowed_to("project:mutate", p, 0, reason)){
                    continue;
                }
            }
            
            std::time_t created_time = std::chrono::system_clock::to_time_t(record.created.value());
            std::string created_str(std::ctime(&created_time));

            nlohmann::json project = {
                {"id",           record.id.value()},
                {"name",         record.name},
                {"label",        record.label},
                {"description",  record.description},
                {"reports",      record.nreports.value()},
                {"types",        record.ntypes.value()},
                {"created",      created_str},
                {"listing",      record.listing.value()},
                {"properties",   nlohmann::json::parse(record.properties.value())},
                {"owner", {
                    {"id",    ctx.session().exists<wee::session::user>() ? record.owner.value() : 0},
                    {"first", ctx.session().exists<wee::session::user>() ? record.firstName : std::string("VoxPeople")},
                    {"last",  ctx.session().exists<wee::session::user>() ? record.lastName : std::string()}
                }}
            };
            spatial_projects.push_back(project);
        }
        
        return {
            {"count",    spatial_projects.size()},
            {"projects", spatial_projects}
        };
    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}

std::string wee::apps::admin::projects(udho::contexts::stateful<wee::session::user> ctx){
    data::user user(ctx.session().exists<wee::session::user>());
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        user.id   = loggedin_user.id();
        user.name = loggedin_user.name();
        
        return ctx.render("projects.html", user);
    }else{
        throw udho::exceptions::http_redirection("/user/login");
    }
}

bool wee::apps::admin::allowed_to(const std::string& resource, std::size_t project, std::size_t uid, std::string& reason){
    std::string sql = (boost::format("select * from allowed_to('%1%', %2%, %3%)") % resource % project % uid).str();
    auto statement = custom_query(sqlpp::verbatim(sql))
        .with_result_type_of(select(
            sqlpp::value(false).as(sqlpp::alias::a),
            sqlpp::value("").as(sqlpp::alias::b)
        ));
    auto records = _connection(statement);
    const auto& permission = records.front();
    reason = permission.b.value();
    return permission.a.value();
}
