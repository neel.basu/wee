import Vue from 'vue'
import WeeNav from './WeeNav'
import WeeNavItem from './WeeNavItem'
// import WeeSpatialView from './WeeSpatialView'
import WeeSpatialCanvasView from './WeeSpatialCanvasView'
import WeeSpatialReportBrowser from './WeeSpatialReportBrowser'
import WeeSpatialReportViewer from './WeeSpatialReportViewer'
import VueDisqus from 'vue-disqus'
import {ThemeMixin} from './themes.js';
import { loadProgressBar } from 'axios-progress-bar'
import 'axios-progress-bar/dist/nprogress.css';
    
loadProgressBar();

// Vue.config.devtools = true

const convertArrayToObject = (array, key) => {
  const initialValue = {};
  return array.reduce((obj, item) => {
    return {
      ...obj,
      [item[key]]: item,
    };
  }, initialValue);
};

new Vue({
    el: '#main',
    data(){
        const default_color = synopsis.properties.color;
        return {
            theme: {
                color: default_color,
                types: convertArrayToObject(synopsis.types, 'id')
            },
            tags: {
                universe: synopsis.properties.universe,
                common:   synopsis.properties.common
            },
            browsing:  false,
            viewing:   false,
            commenting: false,
            disqusLoaded: false,
            capturing: false,
            exploring: false,
            notice: false
        };
    },
    components: {
        WeeNav,
        WeeNavItem,
        // WeeSpatialView,
        WeeSpatialCanvasView,
        WeeSpatialReportBrowser,
        WeeSpatialReportViewer
    },
    mixins: [ThemeMixin],
    mounted: function(){},
    methods: {
        selected(selection){
            this.$refs.browser.selected(selection);
            this.$refs.viewer.selected(selection);
        },
        go(coordinate){
            this.$refs.map.focus(coordinate);
        },
        explore(flag){
            this.exploring  = flag;
            this.capturing  = false;
            this.browsing   = false;
            this.viewing    = false;
            this.commenting = false;
            this.exploring  = false;
        },
        search(place){
            this.$refs.map.query(place);
        },
        capture(mode, id = 0){
            this.$refs.map.startCapturing(mode);
            if(id){
                this.$refs.map.makeEditable(id);
            }
            this.capturing  = true;
            this.commenting = false;
            this.exploring  = false;
        },
        captured(){
            this.$refs.browser.captured();
            this.capturing = false;
        },
        browse(){
            this.browsing   = true;
            this.viewing    = false;
            this.commenting = false;
            this.capturing  = false;
            this.exploring  = false;
        },
        closeBrowser(){
            this.browsing = false;
        },
        closeViewer(){
            this.viewing = false;
        },
        view(id){
            console.log('spatial::view', id);
            this.viewing = true;
            this.$refs.viewer.report = id;
            this.browsing   = false;
            this.commenting = false;
            this.capturing  = false;
            this.exploring  = false;
        },
        pull(){
            this.$refs.map.reload();
        },
        update(id){
            this.$refs.map.update(id);
        },
        deplaced(id){
            this.$refs.map.remove(id);
        },
        comments(){
            this.commenting = true;
            if(!this.disqusLoaded){
                disqusLoader(this.$refs.disqus, {
                    scriptUrl: '//http-voxpeople-org.disqus.com/embed.js',
                    disqusConfig: function(){
                        this.page.identifier 	= '/spatial/'+synopsis.id;
                        this.page.url			= 'https://voxpeople.org/spatial/'+synopsis.id;
                    }
                });
                this.disqusLoaded = true;
            }
        },
        closeComments(){
            this.commenting = false;
        },
        showNotice(){
            this.notice = true;
        },
        closeNotice(){
            this.notice = false;
        }
    }
});
