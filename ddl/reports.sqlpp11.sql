CREATE TABLE reports
(
    id bigserial NOT NULL,
    story text,
    aoi text NOT NULL,
    type bigint NOT NULL,
    toi timestamp without time zone NOT NULL,
    properties text,
    author bigint,
    created timestamp without time zone,
    PRIMARY KEY (id)
)
