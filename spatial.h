/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef WEE_SPATIAL_H
#define WEE_SPATIAL_H

#include <nlohmann/json.hpp>
#include <udho/context.h>
#include <udho/application.h>
#include <sqlpp11/postgresql/connection.h>
#include <sqlpp11/postgresql/exception.h>
#include <sqlpp11/sqlpp11.h>
#include "person.h"

namespace wee{
namespace apps{
/**
 * @todo write docs
 */
struct spatial: public udho::application<spatial>{
    typedef udho::application<spatial> base;
    
    sqlpp::postgresql::connection& _connection;
    
    spatial(sqlpp::postgresql::connection& conn);
    nlohmann::json search(udho::contexts::stateful<wee::session::user> ctx, std::string keyword);
    nlohmann::json match(udho::contexts::stateful<wee::session::user> ctx);
    std::string thumbnail(udho::contexts::stateless ctx, double lat, double lng, std::size_t width, std::size_t height, std::size_t zoom);
    
    template <typename RouterT>
    auto route(RouterT& router){
        return router | (get (&spatial::search).json()   = "^/search/(.+)/?$")
                      | (post(&spatial::match).json()    = "^/match/?$")
                      | (get (&spatial::thumbnail).mimed("image/png") = "^/thumb/(\\d+\\.\\d+),(\\d+\\.\\d+)/(\\d+)x(\\d+)/z(\\d+).png$");
    }
    
    private:
        nlohmann::json search_mapbox(std::string keyword);
        nlohmann::json search_google(std::string keyword);
        nlohmann::json match_mapbox(nlohmann::json points);
        nlohmann::json match_google(nlohmann::json points);
};

}
}

#endif // WEE_SPATIAL_H
