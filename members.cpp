/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "members.h"
#include "ddl/users.h"
#include "ddl/projects.h"
#include "ddl/memberships.h"
#include "ddl/project_memberships.h"
#include "ddl/privileges.h"
#include "ddl/permissions.h"
#include "ddl/roles.h"
#include "ddl/ranks.h"
#include <sqlpp11/value_or_null.h>
#include <sqlpp11/remove.h>
#include <sqlpp11/postgresql/insert.h>
#include <sqlpp11/postgresql/update.h>
#include <sqlpp11/postgresql/returning.h>
#include <sqlpp11/group_by.h>

wee::apps::members::members(sqlpp::postgresql::connection& conn): base("members"), _connection(conn){}

nlohmann::json wee::apps::members::overview(udho::contexts::stateful<wee::session::user> ctx, std::size_t project){
    wee::Projects projects;
    auto project_records = _connection(select(projects.id, projects.owner).from(projects).where(projects.id == project));
    if(!project_records.size()){
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
    }
    std::size_t owner = project_records.front().owner;
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        std::string reason;
        if(loggedin_user.id() != owner && !allowed_to("project:mutate", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }else{
        std::string reason;
        if(!allowed_to("project:mutate", project, 0, reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }
    
    wee::Users users;
    wee::Roles roles;
    wee::ProjectMemberships project_memberships;
    
    auto role_records = _connection(select(roles.id, roles.name, roles.order).from(roles).order_by(roles.order.asc()).unconditionally());
    
    auto membership_records = _connection(select(
        project_memberships.user,
        project_memberships.role,
        users.firstName,
        users.lastName,
        project_memberships.membershipId,
        project_memberships.since
    ).from(project_memberships.join(users).on(users.id == project_memberships.user)).where(project_memberships.project == project));
    
    nlohmann::json roles_json = nlohmann::json::array();
    for(const auto& record: role_records){
        roles_json.push_back({
            {"id",    record.id.value()},
            {"name",  record.name.value()},
            {"order", record.order.value()}
        });
    }
    
    nlohmann::json memberships_json = nlohmann::json::array();
    std::size_t counter = 0;
    for(const auto& record: membership_records){
        std::time_t created_time = std::chrono::system_clock::to_time_t(record.since.value());
        std::string created_str(std::ctime(&created_time));
        memberships_json.push_back({
            {"membership",  record.membershipId.value()},
            {"seq",         ++counter},
            {"user", {
                {"id",      record.user.value()},
                {"first",   record.firstName.value()},
                {"last",    record.lastName.value()}
            }},
            {"role",        record.role.value()},
            {"since",       created_str}
        });
    }
    
    return {
        {"memberships", memberships_json},
        {"roles", roles_json},
    };
}

nlohmann::json wee::apps::members::retrieve(udho::contexts::stateful<wee::session::user> ctx, std::size_t project){
    wee::Projects projects;
    auto project_records = _connection(select(projects.id, projects.owner).from(projects).where(projects.id == project));
    if(!project_records.size()){
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
    }
    std::size_t owner = project_records.front().owner;
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        std::string reason;
        if(loggedin_user.id() != owner && !allowed_to("member:fetch", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }else{
        std::string reason;
        if(!allowed_to("member:fetch", project, 0, reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }
    
    wee::Memberships memberships;
    wee::Users       users;
    auto records = _connection(
        select(
            memberships.id,
            memberships.user,
            memberships.recruiter,
            memberships.since,
            users.firstName, 
            users.lastName
        ).from(
            memberships.join(users).on(users.id == memberships.user)
        ).order_by(memberships.since.asc())
        .where(memberships.project == project)
    );
    
    nlohmann::json project_memberships = nlohmann::json::array();
    for(const auto& record: records){
        std::time_t created_time = std::chrono::system_clock::to_time_t(record.since.value());
        std::string created_str(std::ctime(&created_time));
        
        nlohmann::json membership = {
            {"id", record.id.value()},
            {"user", {
                {"id", record.user.value()},
                {"first", record.firstName},
                {"last", record.lastName}
            }},
            {"since", created_str}
        };
        project_memberships.push_back(membership);
    }
    
    return {
        {"memberships", project_memberships}
    };
}

nlohmann::json wee::apps::members::create(udho::contexts::stateful<wee::session::user> ctx, std::size_t project, std::size_t user){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        wee::Projects projects;
        auto project_records = _connection(select(projects.id, projects.owner).from(projects).where(projects.id == project));
        if(!project_records.size()){
            throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
        }
        std::size_t owner = project_records.front().owner;
        std::string reason;
        if(loggedin_user.id() != owner && !allowed_to("member:create", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
        if(loggedin_user.id() != owner && !allowed_to("project:member", project, user, reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
        
        std::string body = ctx.request().body();
        
        wee::Memberships memberships;
        auto statement = sqlpp::postgresql::insert_into(memberships).set(
            memberships.project   = project,
            memberships.user      = user,
            memberships.recruiter = loggedin_user.id()
        ).returning(memberships.id);

        const auto results = _connection(statement);
        return {
            {"id", results.front().id.value()},
            {"success", true}
        };
    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}

nlohmann::json wee::apps::members::remove(udho::contexts::stateful<wee::session::user> ctx, std::size_t project, std::size_t user, std::size_t id){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        wee::Projects projects;
        auto project_records = _connection(select(projects.id, projects.owner).from(projects).where(projects.id == project));
        if(!project_records.size()){
            throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
        }
        std::size_t owner = project_records.front().owner;
        std::string reason;
        if(loggedin_user.id() != owner && !allowed_to("member:delete", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
        
        wee::Memberships memberships;
        auto statement =  remove_from(memberships).where(memberships.project == project and memberships.user == user and memberships.id == id);

        _connection(statement);
        return {
            {"id", id},
            {"success", true}
        };
    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}

bool wee::apps::members::allowed_to(const std::string& resource, std::size_t project, std::size_t uid, std::string& reason){
    std::string sql = (boost::format("select * from allowed_to('%1%', %2%, %3%)") % resource % project % uid).str();
    auto statement = custom_query(sqlpp::verbatim(sql))
        .with_result_type_of(select(
            sqlpp::value(false).as(sqlpp::alias::a),
            sqlpp::value("").as(sqlpp::alias::b)
        ));
    auto records = _connection(statement);
    const auto& permission = records.front();
    reason = permission.b.value();
    return permission.a.value();
}
