CREATE TABLE memberships
(
    id bigserial NOT NULL,
    project bigint NOT NULL,
    "user" bigint NOT NULL,
    recruiter bigint NOT NULL,
    since timestamp without time zone NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id)
);
