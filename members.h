/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef WEE_MEMBERS_H
#define WEE_MEMBERS_H

#include <nlohmann/json.hpp>
#include <udho/context.h>
#include <udho/application.h>
#include <sqlpp11/postgresql/connection.h>
#include <sqlpp11/postgresql/exception.h>
#include <sqlpp11/sqlpp11.h>
#include "person.h"

namespace wee{
namespace apps{
    
/**
 * @todo write docs
 */
struct members: public udho::application<members>{
    typedef udho::application<members> base;
    
    sqlpp::postgresql::connection& _connection;
    
    members(sqlpp::postgresql::connection& conn);
    
    nlohmann::json overview(udho::contexts::stateful<wee::session::user> ctx, std::size_t project);
    nlohmann::json retrieve(udho::contexts::stateful<wee::session::user> ctx, std::size_t project);
    nlohmann::json create(udho::contexts::stateful<wee::session::user> ctx, std::size_t project, std::size_t user);
    nlohmann::json remove(udho::contexts::stateful<wee::session::user> ctx, std::size_t project, std::size_t user, std::size_t id);
    
    template <typename RouterT>
    auto route(RouterT& router){
        return router | (get (&members::overview).json()     = "^/(\\d+)/overview/?$")
                      | (get (&members::retrieve).json()     = "^/(\\d+)/?$")
                      | (post(&members::create).json()       = "^/(\\d+)/create/(\\d+)/?$")
                      | (post(&members::remove).json()       = "^/(\\d+)/remove/(\\d+)/(\\d+)/?$");
    }
    
    private:
        bool allowed_to(const std::string& resource, std::size_t project, std::size_t uid, std::string& reason);
};
    
}
}

#endif // WEE_MEMBERS_H
