/*
 * Copyright 2020 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "user.h"
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <udho/access.h>
#include "ddl/users.h"
#include "ddl/reports.h"
#include "ddl/places.h"
#include "data.h"
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <Magick++.h> 
#include <sqlpp11/postgresql/update.h>

SQLPP_ALIAS_PROVIDER(nreports);
SQLPP_ALIAS_PROVIDER(nplaces);

wee::apps::presence_watch::presence_watch(udho::contexts::stateful<wee::session::user> ctx, std::size_t user, wee::gathering& universe, sqlpp::postgresql::connection& connection)
    : base(user), 
    _universe(universe), 
    _connection(connection), 
    _context(ctx), 
    _user(user){}

void wee::apps::presence_watch::operator()(const boost::system::error_code& e){
    if(_context.session().exists<wee::session::user>()){
        wee::Users user_table;
        try{
            auto records = _connection(
                select(user_table.id, user_table.firstName, user_table.lastName, user_table.phone)
                .from(user_table).unconditionally()
            );
            nlohmann::json users = nlohmann::json::array();
            for(auto& record: records){
                std::string first_name = record.firstName;
                std::string last_name  = record.lastName;
                std::string name       = first_name+" "+last_name;
                std::size_t id         = record.id;
                
                wee::person::status status = _universe.exists(id) ? _universe[id].state() : wee::person::status::offline;
                std::string seen = _universe.exists(id) ? boost::lexical_cast<std::string>(boost::posix_time::second_clock::local_time() - _universe[id].seen()) : std::string();
                nlohmann::json user = {
                    {"name", name},
                    {"id", id},
                    {"state", wee::person::status_str(status)},
                    {"seen", seen}
                };
                users.push_back(user);
            }
            nlohmann::json content = {
                {"users", users}
            };
            _context.respond(content, "text/json");
        }catch(const std::exception& ex){
            _context << udho::exceptions::http_error(boost::beast::http::status::service_unavailable, ex.what());
        }
    }else{
        _context << udho::exceptions::http_error(boost::beast::http::status::forbidden, "Login required");
    }
}

wee::apps::user::user(sqlpp::postgresql::connection& conn, boost::asio::io_service& io, wee::gathering& universe): base("user"), _connection(conn), _universe(universe), _watcher(io, boost::posix_time::seconds(15)){
    _universe.updated.connect(boost::bind(&wee::apps::user::updated, this, _1));
}

std::string wee::apps::user::register_view(udho::contexts::stateful<wee::session::user> ctx) {
    if(!ctx.query().has("secret")){
        throw udho::exceptions::http_redirection("/user/login");
    }else if(ctx.query().field<std::string>("secret") != "42"){
        throw udho::exceptions::http_redirection("/user/login");
    }
    
    auto validator = udho::form::validate(ctx.form());
    return ctx.render("registration_view.html", validator);
}

std::string wee::apps::user::register_submit_view(udho::contexts::stateful<wee::session::user> ctx) {
    if(!ctx.query().has("secret")){
        throw udho::exceptions::http_redirection("/user/login");
    }else if(ctx.query().field<std::string>("secret") != "42"){
        throw udho::exceptions::http_redirection("/user/login");
    }
    
    udho::form::required<std::string> first("first_name", "Please enter your first name");
    first.constrain(udho::form::validators::max_length(20, "First Name must be less than 20 characters"))
         .constrain(udho::form::validators::no_space("First Name must not contain space"));
         
    udho::form::required<std::string> last("last_name", "Please enter your last name");
    last.constrain(udho::form::validators::max_length(25, "Last Name must be less than 25 characters"));
    
    udho::form::required<std::string> phone("phone", "Please enter your 10 digit mobile number");
    phone.constrain(udho::form::validators::exact_length(10, "Mobile number must be exactly 10 digit"))
         .constrain(udho::form::validators::all_digit("Mobile number contains invalid characters"));
    
    udho::form::required<std::string> pass("pass", "Please enter a password");
    pass.constrain(udho::form::validators::min_length(6, "Please enter at least 6 character password"))
        .constrain(udho::form::validators::no_space("Password must not contain space"));
    
    auto validator = udho::form::validate(ctx.form());
    validator << first << last << phone << pass;
    
    if(validator.valid()){
        wee::Users user_table;
        _connection(
            insert_into(user_table).set(
                user_table.firstName = first.value(), 
                user_table.lastName  = last.value(), 
                user_table.phone     = phone.value(), 
                user_table.pass      = pass.value()
            )
        );
    }
    
    return ctx.render("registration_view.html", validator);
}

std::string wee::apps::user::logout(udho::contexts::stateful<wee::session::user> ctx){
    ctx.session().remove();
    throw udho::exceptions::http_redirection("/");
}

std::string wee::apps::user::login_view(udho::contexts::stateful<wee::session::user> ctx) {
    auto validator = udho::form::validate(ctx.form());
    return ctx.render("login.html", validator);
}

std::string wee::apps::user::login_submit_view(udho::contexts::stateful<wee::session::user> ctx) {
    udho::form::required<std::string> phone("phone", "Please enter your 10 digit mobile number");
    phone.constrain(udho::form::validators::exact_length(10, "Mobile number must be exactly 10 digit"))
         .constrain(udho::form::validators::all_digit("Mobile number contains invalid characters"));
    
    udho::form::required<std::string> pass("pass", "Please enter a password");
    pass.constrain(udho::form::validators::min_length(6, "Please enter at least 6 character password"))
        .constrain(udho::form::validators::no_space("Password must not contain space"));
        
    auto validator = udho::form::validate(ctx.form());
    validator << phone << pass;
    
    if(validator.valid()){
        std::string login_phone = phone.value();
        std::string login_passd = pass.value();
        wee::Users user_table;
        
        auto records = _connection(
            select(user_table.id, user_table.firstName, user_table.lastName, user_table.phone)
            .from(user_table)
            .where(user_table.phone == login_phone && user_table.pass == login_passd)
        );
        if(!records.empty()){
            auto& user = records.front();
            
            wee::session::user loggedin_user;
            loggedin_user._id = user.id;
            loggedin_user._first = user.firstName;
            loggedin_user._last = user.lastName;
            ctx.session() << loggedin_user;
        }else{
            validator._errors.push_back("login failed");
        }
    }
    return ctx.render("login.html", validator);
}

void wee::apps::user::ping(udho::contexts::stateful<wee::session::user> ctx, std::size_t n){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        bool busy = false;
        if(ctx.request().method() == boost::beast::http::verb::post){
            std::string body = ctx.request().body();
            nlohmann::json content = nlohmann::json::parse(body);
            if(content.count("busy")){
                busy = content["busy"].get<bool>();
            }
        }
        //{ update user status in universe
        if(!_universe.exists(loggedin_user.id())){
            wee::person p(loggedin_user.id(), loggedin_user.first(), loggedin_user.last());
            _universe.enter(p);
        }
        wee::person& person = _universe[loggedin_user.id()];
        if(busy){
            person.active(boost::posix_time::second_clock::local_time());
            _universe.update(loggedin_user.id(), wee::person::status::busy);
        }else{
            if(person.idle() >= boost::posix_time::minutes(1)){
                _universe.update(loggedin_user.id(), wee::person::status::away);
            }else{
                _universe.update(loggedin_user.id(), wee::person::status::online);
            }
        }
        person.seen(boost::posix_time::second_clock::local_time());
        //}
        presence_watch watch(ctx, loggedin_user.id(), _universe, _connection);
        if(n == 0){
            watch.release();
        }else{
            _watcher.insert(watch);
        }
    }else{
        throw udho::exceptions::http_error(boost::beast::http::status::not_found);
    }
}

void wee::apps::user::updated(const wee::person& person){
    _watcher.async_notify_all();
}

std::string wee::apps::user::self(udho::contexts::stateful<wee::session::user> ctx){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        std::size_t uid = loggedin_user.id();
        return profile(ctx, uid);
    }else{
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such user");
    }
    return "";
}

std::string wee::apps::user::profile(udho::contexts::stateful<wee::session::user> ctx, std::size_t uid){
    wee::Users   users;
    wee::Reports reports;
    wee::Places  places;
    auto reportcount = select(reports.author, count(reports.id)).from(reports).unconditionally().group_by(reports.author).as(sqlpp::alias::a);
    auto placecount  = select(places.author, count(places.id)).from(places).unconditionally().group_by(places.author).as(sqlpp::alias::b);
    auto records = _connection(
        select(
            users.id, 
            users.firstName, 
            users.lastName, 
            users.designation, 
            users.currentDesignation,
            users.photo,
            reportcount.count.as(nreports),
            placecount.count.as(nplaces)
        ).from(
            users.left_outer_join(reportcount).on(reportcount.author == users.id)
                 .left_outer_join(placecount).on(placecount.author == users.id)
        ).where(users.id == uid)
    );
    if(!records.size()){
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such user");
    }
    
    auto& record = records.front();
    
    data::person person(record.id, record.firstName, record.lastName);
    person.affiliation = record.designation;
    person.affiliation2 = record.currentDesignation;
    person.nreports = record.nreports;
    person.nplaces  = record.nplaces;
    person.photo    = record.photo;
    
    data::user user(ctx.session().exists<wee::session::user>());
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        user.id   = loggedin_user.id();
        user.name = loggedin_user.name();
    }
    
    return ctx.render("profile.html", person, user);
}

nlohmann::json wee::apps::user::like(udho::contexts::stateful<wee::session::user> ctx, std::string q){
    std::string pattern = "%"+q+"%";
    std::cout << pattern << std::endl;
    wee::Users   users;
    auto records = _connection(
        select(
            users.id, 
            users.firstName, 
            users.lastName, 
            users.photo,
            users.designation, 
            users.currentDesignation
        ).from(users).where((users.firstName + users.lastName).like(pattern))
    );
    
    nlohmann::json matching_users = nlohmann::json::array();
    for(const auto& record: records){
        std::string name = record.firstName.value() + " " + record.lastName.value();
        nlohmann::json matching_user = {
            {"id", record.id.value()},
            {"first", record.firstName},
            {"last", record.lastName},
            {"name", name},
            {"photo", record.photo},
            {"affiliation", record.designation},
            {"affiliation2", record.currentDesignation}
        };
        matching_users.push_back(matching_user);
    }
    return {
        {"results", matching_users}
    };
}

nlohmann::json wee::apps::user::upload(udho::contexts::stateful<wee::session::user> ctx, std::size_t uid){    
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        std::string reason;
        if(loggedin_user.id() != uid && !allowed_to("user:update", 0, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    
        const auto& form = ctx.form().multipart();
        std::string contents = form.part("photo").str();
        boost::filesystem::path path = boost::filesystem::temp_directory_path() / (boost::filesystem::unique_path().string() + "." +form.part("photo").filename().copied<std::string>());
        std::ofstream file(path.c_str());
        file << contents;
        file.close();
        
        Magick::Image image;
        image.read(path.c_str());
        image.magick("PNG");
        
        boost::filesystem::path www_path(WWW_PATH);
        boost::filesystem::path dest = www_path / "uploads" / "dp";
        boost::uuids::random_generator gen;
        boost::uuids::uuid u = gen();
        std::string location = boost::uuids::to_string(u) + ".png";
        
        image.write((dest / location).c_str());
        
        wee::Users users;
        
        auto statement = sqlpp::postgresql::update(users).set(
            users.photo = location
        ).where(users.id == uid).returning(users.id);
        
        auto records = _connection(statement);
        
        return {
            {"success", records.size()},
            {"photo",   location},
            {"user",    records.size() ? records.front().id.value() : 0}
        };
    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}


bool wee::apps::user::allowed_to(const std::string& resource, std::size_t project, std::size_t uid, std::string& reason){
    std::string sql = (boost::format("select * from allowed_to('%1%', %2%, %3%)") % resource % project % uid).str();
    auto statement = custom_query(sqlpp::verbatim(sql))
        .with_result_type_of(select(
            sqlpp::value(false).as(sqlpp::alias::a),
            sqlpp::value("").as(sqlpp::alias::b)
        ));
    auto records = _connection(statement);
    const auto& permission = records.front();
    reason = permission.b.value();
    return permission.a.value();
}
