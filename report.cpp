/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "report.h"
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/split.hpp>
#include <sqlpp11/value_or_null.h>
#include <sqlpp11/remove.h>
#include <sqlpp11/postgresql/insert.h>
#include <sqlpp11/postgresql/update.h>
#include <sqlpp11/postgresql/returning.h>
#include <sqlpp11/group_by.h>
#include <udho/util.h>
#include <boost/regex.hpp>
#include <xlnt/xlnt.hpp>
#include "ddl/reports.h"
#include "ddl/users.h"
#include "ddl/places.h"
#include "ddl/projects.h"
#include "ddl/memberships.h"
#include "ddl/types.h"
#include "data.h"

SQLPP_ALIAS_PROVIDER(type_name);
SQLPP_ALIAS_PROVIDER(type_label);

wee::apps::report::report(sqlpp::postgresql::connection& conn): base("report"), _connection(conn){}

nlohmann::json wee::apps::report::total(udho::contexts::stateful<wee::session::user> ctx, std::size_t project){
    wee::Reports reports;
    auto records = _connection(
        select(
            count(reports.id)
        ).from(reports)
        .where(reports.project == project)
    );
    const auto& record = records.front();
    return {
        {"count", record.count.value()}
    };
}

nlohmann::json wee::apps::report::fetch(udho::contexts::stateful<wee::session::user> ctx, std::size_t project, std::size_t offset, std::size_t limit){
    wee::Projects projects;
    auto project_records = _connection(select(projects.id, projects.owner).from(projects).where(projects.id == project));
    if(!project_records.size()){
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
    }
    const auto& project_record = project_records.front();
    std::size_t owner = project_record.owner;
    
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        std::string reason;
        if(loggedin_user.id() != owner && !allowed_to("post:fetch", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }else{
        std::string reason;
        if(!allowed_to("post:fetch", project, 0, reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }
    
    wee::Reports reports;
    wee::Places  places;
    wee::Users   users;
    wee::Types   types;
    auto rowcount = select(places.report, count(places.id)).from(places).unconditionally().group_by(places.report).as(sqlpp::alias::a);
    auto records = _connection(
        select(
            reports.id, 
            reports.aoi, 
            reports.toi, 
            reports.type, 
            reports.story, 
            reports.created, 
            reports.properties,
            reports.author,
            reports.project,
            users.firstName, 
            users.lastName,
            rowcount.count,
            types.name.as(type_name),
            types.label.as(type_label)
        ).from(
            reports.join(users).on(reports.author == users.id)
                   .left_outer_join(rowcount).on(reports.id == rowcount.report)
                   .left_outer_join(types).on(reports.type == types.id)
        ).limit(limit).offset(offset).order_by(reports.id.asc())
        .where(reports.project == project)
    );
    nlohmann::json spatial_reports = nlohmann::json::array();
    for(const auto& record: records){
        std::time_t created_time = std::chrono::system_clock::to_time_t(record.created.value());
        std::string created_str(std::ctime(&created_time));
        
        std::time_t interest_time = std::chrono::system_clock::to_time_t(record.toi.value());
        std::string toi_str(std::ctime(&interest_time));

        nlohmann::json report = {
            {"id",         record.id.value()},
            {"story",      record.story},
            {"aoi",        record.aoi},
            {"toi",        toi_str},
            {"type", {
                {"id",     record.type.value()},
                {"name",   record.type_name},
                {"label",  record.type_label}
            }},
            {"places",     record.count.value()},
            {"created",    created_str},
            {"project",    record.project.value()},
            {"properties", nlohmann::json::parse(record.properties.value())},
            {"author", {
                {"id",    ctx.session().exists<wee::session::user>() ? record.author.value() : 0},
                {"first", ctx.session().exists<wee::session::user>() ? record.firstName : std::string("VoxPeople")},
                {"last",  ctx.session().exists<wee::session::user>() ? record.lastName : std::string()}
            }},
        };
        spatial_reports.push_back(report);
    }
    
    nlohmann::json reply = {
        {"count",   spatial_reports.size()},
        {"reports", spatial_reports}
    };
    if(records.size() > 0){
        reply["pages"] = {
            {"curr", (boost::format("/spatial/reports/%1%/%2%") % offset % limit).str()},
            {"next", (boost::format("/spatial/reports/%1%/%2%") % (offset + limit) % limit).str()}
        };
    }
    
    return reply;
}

nlohmann::json wee::apps::report::retrieve(udho::contexts::stateful<wee::session::user> ctx, std::size_t id){
    wee::Reports reports;
    wee::Places  places;
    wee::Users   users;
    wee::Types   types;
    auto rowcount = select(places.report, count(places.id)).from(places).unconditionally().group_by(places.report).as(sqlpp::alias::a);
    auto records = _connection(
        select(
            reports.id, 
            reports.aoi, 
            reports.toi, 
            reports.type, 
            reports.story, 
            reports.created, 
            reports.properties,
            reports.author,
            reports.project,
            users.firstName, 
            users.lastName,
            rowcount.count,
            types.name.as(type_name),
            types.label.as(type_label)
        ).from(
            reports.join(users).on(reports.author == users.id)
                   .left_outer_join(rowcount).on(reports.id == rowcount.report)
                   .left_outer_join(types).on(reports.type == types.id)
        ).where(reports.id == id)
    );
    
    if(!records.size()){
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such report");
    }
    
    auto& record = records.front();
    std::size_t project = record.project.value();
    
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        std::string reason;
        if(loggedin_user.id() != record.author.value() && !allowed_to("post:view", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }else{
        std::string reason;
        if(!allowed_to("post:view", project, 0, reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }

    std::time_t created_time = std::chrono::system_clock::to_time_t(record.created.value());
    std::string created_str(std::ctime(&created_time));
    
    std::time_t interest_time = std::chrono::system_clock::to_time_t(record.toi.value());
    std::string toi_str(std::ctime(&interest_time));

    nlohmann::json report = {
        {"id",         record.id.value()},
        {"story",      record.story},
        {"aoi",        record.aoi},
        {"toi",        toi_str},
        {"type", {
            {"id",     record.type.value()},
            {"name",   record.type_name},
            {"label",  record.type_label}
        }},
        {"places",     record.count.value()},
        {"created",    created_str},
        {"project",    record.project.value()},
        {"properties", nlohmann::json::parse(record.properties.value())},
        {"author", {
            {"id",    ctx.session().exists<wee::session::user>() ? record.author.value() : 0},
            {"first", ctx.session().exists<wee::session::user>() ? record.firstName : std::string("VoxPeople")},
            {"last",  ctx.session().exists<wee::session::user>() ? record.lastName : std::string()}
        }},
    };
    
    return {
        {"report", report}
    };
}

nlohmann::json wee::apps::report::colocated(udho::contexts::stateful<wee::session::user> ctx, std::size_t project, std::string aoi){   
    wee::Projects projects;
    auto project_records = _connection(select(projects.id, projects.owner).from(projects).where(projects.id == project));
    if(!project_records.size()){
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
    }
    const auto& project_record = project_records.front();

    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        std::string reason;
        if(loggedin_user.id() != project_record.owner.value() && !allowed_to("post:view", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }else{
        std::string reason;
        if(!allowed_to("post:view", project, 0, reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }
    
    std::string aoi_query = (boost::format("%%%1%%%") % aoi).str();
    
    wee::Reports reports;
    wee::Users   users;
    wee::Types   types;
    wee::Places  places;
    auto rowcount = select(places.report, count(places.id)).from(places).unconditionally().group_by(places.report).as(sqlpp::alias::a);
    auto records = _connection(
        select(
            reports.id, 
            reports.aoi, 
            reports.toi, 
            reports.type, 
            reports.story, 
            reports.created, 
            reports.properties,
            reports.author,
            reports.project,
            users.firstName, 
            users.lastName,
            rowcount.count,
            types.name.as(type_name),
            types.label.as(type_label)
        ).from(
            reports.join(users).on(reports.author == users.id)
                    .left_outer_join(rowcount).on(reports.id == rowcount.report)
                   .left_outer_join(types).on(reports.type == types.id)
        ).where(reports.project == project and sqlpp::verbatim<sqlpp::integral>((boost::format("SIMILARITY(reports.aoi, '%1%')") % aoi).str()) > 0.8)
    );
    
    nlohmann::json spatial_reports = nlohmann::json::array();
    for(const auto& record: records){
        std::time_t created_time = std::chrono::system_clock::to_time_t(record.created.value());
        std::string created_str(std::ctime(&created_time));
        
        std::time_t interest_time = std::chrono::system_clock::to_time_t(record.toi.value());
        std::string toi_str(std::ctime(&interest_time));

        nlohmann::json report = {
            {"id",         record.id.value()},
            {"story",      record.story},
            {"aoi",        record.aoi},
            {"toi",        toi_str},
            {"type", {
                {"id",     record.type.value()},
                {"name",   record.type_name},
                {"label",  record.type_label}
            }},
            {"created",    created_str},
            {"project",    record.project.value()},
            {"properties", nlohmann::json::parse(record.properties.value())},
            {"places",     record.count.value()},
            {"author", {
                {"id",    ctx.session().exists<wee::session::user>() ? record.author.value() : 0},
                {"first", ctx.session().exists<wee::session::user>() ? record.firstName : std::string("VoxPeople")},
                {"last",  ctx.session().exists<wee::session::user>() ? record.lastName : std::string()}
            }},
        };
        spatial_reports.push_back(report);
    }
    
    nlohmann::json reply = {
        {"count",   spatial_reports.size()},
        {"reports", spatial_reports}
    };
    
    return reply;
}

nlohmann::json wee::apps::report::create(udho::contexts::stateful<wee::session::user> ctx, std::size_t project){
    typedef std::chrono::time_point<std::chrono::system_clock, std::chrono::microseconds> time_type;
    
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        wee::Projects projects;
        auto project_records = _connection(select(projects.id, projects.owner).from(projects).where(projects.id == project));
        if(!project_records.size()){
            throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
        }
        const auto& project_record = project_records.front();
        std::size_t owner = project_record.owner;
        std::string reason;
        if(loggedin_user.id() != owner && !allowed_to("post:create", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
        
        std::string body = ctx.request().body();
        nlohmann::json json = nlohmann::json::parse(body);
        nlohmann::json properties = json["properties"];
        udho::form::required<std::string> aoi("aoi", "Area of Interest must not be empty");
        udho::form::required<std::string> story("story", "Story must not be empty");
        udho::form::required<time_type>   toi("toi", "Time of Interest is Required", "%Y-%m-%dT%H:%M:%S.000Z");
        udho::form::required<std::size_t> type("type", "Type of Report is Required");
        
        auto validator = udho::form::validate();
        validator << (json.contains("aoi")   && json["aoi"].is_string()   ? aoi(json["aoi"]    .get<std::string>()) : aoi());
        validator << (json.contains("story") && json["story"].is_string() ? story(json["story"].get<std::string>()) : story());
        validator << (json.contains("toi")   && json["toi"].is_string()   ? toi(json["toi"]    .get<std::string>()) : toi());
        if(json.contains("type")){
            if(json["type"].is_number()){
                type.unchecked(json["type"].get<std::size_t>());
            }else if(json["type"].is_string()){
                type(json["type"].get<std::string>());
            }else{
                type();
            }
        }else{
            type();
        }
        
        if(!validator.valid()){
            return {
                {"error", true},
                {"success", false},
                {"message", validator.errors().size() ? validator.errors().front() : std::string("Invalid input")},
                {"details", {
                    {"errors", validator.errors()},
                    {"fields", {
                        {"aoi", {
                            {"valid", validator["aoi"].valid()},
                            {"error", validator["aoi"].error()}
                        }},
                        {"story", {
                            {"valid", validator["story"].valid()},
                            {"error", validator["story"].error()}
                        }},
                        {"toi", {
                            {"valid", validator["toi"].valid()},
                            {"error", validator["toi"].error()}
                        }},
                        {"type", {
                            {"valid", validator["type"].valid()},
                            {"error", validator["type"].error()}
                        }}
                    }}
                }}
            };
        }else{
            wee::Reports reports;
            auto statement =  sqlpp::postgresql::insert_into(reports).set(
                reports.aoi        = aoi.value(),
                reports.story      = story.value(),
                reports.toi        = toi.value(),
                reports.type       = type.value(),
                reports.properties = properties.dump(),
                reports.author     = loggedin_user.id(),
                reports.project    = project
            ).returning(reports.id);

            const auto results = _connection(statement);
            return {
                {"id", results.front().id.value()},
                {"success", true}
            };
        }
    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}

nlohmann::json wee::apps::report::upload(udho::contexts::stateful<wee::session::user> ctx, std::size_t project){
    typedef std::chrono::time_point<std::chrono::system_clock, std::chrono::microseconds> time_type;
    
    if(!ctx.session().exists<wee::session::user>()){
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
    wee::session::user loggedin_user;
    ctx.session() >> loggedin_user;
    
    wee::Projects projects;
    auto project_records = _connection(select(projects.id, projects.owner).from(projects).where(projects.id == project));
    if(!project_records.size()){
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
    }
    const auto& project_record = project_records.front();
    std::size_t owner = project_record.owner;

    std::string reason;
    if(loggedin_user.id() != owner && !allowed_to("post:create", project, loggedin_user.id(), reason)){
        return {
            {"error", true},
            {"message", reason}
        };
    }
    
    const auto& form = ctx.form().multipart();
    
    udho::form::required<std::size_t> aoi("aoi", "Area of Interest Column is not supplied");
    aoi .constrain(udho::form::validators::all_digit("Area of Interest Column number must be a number"))
        .constrain(udho::form::validators::no_space("Area of Interest Column number must not have space"));
    udho::form::required<time_type> toi("toi", "Time of Interest is Required", "%Y-%m-%d %H:%M:%S");
    udho::form::required<std::size_t> type("type", "Type of Report is Required");
    udho::form::required<std::string> fixed("fixed", "Constant properties not provided");
    udho::form::required<std::string> tags("tags", "Tags not provided");

    auto validator = udho::form::validate(form);
    validator << aoi << toi << type << fixed << tags;
    
    if(validator.valid()){
        if(form.has("file")){
            std::string contents = form.part("file").str();
            boost::filesystem::path path = boost::filesystem::temp_directory_path() / (boost::filesystem::unique_path().string() + "." +form.part("file").filename().copied<std::string>());
            std::cout << path << std::endl;
            std::ofstream file(path.c_str());
            file << contents;
            file.close();
            
            std::time_t toi_timepoint = std::chrono::system_clock::to_time_t(toi.value());
            
            std::cout << aoi.value() << std::endl;
            std::cout << type.value() << std::endl;
            std::cout << std::ctime(&toi_timepoint) << std::endl;
            std::cout << fixed.value() << std::endl;
            std::cout << tags.value() << std::endl;
            
            nlohmann::json properties = nlohmann::json::object();
            nlohmann::json fixed_properties = nlohmann::json::parse(fixed.value());
            for(auto it = fixed_properties.begin(); it != fixed_properties.end(); ++it){
                properties[it.key()] = it.value();
            }
            nlohmann::json tags_json = nlohmann::json::parse(tags.value());
            
            std::map<std::size_t, std::string> tags_map;
            for(const auto& tag: tags_json){
                std::size_t column = 0;
                if(tag["column"].is_number()){
                    column = tag["column"].get<std::size_t>();
                }else if(tag["column"].is_string()){
                    column = boost::lexical_cast<std::size_t>(tag["column"].get<std::string>());
                }
                std::string name = tag["name"].get<std::string>();
                tags_map.insert(std::make_pair(column, name));
            }
            
            wee::Reports reports;
            auto statement = sqlpp::insert_into(reports).columns(
                reports.aoi,
                reports.toi,
                reports.type,
                reports.story,
                reports.properties,
                reports.author,
                reports.project
            );
            
            xlnt::workbook book;
            book.load(path.c_str());
            auto sheet  = book.active_sheet();
            std::size_t counter = 0, empty_rows = 0;
            for(auto row : sheet.rows(false)){
                if(empty_rows >= 3){
                    break;
                }
                
                nlohmann::json properties_row = properties;
                std::string area;
                if(row.length() >= aoi.value()){
                    auto area_cell = row[aoi.value()];
                    area = area_cell.value<std::string>();
                    if(!area.empty()){
                        for(const auto& p: tags_map){
                            std::size_t column = p.first;
                            if(row.length() >= column){
                                auto cell = row[column];
                                std::string value = cell.to_string();
                                if(!value.empty()){
                                    properties_row[p.second] = value;
                                }
                            }
                        }
                        int reason = type.value();
                        int author = loggedin_user.id();
                        statement.values.add(
                            reports.aoi        = area, 
                            reports.toi        = toi.value(), 
                            reports.type       = reason, 
                            reports.story      = area, 
                            reports.properties = properties_row.dump(), 
                            reports.author     = author,
                            reports.project    = (int)project
                        );
                    }else{
                        ++empty_rows;
                    }
                }else{
                    ++empty_rows;
                }
                ++counter;
            }
            
            _connection(statement);
            return {
                {"success", true}
            };
        }
    }
    return {
        {"error", true},
        {"message", "Invalid Data"}
    };
}

nlohmann::json wee::apps::report::update(udho::contexts::stateful<wee::session::user> ctx, std::size_t id){
    typedef std::chrono::time_point<std::chrono::system_clock, std::chrono::microseconds> time_type;
    
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        wee::Reports reports;
        auto report_records = _connection(select(reports.project, reports.author).from(reports).where(reports.id == id));
        if(!report_records.size()){
            throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Report");
        }
        std::size_t project = report_records.front().project;
        std::size_t author  = report_records.front().author;
        
        std::string reason;
        if(loggedin_user.id() != author && !allowed_to("post:update", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
        
        std::string body = ctx.request().body();
        nlohmann::json json = nlohmann::json::parse(body);
        nlohmann::json properties = json["properties"];
        udho::form::required<std::string> aoi("aoi", "Area of Interest must not be empty");
        udho::form::required<std::string> story("story", "Story must not be empty");
        udho::form::required<time_type>   toi("toi", "Time of Interest is Required", "%Y-%m-%dT%H:%M:%S.000Z");
        udho::form::required<std::size_t> type("type", "Type of Report is Required");
        
        auto validator = udho::form::validate();
        validator << (json.contains("aoi")   && json["aoi"].is_string()   ? aoi(json["aoi"]    .get<std::string>()) : aoi());
        validator << (json.contains("story") && json["story"].is_string() ? story(json["story"].get<std::string>()) : story());
        validator << (json.contains("toi")   && json["toi"].is_string()   ? toi(json["toi"]    .get<std::string>()) : toi());
        if(json.contains("type")){
            if(json["type"].is_number()){
                type.unchecked(json["type"].get<std::size_t>());
            }else if(json["type"].is_string()){
                type(json["type"].get<std::string>());
            }else{
                type();
            }
        }else{
            type();
        }
        
        if(!validator.valid()){
            return {
                {"error", true},
                {"success", false},
                {"message", validator.errors().size() ? validator.errors().front() : std::string("Invalid input")},
                {"details", {
                    {"errors", validator.errors()},
                    {"fields", {
                        {"aoi", {
                            {"valid", validator["aoi"].valid()},
                            {"error", validator["aoi"].error()}
                        }},
                        {"story", {
                            {"valid", validator["story"].valid()},
                            {"error", validator["story"].error()}
                        }},
                        {"toi", {
                            {"valid", validator["toi"].valid()},
                            {"error", validator["toi"].error()}
                        }},
                        {"type", {
                            {"valid", validator["type"].valid()},
                            {"error", validator["type"].error()}
                        }}
                    }}
                }}
            };
        }else{
            auto statement =  sqlpp::postgresql::update(reports).set(
                reports.aoi        = aoi.value(), 
                reports.toi        = toi.value(), 
                reports.type       = type.value(), 
                reports.story      = story.value(), 
                reports.properties = properties.dump(),
                reports.author     = loggedin_user.id()
            ).where(reports.id == id);

            const auto results = _connection(statement);
            return {
                {"id", id},
                {"success", true}
            };
        }
    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}

nlohmann::json wee::apps::report::remove(udho::contexts::stateful<wee::session::user> ctx, std::size_t id){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        wee::Reports reports;
        auto report_records = _connection(select(reports.project, reports.author).from(reports).where(reports.id == id));
        if(!report_records.size()){
            throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Report");
        }
        std::size_t project = report_records.front().project;
        std::size_t author  = report_records.front().author;
        
        std::string reason;
        if(loggedin_user.id() != author && !allowed_to("post:update", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
        
        _connection(remove_from(reports).where(reports.id == id));
        return {
            {"id", id},
            {"success", true}
        };
    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}

bool wee::apps::report::allowed_to(const std::string& resource, std::size_t project, std::size_t uid, std::string& reason){
    std::string sql = (boost::format("select * from allowed_to('%1%', %2%, %3%)") % resource % project % uid).str();
    auto statement = custom_query(sqlpp::verbatim(sql))
        .with_result_type_of(select(
            sqlpp::value(false).as(sqlpp::alias::a),
            sqlpp::value("").as(sqlpp::alias::b)
        ));
    auto records = _connection(statement);
    const auto& permission = records.front();
    reason = permission.b.value();
    return permission.a.value();
}
