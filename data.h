#ifndef WEE_DATA_H
#define WEE_DATA_H

namespace wee{

namespace data{
    struct volunteer: udho::prepare<volunteer>{
        std::size_t id;
        std::string name;
        std::string photo;
        std::string affiliation;
        std::string affiliation2;
        std::size_t count;
        
        inline volunteer(){}
        inline volunteer(const std::string& n): name(n){}
        inline bool currentDiffers() const{
            return affiliation2.size() && affiliation2 != affiliation;
        }
        template <typename DictT>
        auto dict(DictT assoc) const{
            return assoc | var("id",           &volunteer::id)
                         | var("name",         &volunteer::name)
                         | var("photo",        &volunteer::photo)
                         | var("affiliation",  &volunteer::affiliation)
                         | var("affiliation2", &volunteer::affiliation2)
                         | var("count",        &volunteer::count)
                         | fn ("different",    &volunteer::currentDiffers);
        }
    };
    struct person: udho::prepare<person>{
        std::size_t id;
        std::string first;
        std::string last;
        std::string photo;
        std::string affiliation;
        std::string affiliation2;
        std::size_t nreports;
        std::size_t nplaces;
        
        inline person(){}
        inline person(const std::size_t uid, const std::string& f, const std::string& l): id(uid), first(f), last(l){}
        inline std::string name() const{
            return first + " " + last;
        }
        inline bool currentDiffers() const{
            return affiliation2.size() && affiliation2 != affiliation;
        }
        template <typename DictT>
        auto dict(DictT assoc) const{
            return assoc | var("id",            &person::id)
                         | fn ("name",          &person::name)
                         | var("first",         &person::first)
                         | var("last",          &person::last)
                         | var("photo",         &person::photo)
                         | var("affiliation",   &person::affiliation)
                         | var("affiliation2",  &person::affiliation2)
                         | var("nreports",      &person::nreports)
                         | var("nplaces",       &person::nplaces)
                         | fn ("uadifferent",   &person::currentDiffers);
        }
    };
    struct project_properties: udho::prepare<project_properties>{
        std::string color;
        double lat;
        double lng;
        
        inline std::string background() const{
            return "bg-"+color+"-500";
        }
        inline std::string border() const{
            return "border-"+color+"-500";
        }
        
        template <typename DictT>
        auto dict(DictT assoc) const{
            return assoc | var("color",         &project_properties::color)
                         | var("lat",           &project_properties::lat)
                         | var("lng",           &project_properties::lng)
                         | fn ("background",    &project_properties::background)
                         | fn ("border",        &project_properties::border);
        }
    };
    struct project: udho::prepare<project>{
        std::size_t id;
        std::string name;
        std::string label;
        std::string description;
        nlohmann::json synopsis;
        bool        editable;
        std::size_t nreports;
        std::size_t nplaces;
        project_properties properties;
        std::vector<volunteer> volunteers;
        
        inline project(std::size_t p, bool edtble = false): id(p), editable(edtble), nreports(0){}
        
        inline std::string link() const{
            return "/projects/"+name;
        }
        
        template <typename DictT>
        auto dict(DictT assoc) const{
            return assoc | var("project",     &project::id)
                         | var("name",        &project::name)
                         | var("label",       &project::label)
                         | var("description", &project::description)
                         | var("synopsis",    &project::synopsis)
                         | var("editable",    &project::editable)
                         | var("nreports",    &project::nreports)
                         | var("nplaces",     &project::nplaces)
                         | var("properties",  &project::properties)
                         | var("volunteers",  &project::volunteers)
                         | fn ("link",        &project::link);
        }
    };
    struct projects_list: udho::prepare<projects_list>{
        std::vector<project> projects;
        
        inline std::size_t count() const{
            return projects.size();
        }
        
        template <typename DictT>
        auto dict(DictT assoc) const{
            return assoc | fn ("count",     &projects_list::count)
                         | var("projects",  &projects_list::projects);
        }
    };
    struct user: udho::prepare<user>{
        bool        loggedin;
        std::size_t id;
        std::string name;
        
        inline user(bool uloggedin, std::size_t uid = 0): loggedin(uloggedin), id(uid){}
        
        template <typename DictT>
        auto dict(DictT assoc) const{
            return assoc | var("loggedin",  &user::loggedin)
                         | var("uid",       &user::id)
                         | var("uname",     &user::name);
        }
    };
    struct report: udho::prepare<report>{
        std::size_t id;
        std::string aoi;
        std::size_t nplaces;
        
        template <typename DictT>
        auto dict(DictT assoc) const{
            return assoc | var("id",      &report::id)
                         | var("aoi",     &report::aoi)
                         | var("nplaces", &report::nplaces);
        }
    };
    struct book: udho::prepare<book>{
        std::vector<report> reports;
        
        template <typename DictT>
        auto dict(DictT assoc) const{
            return assoc | var("reports", &book::reports);
        }
    };
}

}

#endif // WEE_DATA_H
