const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
//const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const ProgressBarPlugin = require('progress-bar-webpack-plugin');

module.exports = {
    entry: {
        login:   './components/login.js',
        chat:    './components/chat.js',
        profile: './components/profile.js',
        spatial: './components/spatial.js',
        projects:'./components/projects.js',
        plist:   './components/plist.js',
        users:   './components/users.js'
    },
    output: {
        filename: '[name].js',
        path: path.join(process.cwd(), 'dist')
    },
    mode: 'production',
    // devtool: 'inline-source-map',
    optimization: {
        usedExports: true
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: [{
                    loader: 'vue-loader'
                },{
                    loader: 'vue-svg-inline-loader'
                }]
            },{
                test: /\.js$/,
                loader: 'babel-loader'
            },{
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    "postcss-loader"
                ]
            },{
                test: /\.(png|jpg|gif)$/i,
                loader: 'url-loader'
            },{
                test: /\.svg$/,
                loader: 'svg-inline-loader'
            },{
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                  loader: 'babel-loader',
                  options: {
                    presets: ['@babel/preset-env'],
                    plugins: ['@babel/plugin-proposal-object-rest-spread', "@babel/plugin-proposal-class-properties"]
                  }
                }
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
        new ProgressBarPlugin(),
        // new BundleAnalyzerPlugin()
    ],
    resolve: {
        symlinks: false,
        extensions: ['*', '.js', '.vue', '.json'],
        alias: {
            vue: 'vue/dist/vue.esm.js'
        }
    },
    performance: {
        maxEntrypointSize: 1000000,
        maxAssetSize: 1000000
    }
}
