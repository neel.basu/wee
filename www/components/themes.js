const themes = {
    gray:   {
        'lightest': {bg: 'bg-gray-100', text: 'text-gray-100', hover: 'hover:text-gray-100', color: '#f7fafc'},
        'lighter':  {bg: 'bg-gray-200', text: 'text-gray-200', hover: 'hover:text-gray-200', color: '#edf2f7'},
        'light':    {bg: 'bg-gray-300', text: 'text-gray-300', hover: 'hover:text-gray-300', color: '#e2e8f0'},
        'lightish': {bg: 'bg-gray-400', text: 'text-gray-400', hover: 'hover:text-gray-400', color: '#cbd5e0'},
        'regular':  {bg: 'bg-gray-500', text: 'text-gray-500', hover: 'hover:text-gray-500', color: '#a0aec0'},
        'darkish':  {bg: 'bg-gray-600', text: 'text-gray-600', hover: 'hover:text-gray-600', color: '#718096'},
        'dark':     {bg: 'bg-gray-700', text: 'text-gray-700', hover: 'hover:text-gray-700', color: '#4a5568'},
        'darker':   {bg: 'bg-gray-800', text: 'text-gray-800', hover: 'hover:text-gray-800', color: '#2d3748'},
        'darkest':  {bg: 'bg-gray-900', text: 'text-gray-900', hover: 'hover:text-gray-900', color: '#1a202c'}
    },
    red:    {
        'lightest': {bg: 'bg-red-100', text: 'text-red-100', hover: 'hover:text-red-100', color: '#fff5f5'},
        'lighter':  {bg: 'bg-red-200', text: 'text-red-200', hover: 'hover:text-red-200', color: '#fed7d7'},
        'light':    {bg: 'bg-red-300', text: 'text-red-300', hover: 'hover:text-red-300', color: '#feb2b2'},
        'lightish': {bg: 'bg-red-400', text: 'text-red-400', hover: 'hover:text-red-400', color: '#fc8181'},
        'regular':  {bg: 'bg-red-500', text: 'text-red-500', hover: 'hover:text-red-500', color: '#f56565'},
        'darkish':  {bg: 'bg-red-600', text: 'text-red-600', hover: 'hover:text-red-600', color: '#e53e3e'},
        'dark':     {bg: 'bg-red-700', text: 'text-red-700', hover: 'hover:text-red-700', color: '#c53030'},
        'darker':   {bg: 'bg-red-800', text: 'text-red-800', hover: 'hover:text-red-800', color: '#9b2c2c'},
        'darkest':  {bg: 'bg-red-900', text: 'text-red-900', hover: 'hover:text-red-900', color: '#742a2a'}
    },
    orange: {
        'lightest': {bg: 'bg-orange-100', text: 'text-orange-100', hover: 'hover:text-orange-100', color: '#fffaf0'},
        'lighter':  {bg: 'bg-orange-200', text: 'text-orange-200', hover: 'hover:text-orange-200', color: '#feebc8'},
        'light':    {bg: 'bg-orange-300', text: 'text-orange-300', hover: 'hover:text-orange-300', color: '#fbd38d'},
        'lightish': {bg: 'bg-orange-400', text: 'text-orange-400', hover: 'hover:text-orange-400', color: '#f6ad55'},
        'regular':  {bg: 'bg-orange-500', text: 'text-orange-500', hover: 'hover:text-orange-500', color: '#ed8936'},
        'darkish':  {bg: 'bg-orange-600', text: 'text-orange-600', hover: 'hover:text-orange-600', color: '#dd6b20'},
        'dark':     {bg: 'bg-orange-700', text: 'text-orange-700', hover: 'hover:text-orange-700', color: '#c05621'},
        'darker':   {bg: 'bg-orange-800', text: 'text-orange-800', hover: 'hover:text-orange-800', color: '#9c4221'},
        'darkest':  {bg: 'bg-orange-900', text: 'text-orange-900', hover: 'hover:text-orange-900', color: '#7b341e'}
    },
    yellow: {
        'lightest': {bg: 'bg-yellow-100', text: 'text-yellow-100', hover: 'hover:text-yellow-100', color: '#fffff0'},
        'lighter':  {bg: 'bg-yellow-200', text: 'text-yellow-200', hover: 'hover:text-yellow-200', color: '#fefcbf'},
        'light':    {bg: 'bg-yellow-300', text: 'text-yellow-300', hover: 'hover:text-yellow-300', color: '#faf089'},
        'lightish': {bg: 'bg-yellow-400', text: 'text-yellow-400', hover: 'hover:text-yellow-400', color: '#f6e05e'},
        'regular':  {bg: 'bg-yellow-500', text: 'text-yellow-500', hover: 'hover:text-yellow-500', color: '#ecc94b'},
        'darkish':  {bg: 'bg-yellow-600', text: 'text-yellow-600', hover: 'hover:text-yellow-600', color: '#d69e2e'},
        'dark':     {bg: 'bg-yellow-700', text: 'text-yellow-700', hover: 'hover:text-yellow-700', color: '#b7791f'},
        'darker':   {bg: 'bg-yellow-800', text: 'text-yellow-800', hover: 'hover:text-yellow-800', color: '#975a16'},
        'darkest':  {bg: 'bg-yellow-900', text: 'text-yellow-900', hover: 'hover:text-yellow-900', color: '#744210'}
    },
    green:  {
        'lightest': {bg: 'bg-green-100', text: 'text-green-100', hover: 'hover:text-green-100', color: '#f0fff4'},
        'lighter':  {bg: 'bg-green-200', text: 'text-green-200', hover: 'hover:text-green-200', color: '#c6f6d5'},
        'light':    {bg: 'bg-green-300', text: 'text-green-300', hover: 'hover:text-green-300', color: '#9ae6b4'},
        'lightish': {bg: 'bg-green-400', text: 'text-green-400', hover: 'hover:text-green-400', color: '#68d391'},
        'regular':  {bg: 'bg-green-500', text: 'text-green-500', hover: 'hover:text-green-500', color: '#48bb78'},
        'darkish':  {bg: 'bg-green-600', text: 'text-green-600', hover: 'hover:text-green-600', color: '#38a169'},
        'dark':     {bg: 'bg-green-700', text: 'text-green-700', hover: 'hover:text-green-700', color: '#2f855a'},
        'darker':   {bg: 'bg-green-800', text: 'text-green-800', hover: 'hover:text-green-800', color: '#276749'},
        'darkest':  {bg: 'bg-green-900', text: 'text-green-900', hover: 'hover:text-green-900', color: '#22543d'}
    },
    teal:   {
        'lightest': {bg: 'bg-teal-100', text: 'text-teal-100', hover: 'hover:text-teal-100', color: '#e6fffa'},
        'lighter':  {bg: 'bg-teal-200', text: 'text-teal-200', hover: 'hover:text-teal-200', color: '#b2f5ea'},
        'light':    {bg: 'bg-teal-300', text: 'text-teal-300', hover: 'hover:text-teal-300', color: '#81e6d9'},
        'lightish': {bg: 'bg-teal-400', text: 'text-teal-400', hover: 'hover:text-teal-400', color: '#4fd1c5'},
        'regular':  {bg: 'bg-teal-500', text: 'text-teal-500', hover: 'hover:text-teal-500', color: '#38b2ac'},
        'darkish':  {bg: 'bg-teal-600', text: 'text-teal-600', hover: 'hover:text-teal-600', color: '#319795'},
        'dark':     {bg: 'bg-teal-700', text: 'text-teal-700', hover: 'hover:text-teal-700', color: '#2c7a7b'},
        'darker':   {bg: 'bg-teal-800', text: 'text-teal-800', hover: 'hover:text-teal-800', color: '#285e61'},
        'darkest':  {bg: 'bg-teal-900', text: 'text-teal-900', hover: 'hover:text-teal-900', color: '#234e52'}
    },
    blue:   {
        'lightest': {bg: 'bg-blue-100', text: 'text-blue-100', hover: 'hover:text-blue-100', color: '#ebf8ff'},
        'lighter':  {bg: 'bg-blue-200', text: 'text-blue-200', hover: 'hover:text-blue-200', color: '#bee3f8'},
        'light':    {bg: 'bg-blue-300', text: 'text-blue-300', hover: 'hover:text-blue-300', color: '#90cdf4'},
        'lightish': {bg: 'bg-blue-400', text: 'text-blue-400', hover: 'hover:text-blue-400', color: '#63b3ed'},
        'regular':  {bg: 'bg-blue-500', text: 'text-blue-500', hover: 'hover:text-blue-500', color: '#4299e1'},
        'darkish':  {bg: 'bg-blue-600', text: 'text-blue-600', hover: 'hover:text-blue-600', color: '#3182ce'},
        'dark':     {bg: 'bg-blue-700', text: 'text-blue-700', hover: 'hover:text-blue-700', color: '#2b6cb0'},
        'darker':   {bg: 'bg-blue-800', text: 'text-blue-800', hover: 'hover:text-blue-800', color: '#2c5282'},
        'darkest':  {bg: 'bg-blue-900', text: 'text-blue-900', hover: 'hover:text-blue-900', color: '#2a4365'}
    },
    indigo: {
        'lightest': {bg: 'bg-indigo-100', text: 'text-indigo-100', hover: 'hover:text-indigo-100', color: '#ebf4ff'},
        'lighter':  {bg: 'bg-indigo-200', text: 'text-indigo-200', hover: 'hover:text-indigo-200', color: '#c3dafe'},
        'light':    {bg: 'bg-indigo-300', text: 'text-indigo-300', hover: 'hover:text-indigo-300', color: '#a3bffa'},
        'lightish': {bg: 'bg-indigo-400', text: 'text-indigo-400', hover: 'hover:text-indigo-400', color: '#7f9cf5'},
        'regular':  {bg: 'bg-indigo-500', text: 'text-indigo-500', hover: 'hover:text-indigo-500', color: '#667eea'},
        'darkish':  {bg: 'bg-indigo-600', text: 'text-indigo-600', hover: 'hover:text-indigo-600', color: '#5a67d8'},
        'dark':     {bg: 'bg-indigo-700', text: 'text-indigo-700', hover: 'hover:text-indigo-700', color: '#4c51bf'},
        'darker':   {bg: 'bg-indigo-800', text: 'text-indigo-800', hover: 'hover:text-indigo-800', color: '#434190'},
        'darkest':  {bg: 'bg-indigo-900', text: 'text-indigo-900', hover: 'hover:text-indigo-900', color: '#3c366b'}
    },
    purple: {
        'lightest': {bg: 'bg-purple-100', text: 'text-purple-100', hover: 'hover:text-purple-100', color: '#faf5ff'},
        'lighter':  {bg: 'bg-purple-200', text: 'text-purple-200', hover: 'hover:text-purple-200', color: '#e9d8fd'},
        'light':    {bg: 'bg-purple-300', text: 'text-purple-300', hover: 'hover:text-purple-300', color: '#d6bcfa'},
        'lightish': {bg: 'bg-purple-400', text: 'text-purple-400', hover: 'hover:text-purple-400', color: '#b794f4'},
        'regular':  {bg: 'bg-purple-500', text: 'text-purple-500', hover: 'hover:text-purple-500', color: '#9f7aea'},
        'darkish':  {bg: 'bg-purple-600', text: 'text-purple-600', hover: 'hover:text-purple-600', color: '#805ad5'},
        'dark':     {bg: 'bg-purple-700', text: 'text-purple-700', hover: 'hover:text-purple-700', color: '#6b46c1'},
        'darker':   {bg: 'bg-purple-800', text: 'text-purple-800', hover: 'hover:text-purple-800', color: '#553c9a'},
        'darkest':  {bg: 'bg-purple-900', text: 'text-purple-900', hover: 'hover:text-purple-900', color: '#44337a'}
    },
    pink:   {
        'lightest': {bg: 'bg-pink-100', text: 'text-pink-100', hover: 'hover:text-pink-100', color: '#fff5f7'},
        'lighter':  {bg: 'bg-pink-200', text: 'text-pink-200', hover: 'hover:text-pink-200', color: '#fed7e2'},
        'light':    {bg: 'bg-pink-300', text: 'text-pink-300', hover: 'hover:text-pink-300', color: '#fbb6ce'},
        'lightish': {bg: 'bg-pink-400', text: 'text-pink-400', hover: 'hover:text-pink-400', color: '#f687b3'},
        'regular':  {bg: 'bg-pink-500', text: 'text-pink-500', hover: 'hover:text-pink-500', color: '#ed64a6'},
        'darkish':  {bg: 'bg-pink-600', text: 'text-pink-600', hover: 'hover:text-pink-600', color: '#d53f8c'},
        'dark':     {bg: 'bg-pink-700', text: 'text-pink-700', hover: 'hover:text-pink-700', color: '#b83280'},
        'darker':   {bg: 'bg-pink-800', text: 'text-pink-800', hover: 'hover:text-pink-800', color: '#97266d'},
        'darkest':  {bg: 'bg-pink-900', text: 'text-pink-900', hover: 'hover:text-pink-900', color: '#702459'}
    }
};

const ThemeMixin = {
    methods: {
        color(name){
            return themes[name];
        },
        palette(){
            return this.color(this.theme.color);
        },
        reportPalette(type){
            if(this.theme.types[type]){
                const dict = this.theme.types[type];
                const properties = dict.properties;
                if(properties && properties.color){
                    return this.color(properties.color);
                }else{
                    return this.palette();
                }
            }
            return this.palette();
        },
        placePalette(type, place){
            if(this.theme.types[type]){
                const dict = this.theme.types[type];
                const properties = dict.properties;
                if(properties && place.properties.status && properties.status){
                    return this.color(properties.status[place.properties.status]);
                }else{
                    this.reportPalette(type);
                }
            }
            return this.palette();
        }
    }
}

export {themes,ThemeMixin}
