import Vue from 'vue'
import WeeNav from './WeeNav'
import WeeNavItem from './WeeNavItem'
import WeeUsers from './WeeUsers'
import './style.css'

Vue.config.devtools = true

new Vue({
    el: '#main',
    components: {
        WeeNav,
        WeeNavItem,
        WeeUsers
    },
    mounted: function(){},
    methods: {}
});
