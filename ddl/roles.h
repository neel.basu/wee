// generated by ../deps/sqlpp11/scripts/ddl2cpp roles.sqlpp11.sql roles wee
#ifndef WEE_ROLES_H
#define WEE_ROLES_H

#include <sqlpp11/table.h>
#include <sqlpp11/data_types.h>
#include <sqlpp11/char_sequence.h>

namespace wee
{
  namespace Roles_
  {
    struct Id
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "id";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T id;
            T& operator()() { return id; }
            const T& operator()() const { return id; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::bigint, sqlpp::tag::must_not_insert, sqlpp::tag::must_not_update>;
    };
    struct Name
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "name";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T name;
            T& operator()() { return name; }
            const T& operator()() const { return name; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::text, sqlpp::tag::require_insert>;
    };
    struct Order
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "!order";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T order;
            T& operator()() { return order; }
            const T& operator()() const { return order; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::bigint, sqlpp::tag::require_insert>;
    };
    struct Description
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "description";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T description;
            T& operator()() { return description; }
            const T& operator()() const { return description; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::text, sqlpp::tag::can_be_null>;
    };
  } // namespace Roles_

  struct Roles: sqlpp::table_t<Roles,
               Roles_::Id,
               Roles_::Name,
               Roles_::Order,
               Roles_::Description>
  {
    struct _alias_t
    {
      static constexpr const char _literal[] =  "roles";
      using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
      template<typename T>
      struct _member_t
      {
        T roles;
        T& operator()() { return roles; }
        const T& operator()() const { return roles; }
      };
    };
  };
} // namespace wee
#endif
