// generated by ../deps/sqlpp11/scripts/ddl2cpp users.sqlpp11.sql users wee
#ifndef WEE_USERS_H
#define WEE_USERS_H

#include <sqlpp11/table.h>
#include <sqlpp11/data_types.h>
#include <sqlpp11/char_sequence.h>

namespace wee
{
  namespace Users_
  {
    struct Id
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "id";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T id;
            T& operator()() { return id; }
            const T& operator()() const { return id; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::bigint, sqlpp::tag::must_not_insert, sqlpp::tag::must_not_update>;
    };
    struct FirstName
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "first_name";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T firstName;
            T& operator()() { return firstName; }
            const T& operator()() const { return firstName; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::text, sqlpp::tag::require_insert>;
    };
    struct LastName
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "last_name";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T lastName;
            T& operator()() { return lastName; }
            const T& operator()() const { return lastName; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::text, sqlpp::tag::require_insert>;
    };
    struct Email
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "email";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T email;
            T& operator()() { return email; }
            const T& operator()() const { return email; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::text, sqlpp::tag::can_be_null>;
    };
    struct Phone
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "phone";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T phone;
            T& operator()() { return phone; }
            const T& operator()() const { return phone; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::text, sqlpp::tag::require_insert>;
    };
    struct Pass
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "pass";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T pass;
            T& operator()() { return pass; }
            const T& operator()() const { return pass; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::text, sqlpp::tag::require_insert>;
    };
    struct Designation
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "designation";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T designation;
            T& operator()() { return designation; }
            const T& operator()() const { return designation; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::text, sqlpp::tag::can_be_null>;
    };
    struct CurrentDesignation
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "current_designation";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T currentDesignation;
            T& operator()() { return currentDesignation; }
            const T& operator()() const { return currentDesignation; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::text, sqlpp::tag::can_be_null>;
    };
    struct Photo
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "photo";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T photo;
            T& operator()() { return photo; }
            const T& operator()() const { return photo; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::text, sqlpp::tag::can_be_null>;
    };
    struct Rank
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "rank";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T rank;
            T& operator()() { return rank; }
            const T& operator()() const { return rank; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::bigint>;
    };
  } // namespace Users_

  struct Users: sqlpp::table_t<Users,
               Users_::Id,
               Users_::FirstName,
               Users_::LastName,
               Users_::Email,
               Users_::Phone,
               Users_::Pass,
               Users_::Designation,
               Users_::CurrentDesignation,
               Users_::Photo,
               Users_::Rank>
  {
    struct _alias_t
    {
      static constexpr const char _literal[] =  "users";
      using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
      template<typename T>
      struct _member_t
      {
        T users;
        T& operator()() { return users; }
        const T& operator()() const { return users; }
      };
    };
  };
} // namespace wee
#endif
