CREATE TABLE places
(
    id bigserial NOT NULL,
    report bigint NOT NULL,
    type text NOT NULL,
    data text NOT NULL,
    created timestamp without time zone NOT NULL DEFAULT NOW(),
    updated timestamp without time zone NOT NULL DEFAULT NOW(),
    title text NOT NULL,
    properties text,
    author bigint NOT NULL
);
