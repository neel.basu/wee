CREATE TABLE permissions
(
    id bigserial NOT NULL,
    resource bigint NOT NULL,
    role bigint NOT NULL,
    since timestamp without time zone NOT NULL DEFAULT now(),
    project bigint NOT NULL,
    rank bigint NOT NULL,
    assigner bigint NOT NULL,
    PRIMARY KEY (id)
)
