CREATE TABLE users
(
    id bigserial NOT NULL,
    first_name text NOT NULL,
    last_name text NOT NULL,
    email text,
    phone text NOT NULL,
    pass text NOT NULL,
    designation text,
    current_designation text,
    photo text,
    rank bigint NOT NULL DEFAULT 5,
    PRIMARY KEY (id)
);
