CREATE TABLE roles
(
    id bigserial NOT NULL,
    name text NOT NULL,
    order bigint NOT NULL,
    description text,
    PRIMARY KEY (id)
)
