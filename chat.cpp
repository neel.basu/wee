/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "chat.h"
#include <boost/asio/placeholders.hpp>

void wee::apps::to_json(nlohmann::json& j, const message& msg) {
    std::string time_str = boost::lexical_cast<std::string>(msg.time);
    j = nlohmann::json({
        {"id",       msg.id},
        {"from",     msg.from},
        {"to",       msg.to},
        {"content",  msg.content},
        {"type",     msg.type},
        {"category", msg.category},
        {"time",     time_str}
    });
}

void wee::apps::from_json(const nlohmann::json& j, message& msg) {
    std::string time_str;
    
    j.at("from").get_to(msg.from);
    j.at("to").get_to(msg.to);
    j.at("content").get_to(msg.content);
    j.at("type").get_to(msg.type);
    j.at("category").get_to(msg.category);
    j.at("time").get_to(time_str);
    try{
        msg.time = boost::lexical_cast<boost::posix_time::ptime>(time_str);
    }catch(...){
        msg.time = boost::posix_time::second_clock::local_time();
    }
}


wee::apps::message_watch::message_watch(udho::contexts::stateful<wee::session::user> ctx, std::size_t last, std::size_t user, messages_from_storage_type& messages_from, messages_to_storage_type& messages_to)
    : base(user), _last(last), _user(user), _context(ctx), _messages_from(messages_from), _messages_to(messages_to){}

void wee::apps::message_watch::operator()(const boost::system::error_code& e){
    nlohmann::json messages = nlohmann::json::array();
    {
        auto range = _messages_from.equal_range(_user);
        for(auto it = range.first; it != range.second; ++it){
            const message& msg = *it;
            if(msg.id > _last){
                nlohmann::json msg_json = msg;
                messages.push_back(msg_json);
            }
        }
    }{
        auto range = _messages_to.equal_range(_user);
        for(auto it = range.first; it != range.second; ++it){
            const message& msg = *it;
            if(msg.id > _last){
                nlohmann::json msg_json = msg;
                messages.push_back(msg_json);
            }
        }
    }
    nlohmann::json content = {
        {"messages", messages}
    };
    _context.respond(content, "text/json");
}

bool wee::apps::message_watch::pending() const{
    auto range_sent = _messages_from.equal_range(_user);
    auto range_rcvd = _messages_to.equal_range(_user);
    std::size_t pending_sent = 0, pending_rcvd = 0;
    for(auto it = range_sent.first; it != range_sent.second; ++it){
        const message& msg = *it;
        if(msg.id > _last){
            ++pending_sent;
        }
    }
    for(auto it = range_rcvd.first; it != range_rcvd.second; ++it){
        const message& msg = *it;
        if(msg.id > _last){
            ++pending_rcvd;
        }
    }
    return (pending_sent + pending_rcvd) > 0;
}

struct chat_home_data: udho::prepare<chat_home_data>{
    wee::session::user _state;
    
    std::size_t id() const{
        return _state.id();
    }
    std::string name() const{
        return _state.name();
    }
    
    chat_home_data(const wee::session::user& state): _state(state){}
    
    template <typename DictT>
    auto dict(DictT assoc) const{
        return assoc | fn("id", &chat_home_data::id)
                     | fn("name", &chat_home_data::name);
    }
};

wee::apps::chat::chat(sqlpp::postgresql::connection& connection, boost::asio::io_service& io, wee::gathering& universe)
    : base("chat"), 
    _connection(connection), 
    _io(io), 
    _universe(universe), 
    _watcher(io, boost::posix_time::seconds(15)), 
    _messages_by_to(_messages.get<2>()), _messages_by_from(_messages.get<1>()), _messages_by_time(_messages.get<3>()){}

std::string wee::apps::chat::home(udho::contexts::stateful<wee::session::user> ctx){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggin_user;
        ctx.session() >> loggin_user;
        chat_home_data data(loggin_user);
        return ctx.render("chat.html", data);
    }else{
        throw udho::exceptions::http_redirection("/user/login");
    }
}

void wee::apps::chat::ping(udho::contexts::stateful<wee::session::user> ctx, std::size_t last){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        message_watch watch(ctx, last, loggedin_user.id(), _messages_by_from, _messages_by_to);
        if(watch.pending()){
            watch.release();
        }else{
            _watcher.insert(watch);
        }
        //{ update user status in universe
        if(!_universe.exists(loggedin_user.id())){
            wee::person p(loggedin_user.id(), loggedin_user.first(), loggedin_user.last());
            _universe.enter(p);
        }
        wee::person& person = _universe[loggedin_user.id()];
        person.seen(boost::posix_time::second_clock::local_time());
        //}
    }else{
        throw udho::exceptions::http_redirection("/user/login");
    }
}

nlohmann::json wee::apps::chat::send(udho::contexts::stateful<wee::session::user> ctx){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        std::string body = ctx.request().body();
        nlohmann::json content = nlohmann::json::parse(body);
        message msg = content.get<message>();
        
        if(loggedin_user.id() == msg.from){
            msg.id = _messages.size()+1;
            _messages.insert(msg);
            
            //{ update person 
            wee::person& person = _universe[msg.from];
            if(person.state() == wee::person::status::away){
                _universe.update(loggedin_user.id(), wee::person::status::online);
            }
            person.seen(boost::posix_time::second_clock::local_time());
            person.active(boost::posix_time::second_clock::local_time());
            //}
            
            _watcher.async_notify(msg.from);
            _watcher.async_notify(msg.to);
            
            return {
                {"success", true}
            };
        }else{
            throw udho::exceptions::http_error(boost::beast::http::status::not_found);
        }
    }else{
        throw udho::exceptions::http_error(boost::beast::http::status::not_found);
    }
}
