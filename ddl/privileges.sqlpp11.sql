CREATE TABLE privileges
(
    project bigint NOT NULL,
    resource bigint NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    role bigint,
    role_order bigint,
    role_name text,
    permission_id bigint,
    rank bigint,
    rank_order bigint,
    rank_name text,
    assigner bigint,
    assigner_first_name text,
    assigner_last_name text
);
