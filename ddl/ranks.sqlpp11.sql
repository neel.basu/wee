CREATE TABLE ranks
(
    id bigserial NOT NULL,
    name text NOT NULL,
    order bigint NOT NULL,
    permissive boolean NOT NULL,
    PRIMARY KEY (id)
)
