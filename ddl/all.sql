-- Table: public.ranks

-- DROP TABLE public.ranks;

CREATE TABLE public.ranks
(
    id bigserial,
    name text COLLATE pg_catalog."default" NOT NULL,
    "order" bigint NOT NULL,
    permissive boolean NOT NULL,
    CONSTRAINT ranks_pkey PRIMARY KEY (id),
    CONSTRAINT ranks_name_key UNIQUE (name)
);


INSERT INTO public.ranks VALUES (1, 'super', 0, true);
INSERT INTO public.ranks VALUES (2, 'operator', 1, true);
INSERT INTO public.ranks VALUES (3, 'creator', 2, false);
INSERT INTO public.ranks VALUES (4, 'writer', 3, false);
INSERT INTO public.ranks VALUES (5, 'user', 99, false);
INSERT INTO public.ranks VALUES (6, 'visitor', 100, false);

SELECT pg_catalog.setval(pg_get_serial_sequence('ranks', 'id'), MAX(id)) FROM ranks;

-- Table: public.roles

-- DROP TABLE public.roles;

CREATE TABLE public.roles
(
    id bigserial NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    "order" bigint NOT NULL,
    description text COLLATE pg_catalog."default",
    CONSTRAINT roles_pkey PRIMARY KEY (id)
);

INSERT INTO public.roles VALUES (1, 'admin', 0, 'Project Administrator');
INSERT INTO public.roles VALUES (2, 'moderator', 1, 'Project Moderator');
INSERT INTO public.roles VALUES (3, 'contributor', 2, 'contributor of a project');
INSERT INTO public.roles VALUES (6, 'guest', 99, 'Guest of the project');
INSERT INTO public.roles VALUES (4, 'reviewer', 3, 'Reviews the contents of a project');
INSERT INTO public.roles VALUES (5, 'locator', 4, 'Locates posts to a location');
INSERT INTO public.roles VALUES (7, 'member', 100, 'registered user');
INSERT INTO public.roles VALUES (8, 'public', 101, 'unregistered visitor');

SELECT pg_catalog.setval(pg_get_serial_sequence('roles', 'id'), MAX(id)) FROM roles;

-- Table: public.users

-- DROP TABLE public.users;

CREATE TABLE public.users
(
    id bigserial NOT NULL,
    first_name character varying COLLATE pg_catalog."default" NOT NULL,
    last_name character varying COLLATE pg_catalog."default" NOT NULL,
    email character varying COLLATE pg_catalog."default",
    phone character varying COLLATE pg_catalog."default" NOT NULL,
    pass character varying COLLATE pg_catalog."default" NOT NULL,
    designation character varying COLLATE pg_catalog."default",
    current_designation character varying COLLATE pg_catalog."default",
    photo text COLLATE pg_catalog."default",
    rank bigint NOT NULL DEFAULT 5,
    CONSTRAINT user_pk PRIMARY KEY (id),
    CONSTRAINT unique_phone_number UNIQUE (phone),
    CONSTRAINT users_rank_fkey FOREIGN KEY (rank)
        REFERENCES public.ranks (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

-- Table: public.resources

-- DROP TABLE public.resources;

CREATE TABLE public.resources
(
    id bigserial NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    description text COLLATE pg_catalog."default",
    role bigint,
    rank bigint,
    CONSTRAINT permissions_pkey PRIMARY KEY (id),
    CONSTRAINT name_unique UNIQUE (name),
    CONSTRAINT permissions_role_fkey FOREIGN KEY (role)
        REFERENCES public.roles (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT rank_fk FOREIGN KEY (rank)
        REFERENCES public.ranks (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

INSERT INTO public.resources VALUES (1,  'post:fetch', 'fetch the list of posts', 8, 6);
INSERT INTO public.resources VALUES (2,  'post:view', 'view a single post', 8, 6);
INSERT INTO public.resources VALUES (3,  'post:create', 'create a post', 3, 3);
INSERT INTO public.resources VALUES (4,  'post:update', 'update a post', 4, 3);
INSERT INTO public.resources VALUES (5,  'post:delete', 'delete a post', 3, 3);

INSERT INTO public.resources VALUES (6,  'place:view', 'view a single place', 8, 6);
INSERT INTO public.resources VALUES (7,  'place:fetch', 'fetch the map of the project', 8, 6);
INSERT INTO public.resources VALUES (8,  'place:create', 'locate a post', 5, 3);
INSERT INTO public.resources VALUES (9,  'place:update', 'update a place', 5, 3);
INSERT INTO public.resources VALUES (10, 'place:delete', 'delete a place', 5, 3);

INSERT INTO public.resources VALUES (11, 'project:fetch', 'View a project', 8, 6);
INSERT INTO public.resources VALUES (12, 'project:view', 'View a project', 8, 6);
INSERT INTO public.resources VALUES (13, 'project:create', 'create a project', NULL, 2);
INSERT INTO public.resources VALUES (14, 'project:mutate', 'update a project', 2, 3);
INSERT INTO public.resources VALUES (15, 'project:update', 'update a project', 2, 3);
INSERT INTO public.resources VALUES (16, 'project:delete', 'delete a project', 1, 3);
INSERT INTO public.resources VALUES (17, 'project:member', 'be contributor of any project', NULL, 3);

INSERT INTO public.resources VALUES (18, 'user:fetch', 'update an user', NULL, 1);
INSERT INTO public.resources VALUES (19, 'user:view', 'update an user', 8, 6);
INSERT INTO public.resources VALUES (20, 'user:create', 'create an user', NULL, 1);
INSERT INTO public.resources VALUES (21, 'user:update', 'update an user', NULL, 1);
INSERT INTO public.resources VALUES (22, 'user:restrict', 'restrict an user', NULL, 1);
INSERT INTO public.resources VALUES (23, 'user:delete', 'delete an user', NULL, 1);

INSERT INTO public.resources VALUES (24, 'member:fetch', 'view members of a project', 4, 3);
INSERT INTO public.resources VALUES (25, 'member:create', 'add a contributor', 2, 2);
INSERT INTO public.resources VALUES (26, 'member:update', 'update role of a contributor', 2, 2);
INSERT INTO public.resources VALUES (27, 'member:delete', 'remove a contributor', 2, 2);

SELECT pg_catalog.setval(pg_get_serial_sequence('resources', 'id'), MAX(id)) FROM resources;


-- Table: public.projects

-- DROP TABLE public.projects;

CREATE TABLE public.projects
(
    id bigserial NOT NULL,
    name character varying COLLATE pg_catalog."default" NOT NULL,
    label character varying COLLATE pg_catalog."default" NOT NULL,
    description character varying COLLATE pg_catalog."default",
    properties json,
    created timestamp without time zone NOT NULL DEFAULT now(),
    owner bigint NOT NULL,
    listing boolean NOT NULL DEFAULT true,
    CONSTRAINT projects_pkey PRIMARY KEY (id),
    CONSTRAINT projects_owner_fkey FOREIGN KEY (owner)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

-- Table: public.memberships

-- DROP TABLE public.memberships;

CREATE TABLE public.memberships
(
    id bigserial NOT NULL,
    project bigint NOT NULL,
    "user" bigint NOT NULL,
    recruiter bigint NOT NULL,
    since timestamp without time zone NOT NULL DEFAULT now(),
    role bigint NOT NULL,
    CONSTRAINT memberships_pkey PRIMARY KEY (id),
    CONSTRAINT project_user_unique UNIQUE (project, "user"),
    CONSTRAINT project_fk FOREIGN KEY (project)
        REFERENCES public.projects (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT recruiter_fk FOREIGN KEY (recruiter)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT roles_fk FOREIGN KEY (role)
        REFERENCES public.roles (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT user_fk FOREIGN KEY ("user")
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

-- Table: public.permissions

-- DROP TABLE public.permissions;

CREATE TABLE public.permissions
(
    id bigserial NOT NULL,
    resource bigint NOT NULL,
    role bigint NOT NULL,
    since timestamp without time zone NOT NULL DEFAULT now(),
    project bigint NOT NULL,
    rank bigint NOT NULL,
    assigner bigint NOT NULL,
    CONSTRAINT permissions_pkey1 PRIMARY KEY (id),
    CONSTRAINT project_resource_unique UNIQUE (project, resource),
    CONSTRAINT assigner_fk FOREIGN KEY (assigner)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT permissions_resource_fkey FOREIGN KEY (resource)
        REFERENCES public.resources (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT permissions_role_fkey1 FOREIGN KEY (role)
        REFERENCES public.roles (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT project_fk FOREIGN KEY (project)
        REFERENCES public.projects (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT rank_fk FOREIGN KEY (rank)
        REFERENCES public.ranks (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

-- FUNCTION: public.allowed_to(text, bigint, bigint)

-- DROP FUNCTION public.allowed_to(text, bigint, bigint);

CREATE OR REPLACE FUNCTION public.allowed_to(
	resource_name text,
	pid bigint,
	uid bigint,
	OUT allowed boolean,
	OUT reason text)
    RETURNS record
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    
AS $BODY$
DECLARE
	rank_order_required bigint;
	role_order_required bigint;
	user_rank_order bigint;
	user_rank_permissive boolean;
	user_role_order bigint;
BEGIN
	-- hardcoded assumptions about orders of rank and role
	-- rank 100 -> any visitor 
	-- role 100 -> any registered user
	-- role 101 -> any unregistered user
	-- end
	select rank_order, role_order from privileges where project is not distinct from NULLIF(pid, 0) and name = resource_name into rank_order_required, role_order_required;
	if rank_order_required is null and role_order_required is null then
		allowed := false;
		reason  := 'no such project or resource';
		return;
	end if;
	if rank_order_required >= 100 then  						-- any visitor is alowed to acess the resource
		allowed := true;										-- no need to check anything about the user
		reason  := format('Resource is public');
		raise info 'rank_order_required >= %', rank_order_required;
	else														-- a registered member is required to access this project resource combination
		select ranks.order, ranks.permissive from users	join ranks on ranks.id = users.rank	where users.id = uid into user_rank_order, user_rank_permissive;
		if user_rank_order is null and user_rank_permissive is null then
			allowed := false;
			reason  := 'no such user';
			raise info 'user id % does not exist', uid;
			return;
		end if;
		if user_rank_order <= rank_order_required then			-- user's rank qualifies the required rank order
			if user_rank_permissive then						-- user's rank is permissive 
				allowed := true;								-- no need to check role
				reason  := 'Access granted by rank';
				raise info 'user_rank_order <= rank_order_required %, user_rank_permissive is %', (user_rank_order <= rank_order_required), user_rank_permissive;
			else												-- user rank is not permissive
				select project_memberships.order from project_memberships where project_memberships.user = uid and project_memberships.project = pid into user_role_order;
				if user_role_order is null then					-- user is not associated as a member of the project
					user_role_order := 100;						-- set the user role to 100
				end if;
				allowed := user_role_order <= role_order_required;
				if allowed then
					reason  := 'Access granted';
				else 
					reason  := 'Access denied, project role insufficient';
				end if;
				raise info 'user_rank_order <= rank_order_required %, user_rank_permissive is %, user_role_order <= role_order_required is %', (user_rank_order <= rank_order_required), user_rank_permissive, user_role_order <= role_order_required;
			end if;
		else 
			allowed := false;
			reason  := 'Access denied, user rank insufficient';
			raise info 'user_rank_order <= rank_order_required %', (user_rank_order <= rank_order_required);
		end if;
	end if;
END
$BODY$;


-- View: public.privileges

-- DROP VIEW public.privileges;

CREATE OR REPLACE VIEW public.privileges
 AS
 SELECT projects.id AS project,
    resources.id AS resource,
    resources.name,
    resources.description,
    COALESCE(permissions.role, resources.role) AS role,
    roles."order" AS role_order,
    roles.name AS role_name,
    COALESCE(permissions.id, 0::bigint) AS permission_id,
    COALESCE(permissions.rank, resources.rank) AS rank,
    ranks."order" AS rank_order,
    ranks.name AS rank_name,
    COALESCE(permissions.assigner, 0::bigint) AS assigner,
    COALESCE(users.first_name, 'System'::character varying) AS assigner_first_name,
    COALESCE(users.last_name, 'Default'::character varying) AS assigner_last_name
   FROM projects
     LEFT JOIN resources ON resources.id > 0
     LEFT JOIN permissions ON resources.id = permissions.resource AND permissions.project = projects.id
     JOIN roles ON roles.id = COALESCE(permissions.role, resources.role)
     JOIN ranks ON ranks.id = COALESCE(permissions.rank, resources.rank)
     LEFT JOIN users ON users.id = COALESCE(permissions.assigner, 0::bigint)
UNION
 SELECT NULL::bigint AS project,
    resources.id AS resource,
    resources.name,
    resources.description,
    NULL::bigint AS role,
    NULL::bigint AS role_order,
    NULL::text AS role_name,
    0 AS permission_id,
    resources.rank,
    ranks."order" AS rank_order,
    NULL::text AS rank_name,
    0 AS assigner,
    'System'::character varying AS assigner_first_name,
    'Default'::character varying AS assigner_last_name
   FROM resources
     JOIN ranks ON ranks.id = resources.rank
  WHERE resources.role IS NULL
  ORDER BY 2, 1;

-- View: public.project_memberships

-- DROP VIEW public.project_memberships;

CREATE OR REPLACE VIEW public.project_memberships
 AS
 SELECT DISTINCT ON (sub."user", sub.project) sub."user",
    sub.project,
    sub.role,
    sub."order",
    sub.name,
    sub.membership_id,
    sub.since
   FROM ( SELECT memberships."user",
            memberships.project,
            memberships.role,
            roles."order",
            roles.name,
            memberships.id AS membership_id,
            memberships.since
           FROM memberships
             JOIN roles ON roles.id = memberships.role
        UNION
         SELECT projects.owner AS "user",
            projects.id AS project,
            1 AS role,
            0 AS "order",
            'admin'::text AS name,
            0 AS membership_id,
            projects.created AS since
           FROM projects) sub
  ORDER BY sub."user", sub.project, sub."order";
  

-- Table: public.types

-- DROP TABLE public.types;

CREATE TABLE public.types
(
    id bigserial NOT NULL,
    name character varying COLLATE pg_catalog."default" NOT NULL,
    label character varying COLLATE pg_catalog."default" NOT NULL,
    project bigint NOT NULL,
    properties json,
    CONSTRAINT types_pkey PRIMARY KEY (id),
    CONSTRAINT types_name_project_key UNIQUE (name, project),
    CONSTRAINT types_project_fkey FOREIGN KEY (project)
        REFERENCES public.projects (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

-- Table: public.reports

-- DROP TABLE public.reports;

CREATE TABLE public.reports
(
    id bigserial NOT NULL,
    aoi text COLLATE pg_catalog."default" NOT NULL,
    toi timestamp without time zone NOT NULL,
    properties json,
    created timestamp without time zone NOT NULL DEFAULT now(),
    story text COLLATE pg_catalog."default",
    author bigint,
    project bigint NOT NULL,
    type bigint NOT NULL DEFAULT 1,
    CONSTRAINT reports_pkey PRIMARY KEY (id),
    CONSTRAINT project_fkey FOREIGN KEY (project)
        REFERENCES public.projects (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT reports_author_fkey FOREIGN KEY (author)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT type_fk FOREIGN KEY (type)
        REFERENCES public.types (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

-- Table: public.places

-- DROP TABLE public.places;

CREATE TABLE public.places
(
    id bigserial NOT NULL,
    report bigint NOT NULL,
    type text COLLATE pg_catalog."default" NOT NULL,
    data json NOT NULL,
    created timestamp without time zone NOT NULL DEFAULT now(),
    updated timestamp without time zone NOT NULL DEFAULT now(),
    title text COLLATE pg_catalog."default" NOT NULL,
    properties json,
    author bigint NOT NULL,
    project bigint NOT NULL,
    CONSTRAINT places_pkey PRIMARY KEY (id),
    CONSTRAINT places_author_fkey FOREIGN KEY (author)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT places_report_fkey FOREIGN KEY (report)
        REFERENCES public.reports (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT project_fkey FOREIGN KEY (project)
        REFERENCES public.projects (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE EXTENSION pg_trgm;
