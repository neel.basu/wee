import Vue from 'vue'
import WeeNav from './WeeNav'
import WeeNavItem from './WeeNavItem'
import AvatarCropper from "vue-avatar-cropper"
import './style.css';

Vue.config.devtools = true

new Vue({
    el: '#main',
    data(){
        return {
            user: 0
        };
    },
    components: {
        WeeNav,
        WeeNavItem,
        AvatarCropper
    },
    mounted: function(){},
    methods: {
        handleUploaded(response){
            if(response.success){
                this.$refs.photo.src = "/uploads/dp/"+response.photo;
            }
        }
    }
});
