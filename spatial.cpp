/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "spatial.h"
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/split.hpp>
#include <cpr/cpr.h>
#include <polylineencoder.h>
#include <sqlpp11/value_or_null.h>
#include <sqlpp11/remove.h>
#include <sqlpp11/postgresql/insert.h>
#include <sqlpp11/postgresql/update.h>
#include <sqlpp11/postgresql/returning.h>
#include <sqlpp11/group_by.h>
#include <udho/util.h>
#include <boost/regex.hpp>
#include <xlnt/xlnt.hpp>
#include "ddl/reports.h"
#include "ddl/users.h"
#include "ddl/places.h"
#include "ddl/projects.h"
#include "ddl/types.h"
#include "data.h"

#define SEARCH_USING_GOOGLE 1
#define SEARCH_USING_MAPBOX 0
#define MATCH_USING_GOOGLE  0
#define MATCH_USING_MAPBOX  1

wee::apps::spatial::spatial(sqlpp::postgresql::connection& conn): base("spatial"), _connection(conn){}

nlohmann::json wee::apps::spatial::search(udho::contexts::stateful<wee::session::user> ctx, std::string keyword){
    nlohmann::json results  = nlohmann::json::array();
    std::size_t mapbox_provided = 0;
    std::size_t gmap_provided   = 0;
    std::size_t eci_provided    = 0;
    
    boost::regex part_expression("(\\d+)[\\-](\\d+)");
    boost::smatch matches;
    if(boost::regex_search(keyword, matches, part_expression)) {
        std::string acno = matches[1];
        std::string ptno = matches[2];
        try{
            auto response = cpr::Get(
                cpr::Url("https://electoralsearch.in/Home/SearchLatLong"),
                cpr::Parameters{
                    {"st_code", "S25"},
                    {"acno",    acno},
                    {"partno",  ptno}
                },
                cpr::Timeout{4000},
                cpr::VerifySsl{false}  
            );
            nlohmann::json json = nlohmann::json::parse(response.text);
            std::string latlong = json["latlong"].get<std::string>();
            std::vector<std::string> latlng_parts;
            boost::split(latlng_parts, latlong, boost::is_any_of(", "), boost::token_compress_on);
            double lat = boost::lexical_cast<double>(latlng_parts[1]);
            double lng = boost::lexical_cast<double>(latlng_parts[0]);
            if(lng <= lat){
                std::swap(lat, lng);
                ctx << udho::logging::messages::formatted::warning("wee::apps::spatial::search", "swaping lat long data for %1% - %2%") % acno % ptno;
            }
            nlohmann::json result = {
                {"name", (boost::format("Assembly %1% Part %2%") % acno % ptno).str()},
                {"coordinate", {
                    {"lat", lat},
                    {"lng", lng}
                }},
                {"provider", "ECI"}
            };
            results.push_back(result);
        }catch(const std::exception& ex){
            ctx << udho::logging::messages::formatted::error("wee::apps::spatial::search", "Failed to fetch ECI data for %1% - %2%\n error: %3%") % acno % ptno % ex.what();
        }
    }
    
    
    if(SEARCH_USING_MAPBOX){ // mapbox spatialcoding results
        try{
            nlohmann::json results_mabbox = search_mapbox(keyword);
            gmap_provided = results_mabbox.size();
            for(const auto& res: results_mabbox){
                results.push_back(res);
            }
        }catch(const std::exception& ex){
            ctx << udho::logging::messages::formatted::error("wee::apps::spatial::search", "Failed to parse mapbox response\n error: %1%") % ex.what();
        }
    }
    
    if(SEARCH_USING_GOOGLE){ // google spatialcoding results
        try{
            nlohmann::json results_google = search_google(keyword);
            mapbox_provided = results_google.size();
            for(const auto& res: results_google){
                results.push_back(res);
            }
        }catch(const std::exception& ex){
            ctx << udho::logging::messages::formatted::error("wee::apps::spatial::search", "Failed to parse google response\n error: %1%") % ex.what();
        }
    }
    
    return {
        {"count", results.size()},
        {"providers", {
            {"mapbox", mapbox_provided},
            {"google", gmap_provided},
            {"eci", eci_provided}
        }},
        {"results", results}
    };
}

std::string wee::apps::spatial::thumbnail(udho::contexts::stateless /*ctx*/, double lat, double lng, std::size_t width, std::size_t height, std::size_t zoom){
    std::string center = (boost::format("%1%,%2%") % lat % lng).str();
    std::string size   = (boost::format("%1%x%2%") % width % height).str();
    auto response = cpr::Get(
        cpr::Url("https://maps.googleapis.com/maps/api/staticmap"),
        cpr::Parameters{
            {"maptype", "roadmap"},
            {"center", center},
            {"size", size},
            {"zoom", boost::lexical_cast<std::string>(zoom)},
            {"key", "AIzaSyC9zXuPtasjdOKQR0tVcwPNXxTaIoC9wgA"}
        },
        cpr::Timeout{4000},
        cpr::VerifySsl{false}
    );
    return response.text;
}

nlohmann::json wee::apps::spatial::match(udho::contexts::stateful<wee::session::user> ctx){
    std::string body = ctx.request().body();
    nlohmann::json contents = nlohmann::json::parse(body);
    nlohmann::json points = contents["points"];
    nlohmann::json results = match_mapbox(points);
    return results;
}

nlohmann::json wee::apps::spatial::search_mapbox(std::string keyword){
    std::string keyword_encoded = udho::util::urlencode(keyword);
    auto response = cpr::Get(
        cpr::Url("https://api.mapbox.com/spatialcoding/v5/mapbox.places/"+keyword_encoded+".json"),
        cpr::Parameters{
            {"country", "IN"},
            {"access_token", "pk.eyJ1IjoibmVlbC1iYXN1IiwiYSI6ImNqNTcwNGttcTByb3UycXBteDV0dGp2dTcifQ.0gpnb7D-LwOTQeGMuO0WXw"}
        },
        cpr::Timeout{4000},
        cpr::VerifySsl{false}
    );
    nlohmann::json results  = nlohmann::json::array();
    nlohmann::json json     = nlohmann::json::parse(response.text);
    nlohmann::json features = json["features"];
    for(const nlohmann::json& feature: features){
        std::string name = feature["place_name"].get<std::string>();
        nlohmann::json coordinate = feature["spatialmetry"]["coordinates"];
        results.push_back({
            {"name", name},
            {"coordinate", {
                {"lat", coordinate[1].get<double>()},
                {"lng", coordinate[0].get<double>()}
            }},
            {"provider", "mapbox"}
        });
    }
    return results;
}

nlohmann::json wee::apps::spatial::search_google(std::string keyword){
    std::string keyword_encoded = keyword; // udho::util::urlencode(keyword); // ! do not urlencode because keyword is already urlencoded
    auto response = cpr::Get(
        cpr::Url("https://maps.googleapis.com/maps/api/geocode/json"),
        cpr::Parameters{
            {"address", keyword_encoded},
            {"key", "AIzaSyC9zXuPtasjdOKQR0tVcwPNXxTaIoC9wgA"}
        },
        cpr::Timeout{4000},
        cpr::VerifySsl{false}
    );
    nlohmann::json results  = nlohmann::json::array();
    nlohmann::json json     = nlohmann::json::parse(response.text);
    nlohmann::json gresults = json["results"];
    for(const nlohmann::json& result: gresults){
        std::string name = result["formatted_address"].get<std::string>();
        nlohmann::json location = result["geometry"]["location"];
        results.push_back({
            {"name", name},
            {"coordinate", location},
            {"provider", "google"}
        });
    }
    return results;
}


nlohmann::json wee::apps::spatial::match_mapbox(nlohmann::json points){
    std::vector<std::string> list;
    for(const nlohmann::json& point: points){
        list.push_back((boost::format("%1%,%2%") % point["lng"].get<double>() % point["lat"].get<double>()).str());
    }
    std::string input_points = boost::algorithm::join(list, ";");
    auto response = cpr::Get(
        cpr::Url("https://api.mapbox.com/matching/v5/mapbox/driving/"+input_points),
        cpr::Parameters{
            {"access_token", "pk.eyJ1IjoibmVlbC1iYXN1IiwiYSI6ImNqNTcwNGttcTByb3UycXBteDV0dGp2dTcifQ.0gpnb7D-LwOTQeGMuO0WXw"}
        },
        cpr::VerifySsl{false}
    );
    nlohmann::json mapbox_api_result = nlohmann::json::parse(response.text);
    bool mapbox_api_found_match = mapbox_api_result.count("code") && (mapbox_api_result["code"].get<std::string>() == "Ok");
    nlohmann::json mapbox_result;
    if(mapbox_api_found_match){
        // take the first match only
        nlohmann::json matching = mapbox_api_result["matchings"][0];
        double confidence = matching["confidence"].get<double>();
        std::string geometry = matching["geometry"].get<std::string>();
        nlohmann::json path = nlohmann::json::array();
        auto polyline = PolylineEncoder::decode(geometry);
        for (const auto &point : polyline) {
            double lat = std::get<0>(point);
            double lng = std::get<1>(point);
            path.push_back({
                {"lng", lng},
                {"lat", lat}
            });
        }
        mapbox_result = {
            {"matched", true},
            {"provider", "mapbox"},
            {"confidence", confidence},
            {"polyline", path}
        };
    }else{
        return {
            {"matched", false}
        };
    }
    
    return mapbox_result;
}

nlohmann::json wee::apps::spatial::match_google(nlohmann::json points){
    std::vector<std::string> list;
    for(const nlohmann::json& point: points){
        list.push_back((boost::format("%1%,%2%") % point["lat"].get<double>() % point["lng"].get<double>()).str());
    }
    std::string input_points = boost::algorithm::join(list, "|");
    auto response = cpr::Get(
        cpr::Url("https://roads.googleapis.com/v1/snapToRoads"),
        cpr::Parameters{
            {"path", input_points},
            {"interpolate", "true"},
            {"key", "AIzaSyC9zXuPtasjdOKQR0tVcwPNXxTaIoC9wgA"}
        },
        cpr::VerifySsl{false}
    );
    std::cout << response.url << std::endl;
    std::string body         = response.text;
    std::cout << "body " << body << std::endl;
    nlohmann::json polyline = nlohmann::json::array();
    try{
        nlohmann::json json      = nlohmann::json::parse(body);
        nlohmann::json matched   = json["snappedPoints"];
        for(const nlohmann::json& point: matched){
            polyline.push_back({
                {"lat", point["location"]["latitude"].get<double>()},
                {"lng", point["location"]["longitude"].get<double>()}
            });
        }
    }catch(const nlohmann::json::exception& ex){
        std::cout << ex.what() << std::endl;
        std::cout << body << std::endl;
        throw ex;
    }

    if(polyline.size()){
        return {
            {"matched", true},
            {"provider", "google"},
            {"polyline", polyline}
        };
    }else{
        return {
            {"matched", false}
        };
    }
}
