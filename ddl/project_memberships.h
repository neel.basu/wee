// generated by ../deps/sqlpp11/scripts/ddl2cpp project_memberships.sqlpp11.sql project_memberships wee
#ifndef WEE_PROJECT_MEMBERSHIPS_H
#define WEE_PROJECT_MEMBERSHIPS_H

#include <sqlpp11/table.h>
#include <sqlpp11/data_types.h>
#include <sqlpp11/char_sequence.h>

namespace wee
{
  namespace ProjectMemberships_
  {
    struct User
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "user";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T user;
            T& operator()() { return user; }
            const T& operator()() const { return user; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::bigint, sqlpp::tag::require_insert>;
    };
    struct Project
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "project";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T project;
            T& operator()() { return project; }
            const T& operator()() const { return project; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::bigint, sqlpp::tag::require_insert>;
    };
    struct Role
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "role";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T role;
            T& operator()() { return role; }
            const T& operator()() const { return role; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::bigint, sqlpp::tag::require_insert>;
    };
    struct Order
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "!order";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T order;
            T& operator()() { return order; }
            const T& operator()() const { return order; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::bigint, sqlpp::tag::can_be_null>;
    };
    struct Name
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "name";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T name;
            T& operator()() { return name; }
            const T& operator()() const { return name; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::text, sqlpp::tag::can_be_null>;
    };
    struct MembershipId
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "membership_id";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T membershipId;
            T& operator()() { return membershipId; }
            const T& operator()() const { return membershipId; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::bigint, sqlpp::tag::can_be_null>;
    };
    struct Since
    {
      struct _alias_t
      {
        static constexpr const char _literal[] =  "since";
        using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
        template<typename T>
        struct _member_t
          {
            T since;
            T& operator()() { return since; }
            const T& operator()() const { return since; }
          };
      };
      using _traits = sqlpp::make_traits<sqlpp::time_point, sqlpp::tag::can_be_null>;
    };
  } // namespace ProjectMemberships_

  struct ProjectMemberships: sqlpp::table_t<ProjectMemberships,
               ProjectMemberships_::User,
               ProjectMemberships_::Project,
               ProjectMemberships_::Role,
               ProjectMemberships_::Order,
               ProjectMemberships_::Name,
               ProjectMemberships_::MembershipId,
               ProjectMemberships_::Since>
  {
    struct _alias_t
    {
      static constexpr const char _literal[] =  "project_memberships";
      using _name_t = sqlpp::make_char_sequence<sizeof(_literal), _literal>;
      template<typename T>
      struct _member_t
      {
        T projectMemberships;
        T& operator()() { return projectMemberships; }
        const T& operator()() const { return projectMemberships; }
      };
    };
  };
} // namespace wee
#endif
