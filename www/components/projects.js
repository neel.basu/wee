import Vue from 'vue'
import WeeNav from './WeeNav'
import WeeNavItem from './WeeNavItem'
import WeeSpatialProjects from './WeeSpatialProjects'
import WeeSpatialProjectEditor from './WeeSpatialProjectEditor'
import './style.css';

Vue.config.devtools = true

new Vue({
    el: '#main',
    data(){
        return {
            project: null
        };
    },
    components: {
        WeeNav,
        WeeNavItem,
        WeeSpatialProjects,
        WeeSpatialProjectEditor
    },
    mounted: function(){},
    methods: {
        selected(p){
            this.project = p.id;
        },
        backToProjects(){
            this.project = null;
        }
    }
});
