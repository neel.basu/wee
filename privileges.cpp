/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "privileges.h"
#include "ddl/users.h"
#include "ddl/projects.h"
#include "ddl/memberships.h"
#include "ddl/project_memberships.h"
#include "ddl/privileges.h"
#include "ddl/permissions.h"
#include "ddl/roles.h"
#include "ddl/ranks.h"
#include <sqlpp11/value_or_null.h>
#include <sqlpp11/remove.h>
#include <sqlpp11/postgresql/insert.h>
#include <sqlpp11/postgresql/update.h>
#include <sqlpp11/postgresql/returning.h>
#include <sqlpp11/group_by.h>

wee::apps::privileges::privileges(sqlpp::postgresql::connection& conn): base("privileges"), _connection(conn){
    
}

nlohmann::json wee::apps::privileges::fetch(udho::contexts::stateful<wee::session::user> ctx, std::size_t project){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
    
        wee::Projects projects;
        auto project_records = _connection(select(projects.id, projects.owner).from(projects).where(projects.id == project));
        if(!project_records.size()){
            throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
        }
        std::size_t owner = project_records.front().owner;
        std::string reason;
        if(loggedin_user.id() != owner && !allowed_to("project:mutate", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
        
        wee::Roles roles;
        wee::Ranks ranks;
        wee::Privileges privileges;
        
        auto role_records = _connection(select(roles.id, roles.name, roles.order).from(roles).order_by(roles.order.asc()).unconditionally());
        auto rank_records = _connection(select(ranks.id, ranks.name, ranks.order, ranks.permissive).from(ranks).order_by(ranks.order.asc()).unconditionally());
        auto records = _connection(select(
            privileges.resource,
            privileges.name,
            privileges.description,
            privileges.role,
            privileges.rank,
            privileges.permissionId,
            privileges.assigner,
            privileges.assignerFirstName,
            privileges.assignerLastName
        ).from(privileges).where(privileges.project == project));
        
        nlohmann::json roles_json = nlohmann::json::array();
        for(const auto& record: role_records){
            roles_json.push_back({
                {"id",    record.id.value()},
                {"name",  record.name.value()},
                {"order", record.order.value()}
            });
        }
        
        nlohmann::json ranks_json = nlohmann::json::array();
        for(const auto& record: rank_records){
            ranks_json.push_back({
                {"id",         record.id.value()},
                {"name",       record.name.value()},
                {"order",      record.order.value()},
                {"permissive", record.permissive.value()}
            });
        }
        
        std::size_t seq = 0;
        nlohmann::json privileges_json = nlohmann::json::array();
        for(const auto& record: records){
            privileges_json.push_back({
                {"seq", seq++},
                {"resource", {
                    {"id",          record.resource.value()},
                    {"name",        record.name.value()},
                    {"description", record.description.value()}
                }},
                {"role",            record.role.value()},
                {"rank",            record.rank.value()},
                {"permission",      record.permissionId.value()},
                {"assigner", {
                    {"id",          record.assigner.value()},
                    {"first",       record.assignerFirstName},
                    {"last",        record.assignerLastName}
                }}
            });
        }
        
        return {
            {"privileges", privileges_json},
            {"roles", roles_json},
            {"ranks", ranks_json}
        };
    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}

nlohmann::json wee::apps::privileges::permit(udho::contexts::stateful<wee::session::user> ctx, std::size_t project, std::size_t id){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        wee::Projects projects;
        auto project_records = _connection(select(projects.id, projects.owner).from(projects).where(projects.id == project));
        if(!project_records.size()){
            throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
        }
        std::size_t owner = project_records.front().owner;
        std::string reason;
        if(loggedin_user.id() != owner && !allowed_to("project:mutate", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    
        std::string body = ctx.request().body();
        nlohmann::json json = nlohmann::json::parse(body);
        
        udho::form::required<std::size_t> resource("resource", "resource must not be empty");
        udho::form::required<std::size_t> role("role", "role must not be empty");
        udho::form::required<std::size_t> rank("rank", "rank must not be empty");
        
        auto validator = udho::form::validate();
        if(json.contains("resource")){
            if(json["resource"].is_number()){
                validator << resource.unchecked(json["resource"].get<std::size_t>());
            }else if(json["resource"].is_string()){
                validator << resource(json["resource"].get<std::string>());
            }else{
                validator << resource();
            }
        }else{
            validator << resource();
        }
        if(json.contains("role")){
            if(json["role"].is_number()){
                validator << role.unchecked(json["role"].get<std::size_t>());
            }else if(json["role"].is_string()){
                validator << role(json["role"].get<std::string>());
            }else{
                validator << role();
            }
        }else{
            validator << role();
        }
        if(json.contains("rank")){
            if(json["rank"].is_number()){
                validator << rank.unchecked(json["rank"].get<std::size_t>());
            }else if(json["rank"].is_string()){
                validator << rank(json["rank"].get<std::string>());
            }else{
                validator << rank();
            }
        }else{
            validator << rank();
        }
        
        if(!validator.valid()){
            return {
                {"error", true},
                {"success", false},
                {"message", validator.errors().size() ? validator.errors().front() : std::string("Invalid input")},
                {"details", {
                    {"errors", validator.errors()},
                    {"fields", {
                        {"resource", {
                            {"valid", validator["resource"].valid()},
                            {"error", validator["resource"].error()}
                        }},
                        {"role", {
                            {"valid", validator["role"].valid()},
                            {"error", validator["role"].error()}
                        }},
                        {"rank", {
                            {"valid", validator["rank"].valid()},
                            {"error", validator["rank"].error()}
                        }}
                    }}
                }}
            };
        }
        
        wee::Permissions permissions;
        if(!id){
            auto results = _connection(sqlpp::postgresql::insert_into(permissions).set(
                permissions.resource = resource.value(),
                permissions.role     = role.value(),
                permissions.rank     = rank.value(),
                permissions.project  = project,
                permissions.assigner = loggedin_user.id()
            ).returning(permissions.id));
            
            return {
                {"id", results.front().id.value()},
                {"success", true}
            };
        }else{
            _connection(update(permissions).set(
                permissions.role     = role.value(),
                permissions.rank     = rank.value(),
                permissions.assigner = loggedin_user.id()
            ).where(permissions.id == id and permissions.project == project and permissions.resource == resource.value()));
            return {
                {"id", id},
                {"success", true}
            };
        }
    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}

nlohmann::json wee::apps::privileges::revoke(udho::contexts::stateful<wee::session::user> ctx, std::size_t project, std::size_t id){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        wee::Projects projects;
        auto project_records = _connection(select(projects.id, projects.owner).from(projects).where(projects.id == project));
        if(!project_records.size()){
            throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
        }
        std::size_t owner = project_records.front().owner;
        std::string reason;
        if(loggedin_user.id() != owner && !allowed_to("project:mutate", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
            
        wee::Permissions permissions;

        _connection(remove_from(permissions).where(permissions.id == id and permissions.project == project));
        return {
            {"id", id},
            {"success", true}
        };

    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}

bool wee::apps::privileges::allowed_to(const std::string& resource, std::size_t project, std::size_t uid, std::string& reason){
    std::string sql = (boost::format("select * from allowed_to('%1%', %2%, %3%)") % resource % project % uid).str();
    auto statement = custom_query(sqlpp::verbatim(sql))
        .with_result_type_of(select(
            sqlpp::value(false).as(sqlpp::alias::a),
            sqlpp::value("").as(sqlpp::alias::b)
        ));
    auto records = _connection(statement);
    const auto& permission = records.front();
    reason = permission.b.value();
    return permission.a.value();
}
