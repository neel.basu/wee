CREATE TABLE messages
(
    id bigserial NOT NULL,
    from bigint NOT NULL,
    to bigint NOT NULL,
    type text NOT NULL,
    category text NOT NULL,
    content text,
    created timestamp without time zone DEFAULT NOW(),
    delivered timestamp without time zone
    PRIMARY KEY (id)
);
