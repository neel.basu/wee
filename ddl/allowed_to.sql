CREATE OR REPLACE FUNCTION allowed_to(resource text, pid bigint, uid bigint, out allowed boolean, out reason text) as $$
DECLARE
	rank_order_required bigint;
	role_order_required bigint;
	user_rank_order bigint;
	user_rank_permissive boolean;
	user_role_order bigint;
BEGIN
	-- hardcoded assumptions about orders of rank and role
	-- rank 100 -> any visitor 
	-- role 100 -> any registered user
	-- role 101 -> any unregistered user
	-- end
	select rank_order, role_order from privileges where project = pid and name = resource into rank_order_required, role_order_required;
	if rank_order_required is null and role_order_required is null then
		allowed := false;
		reason  := 'no such project or resource';
		return;
	end if;
	if rank_order_required >= 100 then  						-- any visitor is alowed to acess the resource
		allowed := true;										-- no need to check anything about the user
		reason  := format('Resource is public');
		raise info 'rank_order_required >= %', rank_order_required;
	else														-- a registered member is required to access this project resource combination
		select ranks.order, ranks.permissive from users	join ranks on ranks.id = users.rank	where users.id = uid into user_rank_order, user_rank_permissive;
		if user_rank_order is null and user_rank_permissive is null then
			allowed := false;
			reason  := 'no such user';
			raise info 'user id % does not exist', uid;
			return;
		end if;
		if user_rank_order <= rank_order_required then			-- user's rank qualifies the required rank order
			if user_rank_permissive then						-- user's rank is permissive 
				allowed := true;								-- no need to check role
				reason  := 'Access granted by rank';
				raise info 'user_rank_order <= rank_order_required %, user_rank_permissive is %', (user_rank_order <= rank_order_required), user_rank_permissive;
			else												-- user rank is not permissive
				select project_memberships.order from project_memberships where project_memberships.user = uid and project_memberships.project = pid into user_role_order;
				if user_role_order is null then					-- user is not associated as a member of the project
					user_role_order := 100;						-- set the user role to 100
				end if;
				allowed := user_role_order <= role_order_required;
				if allowed then
					reason  := 'Access granted';
				else 
					reason  := 'Access denied, project role insufficient';
				end if;
				raise info 'user_rank_order <= rank_order_required %, user_rank_permissive is %, user_role_order <= role_order_required is %', (user_rank_order <= rank_order_required), user_rank_permissive, user_role_order <= role_order_required;
			end if;
		else 
			allowed := false;
			reason  := 'Access denied, user rank insufficient';
			raise info 'user_rank_order <= rank_order_required %', (user_rank_order <= rank_order_required);
		end if;
	end if;
END
$$ LANGUAGE plpgsql;