import Vue from 'vue'
import WeeNav from './WeeNav'
import WeeNavItem from './WeeNavItem'
import WeeSpatialStaticMap from './WeeSpatialStaticMap'
import './style.css';

Vue.config.devtools = true

new Vue({
    el: '#main',
    data(){
        return {};
    },
    components: {
        WeeNav,
        WeeNavItem,
        WeeSpatialStaticMap
    },
    mounted: function(){},
    methods: {}
});
