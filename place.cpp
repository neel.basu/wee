/*
 * Copyright (c) 2020, Neel Basu <neel.basu.z@gmail.com>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Neel Basu <neel.basu.z@gmail.com> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Neel Basu <neel.basu.z@gmail.com> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "place.h"
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/split.hpp>
#include <sqlpp11/value_or_null.h>
#include <sqlpp11/remove.h>
#include <sqlpp11/postgresql/insert.h>
#include <sqlpp11/postgresql/update.h>
#include <sqlpp11/postgresql/returning.h>
#include <sqlpp11/group_by.h>
#include <udho/util.h>
#include <boost/regex.hpp>
#include <xlnt/xlnt.hpp>
#include "ddl/reports.h"
#include "ddl/users.h"
#include "ddl/places.h"
#include "ddl/projects.h"
#include "ddl/types.h"
#include "ddl/memberships.h"
#include "data.h"

SQLPP_ALIAS_PROVIDER(states);
SQLPP_ALIAS_PROVIDER(report_id);
SQLPP_ALIAS_PROVIDER(type_id);
SQLPP_ALIAS_PROVIDER(type_name);
SQLPP_ALIAS_PROVIDER(type_label);

wee::apps::place::place(sqlpp::postgresql::connection& conn): base("place"), _connection(conn){}

nlohmann::json wee::apps::place::fetch(udho::contexts::stateful<wee::session::user> ctx, std::size_t project, std::size_t limit, std::size_t last){
    wee::Projects projects;
    auto project_records = _connection(select(projects.id, projects.owner).from(projects).where(projects.id == project));
    if(!project_records.size()){
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
    }
    const auto& project_record = project_records.front();
    std::size_t owner = project_record.owner;
    
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        std::string reason;
        if(loggedin_user.id() != owner && !allowed_to("place:fetch", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }else{
        std::string reason;
        if(!allowed_to("place:fetch", project, 0, reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }
    
    wee::Places   places;
    wee::Users    users;
    wee::Reports  reports;
    wee::Types    types;
    auto records = _connection(
        select(
            places.id,
            places.type,
            places.data,
            places.created,
            places.title,
            places.properties.as(states),
            places.author,
            users.firstName, 
            users.lastName,
            reports.id.as(report_id),
            reports.type.as(type_id),
            reports.toi,
            reports.aoi,
            reports.story,
            reports.properties,
            reports.project,
            types.name.as(type_name),
            types.label.as(type_label)
        ).from(
            places.join(users).on(places.author == users.id)
                  .left_outer_join(reports).on(places.report == reports.id)
                  .left_outer_join(types).on(reports.type == types.id)
        ).limit(limit).order_by(places.id.asc())
        .where(reports.project == project && places.id > last)
    );
    
    nlohmann::json places_attached = nlohmann::json::array();
    for(const auto& record: records){
        std::time_t place_time = std::chrono::system_clock::to_time_t(record.created.value());
        std::string created_str(std::ctime(&place_time));
        
        std::time_t report_time = std::chrono::system_clock::to_time_t(record.toi.value());
        std::string toi_str(std::ctime(&report_time));
        
        nlohmann::json place_attached = {
            {"id",         record.id.value()},
            {"type",       record.type},
            {"created",    place_time},
            {"title",      record.title},
            {"data",       nlohmann::json::parse(record.data.value())},
            {"properties", nlohmann::json::parse(record.states.value())},
            {"report", {
                {"id",      record.report_id.value()},
                {"story",   record.story},
                {"aoi",     record.aoi},
                {"type", {
                    {"id",     record.type_id.value()},
                    {"name",   record.type_name},
                    {"label",  record.type_label}
                }},
                {"toi",     report_time},
                {"project", record.project.value()},
                {"properties", nlohmann::json::parse(record.properties.value())}
            }},
            {"author", {
                {"id",    ctx.session().exists<wee::session::user>() ? record.author.value() : 0},
                {"first", ctx.session().exists<wee::session::user>() ? record.firstName : std::string("VoxPeople")},
                {"last",  ctx.session().exists<wee::session::user>() ? record.lastName : std::string()}
            }}
        };
        places_attached.push_back(place_attached);
    }
    std::pair<std::size_t, std::size_t> range(last, last);
    if(records.size() >= 1){
        range.first  = places_attached.front()["id"].get<std::size_t>();
        range.second = places_attached.back()["id"].get<std::size_t>();
    }
    return {
        {"count", places_attached.size()},
        {"places", places_attached},
        {"limit", limit},
        {"range", {
            {"begin", range.first},
            {"end", range.second}
        }}
    };
}

nlohmann::json wee::apps::place::retrieve(udho::contexts::stateful<wee::session::user> ctx, std::size_t id){
    wee::Places   places;
    wee::Users    users;
    wee::Reports  reports;
    wee::Types    types;
    auto records = _connection(
        select(
            places.id,
            places.type,
            places.data,
            places.created,
            places.title,
            places.properties.as(states),
            places.author,
            places.project,
            users.firstName, 
            users.lastName,
            reports.id.as(report_id),
            reports.type.as(type_id),
            reports.toi,
            reports.aoi,
            reports.story,
            reports.properties,
            types.name.as(type_name),
            types.label.as(type_label)
        ).from(
            places.join(users).on(places.author == users.id)
                  .left_outer_join(reports).on(places.report == reports.id)
                  .left_outer_join(types).on(reports.type == types.id)
        ).order_by(places.id.asc())
        .where(places.id == id)
    );
    
    if(!records.size()){
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such place");
    }
    
    auto& record = records.front();
    std::size_t project = record.project.value();
    
    wee::Projects projects;
    auto project_records = _connection(select(projects.id, projects.owner).from(projects).where(projects.id == project));
    if(!project_records.size()){
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Project");
    }
    const auto& project_record = project_records.front();
    std::size_t owner = project_record.owner;
    
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        std::string reason;
        if(loggedin_user.id() != owner && !allowed_to("place:view", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }else{
        std::string reason;
        if(!allowed_to("place:view", project, 0, reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }

    std::time_t place_time = std::chrono::system_clock::to_time_t(record.created.value());
    std::string created_str(std::ctime(&place_time));
    
    std::time_t report_time = std::chrono::system_clock::to_time_t(record.toi.value());
    std::string toi_str(std::ctime(&report_time));
    
    nlohmann::json place_attached = {
        {"id",         record.id.value()},
        {"type",       record.type},
        {"created",    place_time},
        {"title",      record.title},
        {"data",       nlohmann::json::parse(record.data.value())},
        {"properties", nlohmann::json::parse(record.states.value())},
        {"report", {
            {"id",    record.report_id.value()},
            {"story", record.story},
            {"aoi",   record.aoi},
            {"type", {
                {"id",     record.type.value()},
                {"name",   record.type_name},
                {"label",  record.type_label}
            }},
            {"toi",   report_time},
            {"properties", nlohmann::json::parse(record.properties.value())}
        }},
        {"author", {
            {"id",    ctx.session().exists<wee::session::user>() ? record.author.value() : 0},
            {"first", ctx.session().exists<wee::session::user>() ? record.firstName : std::string("VoxPeople")},
            {"last",  ctx.session().exists<wee::session::user>() ? record.lastName : std::string()}
        }}
    };

    return {
        {"place", place_attached}
    };
}

nlohmann::json wee::apps::place::of(udho::contexts::stateful<wee::session::user> ctx, std::size_t report){
    wee::Reports reports;
    auto report_records = _connection(select(reports.id, reports.project, reports.author).from(reports).where(reports.id == report));
    if(!report_records.size()){
        throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Report");
    }
    const auto& report_record = report_records.front();
    std::size_t project = report_record.project.value();
    std::size_t author  = report_record.author.value();
    
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        std::string reason;
        if(loggedin_user.id() != author && !allowed_to("post:view", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }else{
        std::string reason;
        if(!allowed_to("post:view", project, 0, reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
    }
    
    wee::Places  places;
    wee::Users   users;
    
    auto records = _connection(
        select(
            places.id,
            places.type,
            places.data,
            places.created,
            places.updated,
            places.title,
            places.properties,
            places.author,
            users.firstName, 
            users.lastName
        ).from(
            places.join(users)
            .on(places.author == users.id)
        ).where(places.report == report)
    );
    
    nlohmann::json places_attached = nlohmann::json::array();
    for(const auto& record: records){
        std::time_t created_time = std::chrono::system_clock::to_time_t(record.created.value());
        std::string created_str(std::ctime(&created_time));
        
        std::time_t updated_time = std::chrono::system_clock::to_time_t(record.updated.value());
        std::string toi_str(std::ctime(&updated_time));
        
        nlohmann::json place_attached = {
            {"id",         record.id.value()},
            {"type",       record.type},
            {"created",    created_time},
            {"updated",    updated_time},
            {"title",      record.title},
            {"data",       nlohmann::json::parse(record.data.value())},
            {"properties", nlohmann::json::parse(record.properties.value())},
            {"author", {
                {"id",    ctx.session().exists<wee::session::user>() ? record.author.value() : 0},
                {"first", ctx.session().exists<wee::session::user>() ? record.firstName : std::string("VoxPeople")},
                {"last",  ctx.session().exists<wee::session::user>() ? record.lastName : std::string()}
            }}
        };
        places_attached.push_back(place_attached);
    }
    return {
        {"report", report},
        {"places", places_attached}
    };
}

nlohmann::json wee::apps::place::create(udho::contexts::stateful<wee::session::user> ctx, std::size_t report){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        wee::Reports reports;
        auto report_records = _connection(select(reports.project, reports.author).from(reports).where(reports.id == report));
        if(!report_records.size()){
            throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Report");
        }
        std::size_t project = report_records.front().project;
        std::size_t author  = report_records.front().author;

        std::string reason;
        if(loggedin_user.id() != author && !allowed_to("place:create", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
        
        nlohmann::json ids = nlohmann::json::array();
        std::string body = ctx.request().body();
        nlohmann::json json = nlohmann::json::parse(body);
        
        udho::form::required<std::string> type("type", "Type must not be empty");
        udho::form::required<std::string> title("title", "Title must not be empty");
        
        auto validator = udho::form::validate();
        validator << (json.contains("type")  && json["type"].is_string()  ? type(json["type"]  .get<std::string>()) : type());
        validator << (json.contains("title") && json["title"].is_string() ? title(json["title"].get<std::string>()) : title());
        
        if(!validator.valid()){
            return {
                {"error", true},
                {"success", false},
                {"message", validator.errors().size() ? validator.errors().front() : std::string("Invalid input")},
                {"details", {
                    {"errors", validator.errors()},
                    {"fields", {
                        {"type", {
                            {"valid", validator["type"].valid()},
                            {"error", validator["type"].error()}
                        }},
                        {"title", {
                            {"valid", validator["title"].valid()},
                            {"error", validator["title"].error()}
                        }}
                    }}
                }}
            };
        }
               
        if(json.type() == nlohmann::json::value_t::array){
            for(auto it = json.begin(); it != json.end(); ++it){
                ids.push_back(add_place(*it, project, report, loggedin_user.id()));
            }
        }else{
            ids.push_back(add_place(json, project, report, loggedin_user.id()));
        }
        return {
            {"success", true},
            {"places", ids}
        };
    }
    
    return {
        {"error", true},
        {"message", "user not logged in"}
    };
}

nlohmann::json wee::apps::place::update(udho::contexts::stateful<wee::session::user> ctx, std::size_t place){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        wee::Places  places;
        wee::Reports reports;
        
        auto place_records = _connection(select(places.project, places.author, reports.author.as(sqlpp::alias::a)).from(places.join(reports).on(reports.id == places.report)).where(places.id == place));
        if(!place_records.size()){
            throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Place");
        }
        std::size_t project = place_records.front().project;
        std::size_t author  = place_records.front().author;
        std::size_t owner   = place_records.front().a;
        
        std::string reason;
        if(loggedin_user.id() != author && loggedin_user.id() != owner && !allowed_to("place:update", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
        
        nlohmann::json ids = nlohmann::json::array();
        std::string body = ctx.request().body();
        nlohmann::json json = nlohmann::json::parse(body);
        
        udho::form::required<std::string> type("type", "Type must not be empty");
        udho::form::required<std::string> title("title", "Title must not be empty");
        
        auto validator = udho::form::validate();
        validator << (json.contains("type")  && json["type"].is_string()  ? type(json["type"]  .get<std::string>()) : type());
        validator << (json.contains("title") && json["title"].is_string() ? title(json["title"].get<std::string>()) : title());
        
        if(!validator.valid()){
            return {
                {"error", true},
                {"success", false},
                {"message", validator.errors().size() ? validator.errors().front() : std::string("Invalid input")},
                {"details", {
                    {"errors", validator.errors()},
                    {"fields", {
                        {"type", {
                            {"valid", validator["type"].valid()},
                            {"error", validator["type"].error()}
                        }},
                        {"title", {
                            {"valid", validator["title"].valid()},
                            {"error", validator["title"].error()}
                        }}
                    }}
                }}
            };
        }
        
        std::size_t updated = update_place(json, place, loggedin_user.id());
        return {
            {"success", true},
            {"error", false},
            {"id", updated}
        };
    }
    
    return {
        {"error", true},
        {"message", "user not logged in"}
    };
}

nlohmann::json wee::apps::place::remove(udho::contexts::stateful<wee::session::user> ctx, std::size_t place){
    if(ctx.session().exists<wee::session::user>()){
        wee::session::user loggedin_user;
        ctx.session() >> loggedin_user;
        
        wee::Places  places;
        wee::Reports reports;
        
        auto place_records = _connection(select(places.project, places.author, reports.author.as(sqlpp::alias::a)).from(places.join(reports).on(reports.id == places.report)).where(places.id == place));
        if(!place_records.size()){
            throw udho::exceptions::http_error(boost::beast::http::status::not_found, "No such Place");
        }
        std::size_t project = place_records.front().project;
        std::size_t author  = place_records.front().author;
        std::size_t owner   = place_records.front().a;
        
        std::string reason;
        if(loggedin_user.id() != author && loggedin_user.id() != owner && !allowed_to("place:delete", project, loggedin_user.id(), reason)){
            return {
                {"error", true},
                {"message", reason}
            };
        }
        

        _connection(remove_from(places).where(places.id == place));
        return {
            {"id", place},
            {"success", true}
        };
    }else{
        return {
            {"error", true},
            {"message", "user not logged in"}
        };
    }
}

std::size_t wee::apps::place::add_place(nlohmann::json place, std::size_t project, std::size_t report, std::size_t user){
    std::string type          = place["type"].get<std::string>();
    std::string title         = place["title"].get<std::string>();
    nlohmann::json data       = place["data"];
    nlohmann::json properties = place["properties"];
    
    bool location_valid = false;
    if(data.is_object() && (type == "point" || type == "circle")){
        location_valid = data.contains("lat") && data.contains("lng");
    }else if(data.is_array() && (type == "polyline" || type == "polygon")){
        if(data.size() <= 1){
            location_valid = false;
        }else{
            location_valid = true;
            for(const auto& l: data){
                if(!l.contains("lat") || !l.contains("lng")){
                    location_valid = false;
                    break;
                }
            }
        }
    }else{
        location_valid = false;
    }
    
    if(!location_valid){
        return 0;
    }
    
    wee::Places places;
    auto statement =  sqlpp::postgresql::insert_into(places).set(
        places.type       = type, 
        places.title      = title, 
        places.data       = data.dump(), 
        places.properties = properties.dump(),
        places.report     = report,
        places.author     = user,
        places.project    = project
    ).returning(places.id);

    const auto results = _connection(statement);
    
    return results.front().id.value();
}

std::size_t wee::apps::place::update_place(nlohmann::json place, std::size_t id, std::size_t user){
    std::string type   = place["type"].get<std::string>();
    std::string title  = place["title"].get<std::string>();
    nlohmann::json data       = place["data"];
    nlohmann::json properties = place["properties"];
    
    wee::Places places;
    auto statement =  sqlpp::postgresql::update(places).set(
        places.type       = type, 
        places.title      = title, 
        places.data       = data.dump(), 
        places.properties = properties.dump(),
        places.author     = user
    ).where(places.id == id).returning(places.id);

    const auto results = _connection(statement);
    
    return results.front().id.value();
}

bool wee::apps::place::allowed_to(const std::string& resource, std::size_t project, std::size_t uid, std::string& reason){
    std::string sql = (boost::format("select * from allowed_to('%1%', %2%, %3%)") % resource % project % uid).str();
    auto statement = custom_query(sqlpp::verbatim(sql))
        .with_result_type_of(select(
            sqlpp::value(false).as(sqlpp::alias::a),
            sqlpp::value("").as(sqlpp::alias::b)
        ));
    auto records = _connection(statement);
    const auto& permission = records.front();
    reason = permission.b.value();
    return permission.a.value();
}
